// Copyright 2016 Matt Kinsey

// This is a simple skeleton for calling the grsph library

#include "grsph.h"

int main(int argc, char* argv[]) {
  // Hop into the grsph namespace
  using namespace grsph;

  // Init MPI
  MPI_Init(&argc, &argv);

  // Init grsph
  init_grsph();

  // Load GRSPH cfg file
  const char* cfgfile;
  if (argc < 2) {
    if (mpi_rank == 0)
      std::cout << "CFG file not given ( ./grsph <cfgfile> ), aborting"
                << std::endl;
    assert(0);
  } else {
    assert(argc == 2);
    cfgfile = argv[1];
  }
  load_cfg(cfgfile);

  // Init particle mesh from cfg
  init_mesh();

  // Init primative hydro varables and output them
  init_hydro_prims();
  //    init_STtest(); // TODO move this to config.cc
  output_h5part(0);

  // Init system from primative varables (and output result)
  init_sys_from_prim();
  output_h5part(0);

  // Iterate through time
  auto begin = std::chrono::high_resolution_clock::now();
  while (t < tmax) {
    take_step();
  }
  auto end = std::chrono::high_resolution_clock::now();

  // Print total time taken for run
  if (mpi_rank == 0)
    std::cout
        << "Done in "
        << std::chrono::duration_cast<std::chrono::seconds>(end - begin).count()
        << "s" << std::endl;

  // Wrap up MPI
  MPI_Finalize();
  return 0;
}
