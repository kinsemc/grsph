#!/bin/bash

# Copyright 2016 Matt Kinsey

# This script is a wrapper for the dep install scripts
# if the first arg exists rebuild (it can have any val)

cd external_libs

TBBF="./tbb/include/tbb/concurrent_unordered_map.h"
H5F="./hdf5/include/hdf5.h"
H5HF="./h5hut/include/H5Part.h"
GSLF="./gsl/include/gsl/gsl_version.h"
LIBCONFF="./libconfig/include/libconfig.h++"
LIBHILBERTF="./hilbert/include/Hilbert.hpp"

if [ -f "$TBBF" ] && ! [ -n "${1}" ]; then
    echo "$TBBF found, skipping tbb build"
else
    echo "$TBBF not found, building tbb"
    ./build_tbb.sh $1
fi

if [ -f "$H5F" ] && ! [ -n "${1}" ]; then
    echo "$H5F found, skipping hdf5 build"
else
    echo "$H5F not found, building hdf5"
    ./build_hdf5.sh $1
fi

if [ -f "$H5HF" ] && ! [ -n "${1}" ]; then
    echo "$H5HF found, skipping h5hut build"
else
    echo "$H5HF not found, building h5hut"
    ./build_h5hut.sh $1
fi

if [ -f "$GSLF" ] && ! [ -n "${1}" ]; then
    echo "$GSLF found, skipping gsl build"
else
    echo "$GSLF not found, building gsl"
    ./build_gsl.sh $1
fi

if [ -f "$LIBCONFF" ] && ! [ -n "${1}" ]; then
    echo "$LIBCONFF found, skipping libconfig build"
else
    echo "$LIBCONFF not found, building libconfig"
    ./build_libconfig.sh $1
fi

if [ -f "$LIBHILBERTF" ] && ! [ -n "${1}" ]; then                                  
    echo "$LIBHILBERTF found, skipping libhilbert build"                            
else                                                                            
    echo "$LIBHILBERTF not found, building libhilbert"                              
    ./build_hilbert.sh $1                                                     
fi
