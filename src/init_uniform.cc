// Copyright 2016 Matt Kinsey

// Initialize uniform pressure/density everywhere

#include "grsph.h"

void grsph::init_uniform(double P0, double D0) {
  size_t np = x.size();

#pragma omp parallel for
  for (size_t i = 0; i < np; ++i) {
    P[i] = P0;

    double v2 = 0.;
    for (size_t d = 0; d < DIM; ++d) {
      v2 += (*vel_ptr[d])[i] * (*vel_ptr[d])[i];
    }

    W[i] = 1. / std::sqrt(1. - v2);

    mass[i] = 10. / np * D0;
    h[i] = eta * std::pow(mass[i] / D0, 1. / DIM);
  }

  calc_init_D();

  double D_mean = particle_mean(&Dstar);
#pragma omp parallel for
  for (size_t i = 0; i < np; ++i) {
    mass[i] *= D0 / D_mean;
    h[i] = eta * std::pow(mass[i] / D0, 1. / DIM);
  }

  calc_init_D();

#pragma omp parallel for
  for (size_t i = 0; i < np; ++i) {
    u[i] = P[i] * W[i] / (eos_gamma - 1.) / Dstar[i];
  }
}
