// Copyright 2016 Matt Kinsey

// This is the spacetime of a BH in Cartesian Kerr-Schild coordinates

#ifndef SRC_KERR_H_
#define SRC_KERR_H_

#include <cmath>
#include <vector>

#include "spacetime.h"

class Kerr : public Spacetime {
  // public:
  // double Mh, aa;

 public:
  Kerr() {
    Mh = 0.;
    aa = 0.;
  }

  Kerr(double M, double a) {
    Mh = M;
    aa = a;
  }

  double lapse(const std::array<double, 3>& pos) override {
    double r2 = 0.;
    for (const auto& elem : pos) {
      r2 += elem * elem;
    }
    double a2 = aa * aa;
    double rho2 =
        0.5 * (r2 - a2) +
        0.5 * std::sqrt((r2 - a2) * (r2 - a2) + 4.0 * a2 * pos[2] * pos[2]);
    double rho = std::sqrt(rho2);

    double H = Mh * rho2 * rho / (rho2 * rho2 + a2 * pos[2] * pos[2]);

    return 1.0 / std::sqrt(1. + 2. * H);
  }

  std::array<double, DIM> shift(const std::array<double, 3>& pos) override {
    double r2 = 0.;
    for (const auto& elem : pos) {
      r2 += elem * elem;
    }

    double a2 = aa * aa;
    double rho2 =
        0.5 * (r2 - a2) +
        0.5 * std::sqrt((r2 - a2) * (r2 - a2) + 4.0 * a2 * pos[2] * pos[2]);
    double rho = std::sqrt(rho2);

    double H = Mh * rho2 * rho / (rho2 * rho2 + a2 * pos[2] * pos[2]);

    std::array<double, DIM> l;
    l[0] = (rho * pos[0] + aa * pos[1]) / (rho2 + a2);
    l[1] = (rho * pos[1] - aa * pos[0]) / (rho2 + a2);
    l[2] = pos[2] / rho;

    double alp = 1.0 / std::sqrt(1. + 2. * H);

    std::array<double, DIM> beta;
    for (size_t d = 0; d < DIM; ++d) {
      beta[d] = 2. * H * alp * alp * l[d];
    }

    return beta;
  }

  std::array<std::array<double, DIM>, DIM> gammaij(
      const std::array<double, 3>& pos) override {
    double r2 = 0.;
    for (const auto& elem : pos) {
      r2 += elem * elem;
    }

    double a2 = aa * aa;
    double rho2 =
        0.5 * (r2 - a2) +
        0.5 * std::sqrt((r2 - a2) * (r2 - a2) + 4.0 * a2 * pos[2] * pos[2]);
    double rho = std::sqrt(rho2);

    double H = Mh * rho2 * rho / (rho2 * rho2 + a2 * pos[2] * pos[2]);
    std::array<double, DIM> l;
    l[0] = (rho * pos[0] + aa * pos[1]) / (rho2 + a2);
    l[1] = (rho * pos[1] - aa * pos[0]) / (rho2 + a2);
    l[2] = pos[2] / rho;

    std::array<std::array<double, DIM>, DIM> met;
    for (size_t d = 0; d < DIM; ++d) {
      for (size_t d2 = 0; d2 < DIM; ++d2) {
        // init to eta_ij
        if (d == d2) {
          met[d][d2] = 1.;
        } else {
          met[d][d2] = 0.;
        }
        // add other terms
        met[d][d2] += 2. * H * l[d] * l[d2];
      }
    }

    return met;
  }

  std::array<std::array<double, DIM>, DIM> kij(
      const std::array<double, 3>& pos) override {
    double r2 = 0.;
    for (const auto& elem : pos) {
      r2 += elem * elem;
    }

    double a2 = aa * aa;
    double rho2 =
        0.5 * (r2 - a2) +
        0.5 * std::sqrt((r2 - a2) * (r2 - a2) + 4.0 * a2 * pos[2] * pos[2]);
    double rho = std::sqrt(rho2);

    double H = Mh * rho2 * rho / (rho2 * rho2 + a2 * pos[2] * pos[2]);
    std::array<double, DIM> l;
    l[0] = (rho * pos[0] + aa * pos[1]) / (rho2 + a2);
    l[1] = (rho * pos[1] - aa * pos[0]) / (rho2 + a2);
    l[2] = pos[2] / rho;

    double fac = 2. * H / std::sqrt(1. + 2. * H) / rho;

    std::array<std::array<double, DIM>, DIM> kij;
    for (size_t d = 0; d < DIM; ++d) {
      for (size_t d2 = 0; d2 < DIM; ++d2) {
        // init to eta_ij
        if (d == d2) {
          kij[d][d2] = 1.;
        } else {
          kij[d][d2] = 0.;
        }
        // add other terms
        kij[d][d2] -= (2. + H) * l[d] * l[d2];
        kij[d][d2] *= fac;
      }
    }
    return kij;
  }

  double det(const std::array<double, 3>& pos) override { return -1.; }

  double sdet(const std::array<double, 3>& pos) override {
    double r2 = 0.;
    for (const auto& elem : pos) {
      r2 += elem * elem;
    }

    double a2 = aa * aa;
    double rho2 =
        0.5 * (r2 - a2) +
        0.5 * std::sqrt((r2 - a2) * (r2 - a2) + 4.0 * a2 * pos[2] * pos[2]);
    double rho = std::sqrt(rho2);

    double H = Mh * rho2 * rho / (rho2 * rho2 + a2 * pos[2] * pos[2]);

    return 1. + 2. * H;
  }

  std::array<std::array<double, DIM + 1>, DIM + 1> gmunuU(
      const std::array<double, 3>& pos) override {
    double r2 = 0.;
    for (const auto& elem : pos) {
      r2 += elem * elem;
    }

    double a2 = aa * aa;
    double rho2 =
        0.5 * (r2 - a2) +
        0.5 * std::sqrt((r2 - a2) * (r2 - a2) + 4.0 * a2 * pos[2] * pos[2]);
    double rho = std::sqrt(rho2);

    double H = Mh * rho2 * rho / (rho2 * rho2 + a2 * pos[2] * pos[2]);

    std::array<double, DIM + 1> l;
    l[0] = -1.;
    l[1] = (rho * pos[0] + aa * pos[1]) / (rho2 + a2);
    l[2] = (rho * pos[1] - aa * pos[0]) / (rho2 + a2);
    l[3] = pos[2] / rho;

    std::array<std::array<double, DIM + 1>, DIM + 1> met;
    for (size_t d = 0; d < DIM + 1; ++d) {
      for (size_t d2 = 0; d2 < DIM + 1; ++d2) {
        // init to eta_ij
        if (d == d2) {
          met[d][d2] = 1.;
          if (d == 0) met[d][d2] = -1;
        } else {
          met[d][d2] = 0.;
        }
        // add other terms
        met[d][d2] -= 2. * H * l[d] * l[d2];
      }
    }

    return met;
  }

  std::array<std::array<double, DIM + 1>, DIM + 1> gmunuL(
      const std::array<double, 3>& pos) override {
    double r2 = 0.;
    for (const auto& elem : pos) {
      r2 += elem * elem;
    }

    double a2 = aa * aa;
    double rho2 =
        0.5 * (r2 - a2) +
        0.5 * std::sqrt((r2 - a2) * (r2 - a2) + 4.0 * a2 * pos[2] * pos[2]);
    double rho = std::sqrt(rho2);

    double H = Mh * rho2 * rho / (rho2 * rho2 + a2 * pos[2] * pos[2]);

    std::array<double, DIM + 1> l;
    l[0] = 1.;
    l[1] = (rho * pos[0] + aa * pos[1]) / (rho2 + a2);
    l[2] = (rho * pos[1] - aa * pos[0]) / (rho2 + a2);
    l[3] = pos[2] / rho;

    std::array<std::array<double, DIM + 1>, DIM + 1> met;
    for (size_t d = 0; d < DIM + 1; ++d) {
      for (size_t d2 = 0; d2 < DIM + 1; ++d2) {
        // init to eta_ij
        if (d == d2) {
          met[d][d2] = 1.;
          if (d == 0) met[d][d2] = -1;
        } else {
          met[d][d2] = 0.;
        }
        // add other terms
        met[d][d2] += 2. * H * l[d] * l[d2];
      }
    }

    return met;
  }

  std::array<std::array<std::array<double, DIM>, DIM + 1>, DIM + 1> gmunuUdx(
      const std::array<double, 3>& pos) override {
    std::array<std::array<std::array<double, DIM>, DIM + 1>, DIM + 1> gdx;

    double r2 = 0.;
    for (const auto& elem : pos) {
      r2 += elem * elem;
    }

    double z2 = pos[2] * pos[2];
    double a2 = aa * aa;
    double rho2 = 0.5 * (r2 - a2) +
                  0.5 * std::sqrt((r2 - a2) * (r2 - a2) + 4.0 * a2 * z2);
    double rho = std::sqrt(rho2);
    double rho3 = rho2 * rho;
    double rho4 = rho2 * rho2;

    double H = Mh * rho2 * rho / (rho2 * rho2 + a2 * z2);

    std::array<double, DIM + 1> l;
    l[0] = -1.;
    l[1] = (rho * pos[0] + aa * pos[1]) / (rho2 + a2);
    l[2] = (rho * pos[1] - aa * pos[0]) / (rho2 + a2);
    l[3] = pos[2] / rho;

    std::array<double, DIM> rhoD;
    rhoD[0] = pos[0] * rho / (2.0 * rho2 - r2 + a2);
    rhoD[1] = pos[1] * rho / (2.0 * rho2 - r2 + a2);
    rhoD[2] = (pos[2] / rho) * (rho2 + a2) / (2. * rho2 - r2 + a2);

    std::array<double, DIM> HD;
    HD[0] = (3.0 / rho - 4.0 * rho3 / (rho4 + a2 * z2)) * H * rhoD[0];
    HD[1] = (3.0 / rho - 4.0 * rho3 / (rho4 + a2 * z2)) * H * rhoD[1];
    HD[2] = (3.0 * rhoD[2] / rho -
             (4.0 * rho3 * rhoD[2] + 2.0 * a2 * pos[2]) / (rho4 + a2 * z2)) *
            H;

    std::array<std::array<double, DIM + 1>, DIM> lD;
    lD[0][0] = 0.;
    lD[0][1] = 0.;
    lD[0][2] = 0.;

    // TODO make a loop
    // NB: all the pos indices are off by one b/c its only a 3-vec

    lD[1][0] =
        (pos[0] * rhoD[0] + rho - 2.0 * rho * l[1] * rhoD[0]) / (rho2 + a2);
    lD[1][1] =
        (pos[0] * rhoD[1] + aa - 2.0 * rho * l[1] * rhoD[1]) / (rho2 + a2);
    lD[1][2] = (pos[0] * rhoD[2] - 2.0 * rho * l[1] * rhoD[2]) / (rho2 + a2);

    lD[2][0] =
        (pos[1] * rhoD[0] - aa - 2.0 * rho * l[2] * rhoD[0]) / (rho2 + a2);
    lD[2][1] =
        (pos[1] * rhoD[1] + rho - 2.0 * rho * l[2] * rhoD[1]) / (rho2 + a2);
    lD[2][2] = (pos[1] * rhoD[2] - 2.0 * rho * l[2] * rhoD[2]) / (rho2 + a2);

    lD[3][0] = -l[3] * rhoD[0] / rho;
    lD[3][1] = -l[3] * rhoD[1] / rho;
    lD[3][2] = (1.0 - l[3] * rhoD[2]) / rho;

    for (size_t mu = 0; mu < DIM + 1; ++mu) {
      for (size_t nu = 0; nu < DIM + 1; ++nu) {
        for (size_t i = 0; i < DIM; ++i) {
          gdx[mu][nu][i] =
              -2.0 * (HD[i] * l[mu] * l[nu] + H * lD[mu][i] * l[nu] +
                      H * l[mu] * lD[nu][i]);
        }
      }
    }

    return gdx;
  }

  std::array<std::array<std::array<double, DIM>, DIM + 1>, DIM + 1> gmunudx(
      const std::array<double, 3>& pos) override {
    std::array<std::array<std::array<double, DIM>, DIM + 1>, DIM + 1> gdx;

    double r2 = 0.;
    for (const auto& elem : pos) {
      r2 += elem * elem;
    }

    double z2 = pos[2] * pos[2];
    double a2 = aa * aa;
    double rho2 = 0.5 * (r2 - a2) +
                  0.5 * std::sqrt((r2 - a2) * (r2 - a2) + 4.0 * a2 * z2);
    double rho = std::sqrt(rho2);
    double rho3 = rho2 * rho;
    double rho4 = rho2 * rho2;

    double H = Mh * rho2 * rho / (rho2 * rho2 + a2 * z2);

    std::array<double, DIM + 1> l;
    l[0] = 1.;
    l[1] = (rho * pos[0] + aa * pos[1]) / (rho2 + a2);
    l[2] = (rho * pos[1] - aa * pos[0]) / (rho2 + a2);
    l[3] = pos[2] / rho;

    std::array<double, DIM> rhoD;
    rhoD[0] = pos[0] * rho / (2.0 * rho2 - r2 + a2);
    rhoD[1] = pos[1] * rho / (2.0 * rho2 - r2 + a2);
    rhoD[2] = (pos[2] / rho) * (rho2 + a2) / (2. * rho2 - r2 + a2);

    std::array<double, DIM> HD;
    HD[0] = (3.0 / rho - 4.0 * rho3 / (rho4 + a2 * z2)) * H * rhoD[0];
    HD[1] = (3.0 / rho - 4.0 * rho3 / (rho4 + a2 * z2)) * H * rhoD[1];
    HD[2] = (3.0 * rhoD[2] / rho -
             (4.0 * rho3 * rhoD[2] + 2.0 * a2 * pos[2]) / (rho4 + a2 * z2)) *
            H;

    // TODO loop?
    // NB: all the pos indices are off by one b/c its a 3-vec

    std::array<std::array<double, DIM + 1>, DIM> lD;
    lD[0][0] = 0.;
    lD[0][1] = 0.;
    lD[0][2] = 0.;

    lD[1][0] =
        (pos[0] * rhoD[0] + rho - 2.0 * rho * l[1] * rhoD[0]) / (rho2 + a2);
    lD[1][1] =
        (pos[0] * rhoD[1] + aa - 2.0 * rho * l[1] * rhoD[1]) / (rho2 + a2);
    lD[1][2] = (pos[0] * rhoD[2] - 2.0 * rho * l[1] * rhoD[2]) / (rho2 + a2);

    lD[2][0] =
        (pos[1] * rhoD[0] - aa - 2.0 * rho * l[2] * rhoD[0]) / (rho2 + a2);
    lD[2][1] =
        (pos[1] * rhoD[1] + rho - 2.0 * rho * l[2] * rhoD[1]) / (rho2 + a2);
    lD[2][2] = (pos[1] * rhoD[2] - 2.0 * rho * l[2] * rhoD[2]) / (rho2 + a2);

    lD[3][0] = -l[3] * rhoD[0] / rho;
    lD[3][1] = -l[3] * rhoD[1] / rho;
    lD[3][2] = (1.0 - l[3] * rhoD[2]) / rho;

    for (size_t mu = 0; mu < DIM + 1; ++mu) {
      for (size_t nu = 0; nu < DIM + 1; ++nu) {
        for (size_t i = 0; i < DIM; ++i) {
          gdx[mu][nu][i] =
              2.0 * (HD[i] * l[mu] * l[nu] + H * lD[mu][i] * l[nu] +
                     H * l[mu] * lD[nu][i]);
        }
      }
    }

    return gdx;
  }
};
#endif  // SRC_KERR_H_
