// Copyright 2016 Matt Kinsey

// Just some basic output to stdout

#include <fstream>
#include <iomanip>

#include "grsph.h"

namespace grsph {

bool cons_csv = true;
bool cons_csv_overwrite = true;
std::string cons_filename = "conservation.csv";
};  // namespace grsph

// this just dumps a particle to stdout. For debugging...
void grsph::print_part(int i) {
  for (auto &elem : particle_vectors) {
    std::cout << (elem)->at(i) << ", ";
  }
  std::cout << std::endl;
}

void grsph::print_conservation() {
  double Etot = 0, Mtot = 0;
  std::array<double, 3> momtot = {{0., 0., 0.}};
  double Etotg = 0, Mtotg = 0;
  std::array<double, 3> momtotg = {{0, 0, 0}};
  std::array<double, 3> com = {{0, 0, 0}};

  double masssum = 0;
  size_t np = x.size();

  for (size_t i = 0; i < np; ++i) {
    if (ghost[i]) continue;

    std::array<double, 3> pos = {{}};
    for (size_t d = 0; d < DIM; ++d) {
      pos[d] = (*pos_ptr[d])[i];
    }

    double sdet = std::sqrt(spacetime->sdet(pos));
    // TODO I dont think these numbers are right
    // TODO add ang mom
    Etot += E[i] * Dstar[i] / sdet;
    momtot[0] += Sx[i] * Dstar[i] / sdet;
    com[0] += mass[i] * x[i];
#if dim2
    momtot[1] += Sy[i] * Dstar[i] / sdet;
    com[1] += mass[i] * y[i];
#endif
#if dim3
    momtot[2] += Sz[i] * Dstar[i] / sdet;
    com[2] += mass[i] * z[i];
#endif
    // TODO ang_mom?
    masssum += mass[i];
    Mtot += Dstar[i] / sdet / W[i];
  }
  com[0] /= masssum;
  com[1] /= masssum;
  com[2] /= masssum;

  Etotg = 0;
  Mtotg = 0;
  momtotg = {{0, 0, 0}};

  MPI_Allreduce(&Etot, &Etotg, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_GRSPH);
  MPI_Allreduce(&momtot, &momtotg, 3, MPI_DOUBLE, MPI_SUM, MPI_COMM_GRSPH);
  MPI_Allreduce(&Mtot, &Mtotg, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_GRSPH);

  double gnp = global_np(false);

  Etotg /= gnp;
  for (size_t d = 0; d < DIM; ++d) {
    momtotg[d] /= gnp;
  }

  double mommag = std::sqrt(momtotg[0] * momtotg[0] + momtotg[1] * momtotg[1] +
                            momtotg[2] * momtotg[2]);

  double comr = std::sqrt(com[0] * com[0] + com[1] * com[1] + com[2] * com[2]);

  if (cons_csv) {
    std::ofstream consfile;
    std::string filename = io_dir + cons_filename;
    consfile.open(filename, std::ios::out | std::ios::app);
    if (consfile.is_open()) {
      consfile << Mtotg << ", " << Etotg << ", " << momtotg[0] << ", "
               << momtotg[1] << ", " << momtotg[2] << ", " << com[0] << ", "
               << com[1] << ", " << com[2] << ", " << masssum << std::endl;
      consfile.close();
    }
  }

  if (mpi_rank == 0) {
    std::cout << "Reduced values:" << std::endl;
    std::cout << "  " << std::setprecision(8) << "sum(Dstar) = " << Mtotg
              << " | "
              << "avg(E) = " << Etotg << " | "
              << "avg(S) = { " << momtotg[0]
#if dim2
              << ", " << momtotg[1]
#endif
#if dim3
              << ", " << momtotg[2]
#endif
              << " } [" << mommag << "] "
              << "CoM = { " << com[0]
#if dim2
              << ", " << com[1]
#endif
#if dim3
              << ", " << com[2]
#endif
              << " } [" << comr << "]" << std::endl;
  }
}
