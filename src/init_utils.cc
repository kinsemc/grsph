// Copyright 2016 Matt Kinsey

// Some utilities used to initialize the hydro variables on a grid

#include "grsph.h"

namespace grsph {
std::function<void()> init_hydro_prims;
};  // namespace grsph

void grsph::rescale_hydro(double scale) {
  size_t np = x.size();
#pragma omp parallel for
  for (size_t i = 0; i < np; ++i) {
    for (size_t d = 0; d < DIM; ++d) {
      (*pos_ptr[d])[i] *= scale;
      (*vel_ptr[d])[i] *= scale;
      mass[i] *= scale;
    }

    h[i] *= scale;
    P[i] *= scale;
    u[i] *= scale;
  }

  if (periodic) {
    for (size_t d = 0; d < DIM; ++d) {
      periodic_box[2 * d] *= scale;
      periodic_box[2 * d + 1] *= scale;
    }
  }
}

#if 0
void grsph::init_2dRiemanntest() {
#if DIM == 2
  size_t np = x.size();

  for (size_t i = 0; i < np; ++i) {
    mass[i] = 4. / np * 0.1;
    // fermi_func(double A, double B, double yt, double dy, double y)
    // mass[i] =
    if (x[i] <= 0. && y[i] <= 0.) {
      mass[i] *= 2.;
      Dstar[i] = .2;
      P[i] = 1.;
    }
    if (x[i] > 0. && y[i] <= 0.) {
      Dstar[i] = .1;
      vy[i] = 0.5;
      P[i] = 1.;
    }
    if (x[i] <= 0. && y[i] > 0.) {
      Dstar[i] = .1;
      vx[i] = 0.5;
      P[i] = 1.;
    }
    if (x[i] > 0. && y[i] > 0.) {
      Dstar[i] = .1;
      P[i] = 0.01;
    }
    Dstar[i] *= 5.;
    mass[i] *= 5.;
    P[i] /= 5.;
    double v2 = 0.;
    for (size_t d = 0; d < DIM; ++d) {
      v2 += (*vel_ptr[d])[i] * (*vel_ptr[d])[i];
    }

    W[i] = 1. / std::sqrt(1. - v2);

    u[i] = P[i] * W[i] / (eos_gamma - 1.) / Dstar[i];
    h[i] = 5. * eta * std::pow(mass[i], 1. / DIM);  // todo cast dim?
  }
#else
  assert(0);
#endif
}
#endif

void grsph::calc_init_D() {
  if (mpi_rank == 0) std::cout << "Initializing D for the mesh... ";

  // Update l_min/max for calc_ORB_boxes
  auto h_minmax = particle_minmax(&h);
  l_min = static_cast<int>(
      std::ceil(std::log2(nn_hfac * support_r * h_minmax.first)));
  l_max = static_cast<int>(
      std::ceil(std::log2(nn_hfac * support_r * h_minmax.second)));

  // Setup MPI boxes
  calc_ORB_boxes();

  // Sync particles to thier MPI owners
  sync_particles_to_procs();

  // order particles via hilbert curve
  sort_particles();

  // Fill in the hash table
  build_hashtable();

  // Sync periodic ghosts
  if (periodic) find_periodic_ghosts();

  // MPI sync to get ghosts for the hash table
  find_ghosts();

  // Find neighbors
  build_neighbor_list();

  // Calculate D and h
  calc_D();

  // Clear ghosts
  clear_ghosts();

  if (mpi_rank == 0) std::cout << "done" << std::endl;
}

void grsph::init_sys_from_prim() {
  if (mpi_rank == 0)
    std::cout << "Initializing SPH system from prim variables... ";

  calc_init_D();

  prim2con();

  con2prim();  // todo Im pretty sure this isnt needed

  if (mpi_rank == 0) std::cout << "done" << std::endl;
}
