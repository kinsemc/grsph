// Copyright 2016 Matt Kinsey

// TODO this file has some problems, don't use it for now

#include "grsph.h"

void grsph::particle_grc2p_gamma(const size_t i) {
  if (ghost[i]) return;

  std::array<double, 3> pos = {{}};
  for (size_t d = 0; d < DIM; ++d) {
    pos[d] = (*pos_ptr[d])[i];
  }

  double sdet = std::sqrt(spacetime->sdet(pos));
  auto metU = spacetime->gmunuU(pos);  // full metric g^munu
  double lapse = spacetime->lapse(pos);
  auto shift = spacetime->shift(pos);

  std::vector<std::vector<double>> gammaU(DIM, std::vector<double>(DIM));

  for (int ii = 1; ii < DIM + 1; ++ii) {
    for (int jj = 1; jj < DIM + 1; ++jj) {
      gammaU[ii - 1][jj - 1] =
          metU[ii][jj] + shift[ii - 1] * shift[jj - 1] / lapse / lapse;
    }
  }

  std::array<double, DIM> SvU = {};

  double S2 = 0.;
  for (int ii = 0; ii < DIM; ++ii) {
    for (int jj = 0; jj < DIM; ++jj) {
      S2 += gammaU[ii][jj] * (*S_ptr[ii])[i] * (*S_ptr[jj])[i];
      SvU[ii] += gammaU[ii][jj] * (*S_ptr[jj])[i];
    }
  }

  double tmp = 0;
  for (int ii = 0; ii < DIM; ++ii) {
    tmp += shift[ii] * (*S_ptr[ii])[i];
  }

  E[i] = (Ebar[i] + tmp) / lapse;
  double Et = E[i] + sdet * q[i] / eos_gamma / Dstar[i];
  double Et2 = Et * Et;
  double G = 1. - 1. / eos_gamma;

  // Newton-Raphson until we have W
  int nr_iter = 0;
  double W_g = W[i];
  while (true) {
    // F is the func we need the root of (F(W) = 0)
    double F = (S2 - Et2) * W_g * W_g * W_g * W_g +
               2. * G * Et * W_g * W_g * W_g +
               (Et2 - 2. * G * S2 - G * G) * W_g * W_g - 2. * G * Et * W_g +
               G * G * (1. + S2);

    double Fprime = 4. * (S2 - Et2) * W_g * W_g * W_g +
                    6. * G * Et * W_g * W_g +
                    2. * (Et2 - 2. * G * S2 - G * G) * W_g - 2. * G * Et;

    double W_n = W_g - F / Fprime;

    // TODO is this right?
    if (std::abs(W_n - W_g) / W_n < c2p_eps) {
      W[i] = W_n;
      rho[i] = Dstar[i] / sdet / W_n;
      //      P[i] = G * ((rho[i] * Et * W_n - rho[i] * G) / (W_n * W_n -
      //      G) -
      //                  q[i] - rho[i]);
      P[i] =
          rho[i] * G * ((Et * W_n - G) / (W_n * W_n - G) - q[i] / rho[i] - 1.);
      //      P[i] = 1. / (1. + 1. / (eos_gamma- 1)) *
      //             (rho[i] * (Et * W_n - G) / (W_n * W_n - G) - q[i] -
      //             rho[i]);
      //      enth[i] = (Et * W_n - G) / (W_n * W_n - G) - q[i] / rho[i];
      if (P[i] < 0.) {
        std::cout << "S2=" << S2 << " "
                  << "Et=" << Et << " "
                  << "G=" << G << " "
                  << "W_n=" << W_n << " "
                  << "W_g=" << W_g << " "
                  << "F=" << F << " "
                  << "Fprime=" << Fprime << " " << std::endl;
        // std::cerr << "HIT PRESSURE FLOOR" << std::endl;
        // P[i] = 0.00000000000001;
        ghost[i] = 1;
        assert(0);
      }
      enth[i] = 1. + P[i] / G / rho[i];
      u[i] = enth[i] - 1. - P[i] / rho[i];
      cs[i] = std::sqrt(eos_gamma * P[i] / rho[i] / enth[i]);
      // we may not need the vbar arrays...
      for (size_t d = 0; d < DIM; ++d) {
        double vbar = SvU[d] / (W_n * (enth[i] + q[i] / rho[i]));
        (*vel_ptr[d])[i] = lapse * vbar - shift[d];
      }
      // double vbarx = SvU[0] / (W_n * (enth[i] + q[i] / rho[i]));
      // double vbary = SvU[1] / (W_n * (enth[i] + q[i] / rho[i]));
      // double vbarz = SvU[2] / (W_n * (enth[i] + q[i] / rho[i]));
      // vx[i] = lapse * vbarx - shift[0];
      // vy[i] = lapse * vbary - shift[1];
      // vz[i] = lapse * vbarz - shift[2];
      break;  // break out of the while(1) loop
    }

    // relax it so we dont overshoot and wind up negative
    // that shouldnt be needed
    double ome = 1.;
    W_g = (1. - ome) * W_g + ome * W_n;

    if (nr_iter >= max_nr_iter || !std::isfinite(W_g) /*|| W_g < 1.*/) {
      std::cout << " grcon2prim root finding failed @ x=" << x[i] << " "
                << "W_n=" << W_n << " "
                << "W_g=" << W_g << " "
                << "F=" << F << " "
                << "Fprime=" << Fprime << " "
                << "nr_iter=" << nr_iter << std::endl;

      // if more verbose?
      std::cout << "D=" << Dstar[i] << " "
                << "Ebar=" << Ebar[i] << " "
                << "W=" << W[i] << " "
                << "S2=" << S2 << " "
                << "u=" << u[i] << " "
                << "ome=" << omega[i] << std::endl;

      // Fail
      assert(0);
    }
    nr_iter++;
  }
}

void grsph::particle_grp2c_gamma(const size_t i) {
  std::array<double, 3> pos = {{}};
  for (size_t d = 0; d < DIM; ++d) {
    pos[d] = (*pos_ptr[d])[i];
  }

  double det = std::sqrt(spacetime->sdet(pos));
  double lapse = spacetime->lapse(pos);
  auto shift = spacetime->shift(pos);
  auto gammaij = spacetime->gammaij(pos);

  // TODO TODO TODO this is wrong (tmp fix)
  std::array<double, DIM> vb = {{}};
  for (size_t d = 0; d < DIM; ++d) {
    // vb[d] = ((*vel_ptr[d])[i] + shift[i]) / lapse;
    vb[d] = (*vel_ptr[d])[i];
  }

  std::array<double, DIM> vb_ = {{}};
  double tmp = 0.;

  for (size_t m = 0; m < DIM; ++m) {
    for (size_t n = 0; n < DIM; ++n) {
      tmp += gammaij[m][n] * vb[m] * vb[n];
      vb_[m] += gammaij[m][n] * vb[n];
    }
  }

  W[i] = 1. / std::sqrt(1. - tmp);

  rho[i] = Dstar[i] / det / W[i];
  P[i] = (eos_gamma - 1.) * rho[i] * u[i];
  enth[i] = 1. + u[i] + P[i] / rho[i];
  u[i] = enth[i] - 1. - P[i] / rho[i];
  cs[i] = std::sqrt(eos_gamma * P[i] / rho[i] / enth[i]);

  // TODO assumes q = 0
  for (size_t d = 0; d < DIM; ++d) {
    (*S_ptr[d])[i] = enth[i] * W[i] * vb_[d];
  }

  E[i] = enth[i] * W[i] - det * (P[i]) / Dstar[i];

  tmp = 0.;
  for (size_t m = 0; m < DIM; ++m) {
    tmp += shift[m] * (*S_ptr[m])[i];
  }

  Ebar[i] = lapse * E[i] - tmp;
}
