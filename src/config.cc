// Copyright 2016 Matt Kinsey

// utility to read global vars from libconfig .cfg files
// TODO this should be organized

#include <assert.h>
#include <string.h>

#include "grsph.h"

#include <libconfig.h++>

#include "kernels.h"

namespace grsph {

Kerr kerr;
Minkowski minkowski;
Schwarzschild schwarzschild;

template <typename T>
void read_cfg_var(const libconfig::Config &cfg, const std::string name,
                  T &var) {
  try {
    if (cfg.lookupValue(name, var)) {
      if (mpi_rank == 0) std::cout << name << " = " << var << std::endl;
    } else {
      if (mpi_rank == 0) {
        std::cout << name << " not found in cfg! Using default [" << var << "]"
                  << std::endl;
      }
    }
  } catch (const libconfig::SettingNotFoundException) {
    std::cerr << "Setting not found: " << name << std::endl;
  }
}

};  // namespace grsph

void grsph::load_cfg(const char *cfgfilename) {
  libconfig::Config cfg;

  if (mpi_rank == 0)
    std::cout << "Loaded cfg file " << cfgfilename << std::endl;
  if (mpi_rank == 0)
    std::cout << "---------------------Params---------------------"
              << std::endl;

  // Read the file. If there is an error, report it and exit.
  try {
    cfg.readFile(cfgfilename);
  } catch (const libconfig::FileIOException &fioex) {
    std::cerr << "I/O error while reading cfg file." << std::endl;
    assert(0);
  } catch (const libconfig::ParseException &pex) {
    std::cerr << "Parse error at " << pex.getFile() << ":" << pex.getLine()
              << " - " << pex.getError() << std::endl;
    assert(0);
  }

  // Dissipation min/max
  read_cfg_var(cfg, "alpha_min", alpha_min);
  read_cfg_var(cfg, "alpha_max", alpha_max);
  read_cfg_var(cfg, "Nnoise", N_noise);

  // Root finding params
  double D_h_NR_exp = -4, con2primexp = -4;
  read_cfg_var(cfg, "D_h_NR_exp", D_h_NR_exp);
  h_eps = std::pow(10.0, D_h_NR_exp);
  read_cfg_var(cfg, "con2primexp", con2primexp);
  c2p_eps = std::pow(10.0, con2primexp);

  read_cfg_var(cfg, "max_nr_iter", max_nr_iter);

  // Use gsl?
  bool c2p_gsl = true, calcD_gsl = true;
  read_cfg_var(cfg, "c2pgsl", c2p_gsl);
  read_cfg_var(cfg, "calcDgsl", calcD_gsl);
  if (calcD_gsl) {
    calc_D_func = particle_D_h_NR_gsl;
  } else {
    calc_D_func = particle_D_h_NR;
  }

  // Time levels
  read_cfg_var(cfg, "maxtl", maxtl);

  // Periodic box
  if (cfg.exists("periodic")) {
    periodic = true;
    auto &pbox = cfg.lookup("periodic");
    assert(pbox.isArray() && pbox.getLength() == 2 * DIM);
    if (mpi_rank == 0) std::cout << "periodic_box = [ ";
    for (size_t i = 0; i < 2 * DIM; ++i) {
      periodic_box[i] = pbox[i];
      if (mpi_rank == 0) std::cout << periodic_box[i] << ", ";
    }
    if (mpi_rank == 0) std::cout << " ]" << std::endl;
  }

  // Init mesh
  std::string mesh_type;
  int mesh_N;
  read_cfg_var(cfg, "mesh.type", mesh_type);
  read_cfg_var(cfg, "mesh.N", mesh_N);
  if (mesh_type == "hex") {
    mesh_func = std::bind(hex_lattice, mesh_N);
  } else if (mesh_type == "cubic") {
    mesh_func = std::bind(cubic_lattice, static_cast<size_t>(mesh_N));
  } else if (mesh_type == "rand") {
    mesh_func = std::bind(init_rand_mesh, mesh_N);
  } else if (mesh_type == "none") {
    mesh_func = std::bind(init_empty_mesh);
  } else {
    std::cerr << "Unknown mesh type" << std::endl;
    // assert(0);
  }

  // Init hydro prims
  std::string hydro_sys;
  read_cfg_var(cfg, "hydro.system", hydro_sys);
  // Lower hydro_sys string
  std::transform(hydro_sys.begin(), hydro_sys.end(), hydro_sys.begin(),
                 [](unsigned char c) { return std::tolower(c); });
  if (hydro_sys == "riemann") {
    assert(DIM == 1);
    double Dleft, Dright, Pleft, Pright, sep, xminmax, scale;
    int Nsmooth;
    read_cfg_var(cfg, "hydro.Dleft", Dleft);
    read_cfg_var(cfg, "hydro.Dright", Dright);
    read_cfg_var(cfg, "hydro.Pleft", Pleft);
    read_cfg_var(cfg, "hydro.Pright", Pright);
    read_cfg_var(cfg, "hydro.seperation",
                 sep);  // TODO rename to left sep or something
    read_cfg_var(cfg, "hydro.Nsmooth", Nsmooth);
    read_cfg_var(cfg, "hydro.minmax", xminmax);
    read_cfg_var(cfg, "hydro.scale", scale);
    init_hydro_prims = std::bind(init_riemann1d, Dleft, Dright, Pleft, Pright,
                                 sep, Nsmooth, xminmax, scale);
  } else if (hydro_sys == "vortex") {
    assert(DIM == 2);
    double scale, Pmag, Dmag;
    read_cfg_var(cfg, "hydro.scale", scale);
    read_cfg_var(cfg, "hydro.Pmag", Pmag);
    read_cfg_var(cfg, "hydro.Dmag", Dmag);
    init_hydro_prims = std::bind(init_vortex, scale, Pmag, Dmag);
  } else if (hydro_sys == "sedov") {
    double P0, D0, E0, dr;
    read_cfg_var(cfg, "hydro.P0", P0);
    read_cfg_var(cfg, "hydro.D0", D0);
    read_cfg_var(cfg, "hydro.E0", E0);
    read_cfg_var(cfg, "hydro.dr", dr);
    init_hydro_prims = std::bind(init_sedov, P0, D0, E0, dr);
  } else if (hydro_sys == "kh") {
    double vy0 = 0.;
    read_cfg_var(cfg, "hydro.vy0", vy0);
    init_hydro_prims = std::bind(init_KH, vy0);
  } else if (hydro_sys == "bondi") {
    double rc, K, rmin, rmax;
    read_cfg_var(cfg, "hydro.rc", rc);
    read_cfg_var(cfg, "hydro.K", K);
    read_cfg_var(cfg, "hydro.rmin", rmin);
    read_cfg_var(cfg, "hydro.rmax", rmax);
    init_hydro_prims = std::bind(init_bondi, rc, K, rmin, rmax);
  } else if (hydro_sys == "uniform") {
    double P0, D0;
    read_cfg_var(cfg, "hydro.P0", P0);
    read_cfg_var(cfg, "hydro.D0", D0);
    init_hydro_prims = std::bind(init_uniform, P0, D0);
  } else {
    std::cerr << "Unknown hydro system" << std::endl;
    // assert(0);
  }

  // todo this could use asserts
  for (size_t d = 0; d < DIM; ++d) {
    if (cfg.exists("mesh.shift")) {
      auto &tmp = cfg.lookup("mesh.shift");
      init_shift[d] = tmp[d];
    }
    if (cfg.exists("mesh.scale")) {
      auto &tmp = cfg.lookup("mesh.scale");
      init_scale[d] = tmp[d];
    } else {
      init_scale[d] = 1.;
    }
  }
  read_cfg_var(cfg, "mesh.init_glass", init_glass);
  if (init_glass) {
    read_cfg_var(cfg, "mesh.init_glass", init_glass);
    read_cfg_var(cfg, "mesh.perturb_max", perturb_max);
    read_cfg_var(cfg, "mesh.glass_max_err", glass_max_err);
    read_cfg_var(cfg, "mesh.glass_out_every", glass_out_every);
  }

  read_cfg_var(cfg, "print_boxes", print_boxes);

  // Kerr params
  if (cfg.exists("kerr")) {
    double Mh = 0., aa = 0.;
    read_cfg_var(cfg, "kerr.M", Mh);
    read_cfg_var(cfg, "kerr.a", aa);
    kerr = Kerr(Mh, aa);
    spacetime = &kerr;
    read_cfg_var(cfg, "clean_inside_r", clean_inside_r);
  } else if (cfg.exists("schwarzschild")) {
    double Mh = 0.;
    read_cfg_var(cfg, "schwazschild.M", Mh);
    schwarzschild = Schwarzschild(Mh);
    spacetime = &schwarzschild;
  } else {
    minkowski = Minkowski();
    spacetime = &minkowski;
  }

  // H5Part IO
  read_cfg_var(cfg, "h5part.filename", h5_filename);
  read_cfg_var(cfg, "h5part.overwrite", h5_overwrite);
  read_cfg_var(cfg, "h5part.out_every", h5_out_every);
  read_cfg_var(cfg, "h5part.output_ghosts", h5_output_ghosts);
  auto &outvars = cfg.lookup("h5part.outvars");
  if (mpi_rank == 0) std::cout << "h5_outvars = [ ";
  //  std::array<std:string,3> posnames = {"x", "y", "z"};
  //  for ( size_t d; d < DIM; ++d ) {
  //    h5_outvar.push_back(Sname[d]);
  //  }
  for (std::string outvar : outvars) {
    if (outvar == "S") {
      std::vector<std::string> Snames = {{"S^x", "S^y", "S^z"}};
      for (size_t d = 0; d < DIM; ++d) {
        h5_outvars.push_back(Snames[d]);
      }
    } else if (outvar == "v") {
      std::vector<std::string> vnames = {{"v^x", "v^y", "v^z"}};
      for (size_t d = 0; d < DIM; ++d) {
        h5_outvars.push_back(vnames[d]);
      }
    } else if (var_names.count(outvar) > 0) {
      h5_outvars.push_back(outvar);
    } else {
      if (mpi_rank == 0)
        std::cout << std::endl
                  << std::endl
                  << "!!! CRITICAL: INVALID VAR NAME '" << outvar
                  << "' IN h5_outvars !!!" << std::endl
                  << "Valid values are:" << std::endl;
      for (auto var_name : var_names) {
        if (mpi_rank == 0)
          std::cout << "'" << var_name.first << "'" << std::endl;
      }
      if (mpi_rank == 0)
        std::cout << "'S'" << std::endl << "'v'" << std::endl << std::endl;
      assert(var_names.count(outvar) > 0);
    }
    if (mpi_rank == 0) std::cout << outvar << ", ";
  }
  if (mpi_rank == 0) std::cout << "]" << std::endl;
  read_cfg_var(cfg, "io_dir", io_dir);

  // Random stuff todo rename
  // rename these?
  read_cfg_var(cfg, "cons_csv", cons_csv);
  read_cfg_var(cfg, "cons_csv_overwrite", cons_csv_overwrite);
  read_cfg_var(cfg, "cons_filename", cons_filename);

  read_cfg_var(cfg, "recalc_ORB_boxes_every", recalc_ORB_boxes_every);
  read_cfg_var(cfg, "update_ORB_boxes_every", update_ORB_boxes_every);
  read_cfg_var(cfg, "sort_every", sort_every);
  read_cfg_var(cfg, "print_basic_every", print_basic_every);
  read_cfg_var(cfg, "print_timers_every", print_timers_every);
  read_cfg_var(cfg, "print_conservation_every", print_conservation_every);
  read_cfg_var(cfg, "verbose", verbose);

  // Kernel
  std::string kern_str = "wendland";
  read_cfg_var(cfg, "kernel.name", kern_str);
  if (kern_str == "wendland") {
    kernel = wendland;
    Dkernel = Dwendland;
    Dhkernel = Dhwendland;
    eta = 2.2;
    support_r = 1.;
  } else if (kern_str == "quartic_spline") {
    kernel = quartic_spline;
    Dkernel = Dquartic_spline;
    Dhkernel = Dhquartic_spline;
    eta = 1.4;
    support_r = 2.;
  } else if (kern_str == "cubic_spline") {
    kernel = cubic_spline;
    Dkernel = Dcubic_spline;
    Dhkernel = Dhcubic_spline;
    eta = 1.4;
    support_r = 2.;
  } else {
    std::cerr << "Unknown kernel" << std::endl;
    assert(0);
  }
  // careful changing these (warn?)
  // eta is kinda flexible but support_r is 100% kernel dependent
  read_cfg_var(cfg, "kernel.eta", eta);
  // read_cfg_var(cfg, "kernel.support_r", support_r);

  // TODO one rhs for integral derives and one without
  // rhs_func = &grsph::calc_rhs;
  rhs_func = calc_rhs;

  // EoS
  std::string eos_str = "gamma";
  read_cfg_var(cfg, "eos", eos_str);
  if (eos_str == "poly") {
    if (c2p_gsl) {
      //      c2p_func = &grsph::particle_grc2p_poly_gsl;
      assert(0);
    } else {
      c2p_func = particle_grc2p_poly;
    }
    p2c_func = particle_grp2c_poly;
    read_cfg_var(cfg, "eos_gamma", eos_gamma);
    read_cfg_var(cfg, "eos_K", K);
  } else if (eos_str == "gamma") {
    if (c2p_gsl) {
      c2p_func = particle_grc2p_new;  // gamma_gsl; //TODO
    } else {
      c2p_func = particle_grc2p_gamma;
    }
    p2c_func = particle_grp2c_gamma;
    read_cfg_var(cfg, "eos_gamma", eos_gamma);
  }

  // Time Integration
  read_cfg_var(cfg, "dt", dt);
  read_cfg_var(cfg, "tmax", tmax);
  std::string integ_str = "leapfrog";
  read_cfg_var(cfg, "integrator", integ_str);
  if (integ_str == "rk3") {
    integrator_func = tvd_rk3_step;
  } else if (integ_str == "rk2") {
    integrator_func = tvd_rk2_step;
  } else if (integ_str == "leapfrog") {
    assert(mpi_size ==
           1);  // todo Need to mpi comm the *tmp vars for mpi_size > 1
    integrator_func = leapfrog_step;
  } else {
    std::cout << "Invalid Integrator" << std::endl;
    assert(0);
  }

  // Print success on rank 0
  if (mpi_rank == 0)
    std::cout << "------------------------------------------------" << std::endl
              << std::endl;
}
