// Copyright 2016 Matt Kinsey

// Calc RHS for GR SPH vars
// TODO needs comments

#include "grsph.h"
#include "invert_matrix.h"

namespace grsph {
void particle_rhs(const size_t i);
};  // namespace grsph

void grsph::calc_rhs() {
  // Clear the old ghosts before we get new ones
  clear_ghosts();

  // Build the ghost free hashtable
  build_hashtable();

  // Periodic ghosts
  if (periodic) find_periodic_ghosts();

  // MPI sync ghosts
  find_ghosts();
  start_rhs_timing();
  // Find neighbors
  build_neighbor_list();

  // Calc D/h iterativly. Fill in omega too.
  calc_D();

  clear_ghosts();

  // con2prim now that we have D
  con2prim();
  stop_rhs_timing();
  // Rebuild the table with new h's
  build_hashtable();

  // Regrab ghosts
  if (periodic) find_periodic_ghosts();
  find_ghosts();
  start_rhs_timing();
  // Find neighbors
  build_neighbor_list();

  // Calculate and sync C for all particles
  start_timer("Ccalc");
  particle_loop(particle_rhs_Ccalc);
  stop_timer("Ccalc");

  // Resync C_ij
  stop_rhs_timing();
  for (int k = 0; k < DIM; ++k) {
    for (int d = 0; d < DIM; ++d) {
      ghost_sync_particlevec((*C_ptr[k][d]));
    }
  }
  start_rhs_timing();

  // calc divv
  calc_divv();

  // Resync divv
  stop_rhs_timing();
  ghost_sync_particlevec(divv);
  ghost_sync_particlevec(divvdt);
  start_rhs_timing();

  // Calc alpha
  calc_alpha();

  // Resync alpha
  stop_rhs_timing();
  ghost_sync_particlevec(alpha);
  start_rhs_timing();

  // RHS loop over particles
  start_timer("rhs");
  particle_loop(particle_rhs);
  stop_rhs_timing();
  stop_timer("rhs");
}

void grsph::particle_rhs_Ccalc(const size_t i) {
  if (ghost[i]) return;

  std::array<std::array<double, DIM>, DIM> tau;
  std::array<std::array<double, DIM>, DIM> C;

  // Fill the matrix tau
  for (int k = 0; k < DIM; ++k) {
    for (int d = 0; d < DIM; ++d) {
      double tau_tmp = 0.;
      for (const auto& ni : neighbors[i]) {
        double dist2 = fast_dist2(i, ni);
        double dist = std::sqrt(dist2);
        double weight = kernel(dist, h[i]);

        double Vni = mass[ni] / Dstar[ni];

        tau_tmp += Vni * ((*pos_ptr[k])[ni] - (*pos_ptr[k])[i]) *
                   ((*pos_ptr[d])[ni] - (*pos_ptr[d])[i]) * weight;
      }
      tau[k][d] = tau_tmp;
    }
  }

  C = invert_dim_matrix(tau);

  for (int k = 0; k < DIM; ++k) {
    for (int d = 0; d < DIM; ++d) {
      (*C_ptr[k][d])[i] = C[k][d];
    }
  }
}

void grsph::particle_rhs(const size_t i) {
  if (ghost[i]) return;

  if (iter % static_cast<int>(std::pow(2, tlevel[i])) != 0) return;

  for (size_t d = 0; d < DIM; ++d) {
    (*Srhs_ptr[d])[i] = 0.;
  }

  Ebarrhs[i] = 0.;

  q[i] = 0.;

  for (const auto& ni : neighbors[i]) {
    double dist2 = fast_dist2(i, ni);
    double dist = std::sqrt(dist2);
    double weight = kernel(dist, h[i]);
    double nweight = kernel(dist, h[ni]);

    std::array<double, DIM> Ga, Gb;

    for (size_t k = 0; k < DIM; ++k) {
      Ga[k] = 0.;
      Gb[k] = 0.;
      for (size_t d = 0; d < DIM; ++d) {
        Ga[k] +=
            (*C_ptr[k][d])[i] * ((*pos_ptr[d])[ni] - (*pos_ptr[d])[i]) * weight;
        Gb[k] += (*C_ptr[k][d])[ni] * ((*pos_ptr[d])[ni] - (*pos_ptr[d])[i]) *
                 nweight;
      }
    }

    double vijdotrij = 0;
    for (size_t d = 0; d < DIM; ++d) {
      double rij = (*pos_ptr[d])[i] - (*pos_ptr[d])[ni];
      double vij = (*vel_ptr[d])[i] - (*vel_ptr[d])[ni];
      vijdotrij += rij * vij;
    }

    // Artificial viscous pressure
    double qab = 0., qba = 0.;

    double hij = (h[i] + h[ni]) * 0.5;
    double approxdivv = vijdotrij / (dist2 + 0.01 * hij * hij);

    double alphaij = (alpha[i] + alpha[ni]) * 0.5;
    double betaij = 2. * alphaij;

    if (approxdivv < 0.) {
      qab = rho[i] * enth[i] * (-alphaij * cs[i] * h[i] * approxdivv +
                                betaij * h[i] * h[i] * approxdivv * approxdivv);
      qba = rho[ni] * enth[ni] *
            (-alphaij * cs[ni] * h[ni] * approxdivv +
             betaij * h[ni] * h[ni] * approxdivv * approxdivv);

      q[i] += 0.5 * mass[ni] *
              (qab / Dstar[i] * weight + qba / Dstar[ni] * nweight);
    }

    // Signal velocity based artifical viscosity
    // Currently we just calculate it and then zero it out because
    // factoring it in Tmunu is a pain whereas P -> P + q is easy
    //
    // Also many of the variable names don't make sense since
    // this was copied from the SR code

    std::array<double, DIM> unit, rij, vij;
    std::array<double, DIM> Gmean;

    // double vijdotrij = 0.;
    double vdotunit = 0.;
    double vdotunitN = 0.;
    double v2 = 0.;
    double v2N = 0.;
    for (size_t d = 0; d < DIM; ++d) {
      rij[d] = (*pos_ptr[d])[i] - (*pos_ptr[d])[ni];
      vij[d] = (*vel_ptr[d])[i] - (*vel_ptr[d])[ni];
      unit[d] = rij[d] / dist;
      Gmean[d] = 0.5 * (Ga[d] + Gb[d]);

      //  vijdotrij += vij[d] * rij[d];
      vdotunit += (*vel_ptr[d])[i] * unit[d];
      vdotunitN += (*vel_ptr[d])[ni] * unit[d];
      v2 += (*vel_ptr[d])[i] * (*vel_ptr[d])[i];
      v2N += (*vel_ptr[d])[ni] * (*vel_ptr[d])[ni];
    }
#if 0
    double rhoij = (D[i] + Dstar[ni]) * 0.5;

    double visc = 0.;

    double W_star = 1. / std::sqrt(1. - vdotunit * vdotunit);

    std::array<double, DIM> S_star;
    for (size_t d = 0; d < DIM; ++d) {
      S_star[d] = W_star * (*S_ptr[d])[i] / W[i];
    }

    double eps_star = W_star / W[i] * (Ebar[i] + P[i] / Dstar[i]) - P[i] / Dstar[i];

    double vpar = vdotunit;
    double vperp2 = 0.;
    for (size_t d = 0; d < DIM; ++d) {
      vperp2 += ((*vel_ptr[d])[i] - vpar * unit[d]) *
                ((*vel_ptr[d])[i] - vpar * unit[d]);
    }
    double lambdap =
        (vpar * (1. - cs[i] * cs[i]) +
         cs[i] *
             std::sqrt((1. - v2) * (1. - vpar * vpar - vperp2 * cs[i] * cs[i]))) /
        (1. - v2 * cs[i] * cs[i]);
    double lambdan =
        (vpar * (1. - cs[i] * cs[i]) -
         cs[i] *
             std::sqrt((1. - v2) * (1. - vpar * vpar - vperp2 * cs[i] * cs[i]))) /
        (1. - v2 * cs[i] * cs[i]);
    double alp = std::max(-lambdan, lambdap);

    // values for ni (precompute alp? this currently does 2* the number of
    // calcs it needs too... i.e. not aware of index sym)
    //      double vdotunitN = vx[ni]*unit[0] + vy[ni]*unit[1] +
    //      vz[ni]*unit[2];
    double W_starN = 1. / std::sqrt(1. - vdotunitN * vdotunitN);
    std::array<double, DIM> S_starN;
    for (size_t d = 0; d < DIM; ++d) {
      S_starN[d] = W_starN * (*S_ptr[d])[ni] / W[ni];
    }

    double eps_starN =
        W_starN / W[ni] * (Ebar[ni] + P[ni] / Dstar[ni]) - P[ni] / Dstar[ni];

    //      double v2N = vx[ni]*vx[ni] + vy[ni]*vy[ni] + vz[ni]*vz[ni];
    double vparN = vdotunitN;
    double vperp2N = 0.;
    for (size_t d = 0; d < DIM; ++d) {
      vperp2N += ((*vel_ptr[d])[ni] - vparN * unit[d]) *
                 ((*vel_ptr[d])[ni] - vparN * unit[d]);
    }

    double csN = cs[i];  // std::sqrt((eos_gamma- 1.) * (enth[ni] - 1.) / enth[ni]);
    double lambdapN =
        (vparN * (1. - csN * csN) +
         csN * std::sqrt((1. - v2N) * (1. - vparN * vparN - vperp2N * csN * csN))) /
        (1. - v2N * csN * csN);
    double lambdanN =
        (vparN * (1. - csN * csN) -
         csN * std::sqrt((1. - v2N) * (1. - vparN * vparN - vperp2N * csN * csN))) /
        (1. - v2N * csN * csN);
    double alpN = std::max(-lambdanN, lambdapN);

    alp = std::max(alp, alpN);
    alp = std::max(alp, 0.);
    double vsig = alp;
    //      if (vsig > maxvsig) maxvsig = vsig;
    //      if (h[ni] < minh) minh = h[ni];

    std::array<double, DIM> S_starij;
    double S_starijdotunit = 0.;
    std::array<double, DIM> Gmean;
    for (size_t d = 0; d < DIM; ++d) {
      S_starij[d] = S_star[d] - S_starN[d];
      S_starijdotunit += S_starij[d] * unit[d];
      Gmean[d] = 0.5 * (Ga[d] + Gb[d]);
    }
    double eps_starij = eps_star - eps_starN;

    if (vijdotrij < 0.) {
      visc = -alphaij * vsig / rhoij;
    }

    // qab = 0.;
    // qba = 0.;
    // q[i] = 0.;
    double unitdotGmean = 0.;
    double avgDspline = 1.;
    double vdiss = -mass[ni] * visc * S_starijdotunit * avgDspline;
    double udiss = -mass[ni] * visc * eps_starij * avgDspline;
#endif
    double unitdotGmean = 0.;
    double udiss = 0.;
    double vdiss = 0.;
    // End of the signal velocity artifical viscosity

    // These variables allow for different weighting in the SPH interp
    // Currently they are jus the 'standard' prescription
    double Xa = mass[i];
    double Xb = mass[ni];
    double Va = mass[i] / Dstar[i];
    double Vb = mass[ni] / Dstar[ni];
    double vbdotGa = 0.;
    double vadotGb = 0.;
    // parts of this can be computed outside the loop
    for (size_t d = 0; d < DIM; ++d) {
      (*Srhs_ptr[d])[i] += (P[i] + qab) * Va * Va * Xb / Xa * Ga[d] +
                           (P[ni] + qba) * Vb * Vb * Xa / Xb * Gb[d];

      (*Srhs_ptr[d])[i] += -mass[i] * vdiss * Gmean[d];

      vbdotGa += (*vel_ptr[d])[ni] * Ga[d];
      vadotGb += (*vel_ptr[d])[i] * Gb[d];
      unitdotGmean += unit[d] * Gmean[d];
    }
    Ebarrhs[i] += (P[i] + qab) * Va * Va * Xb / Xa * vbdotGa +
                  (P[ni] + qba) * Vb * Vb * Xa / Xb * vadotGb;

    Ebarrhs[i] += -mass[i] * udiss * unitdotGmean;
  }  // NN loop

  for (size_t d = 0; d < DIM; ++d) {
    (*Srhs_ptr[d])[i] /= -mass[i];
  }
  Ebarrhs[i] /= -mass[i];

  // GR terms
  std::array<double, 3> pos = {{}};
  for (size_t d = 0; d < DIM; ++d) {
    pos[d] = (*pos_ptr[d])[i];
  }

  double det = std::sqrt(-spacetime->det(pos));
  double lapse = spacetime->lapse(pos);
  auto metU = spacetime->gmunuU(pos);
  auto dmet = spacetime->gmunudx(pos);

  std::array<double, DIM + 1> uu;
  uu[0] = W[i] / lapse;
  for (size_t d = 1; d < DIM + 1; ++d) {
    uu[d] = W[i] / lapse * (*vel_ptr[d - 1])[i];
  }

  double prefac = det / 2. / Dstar[i];  /// omega[i];
  double vfac = prefac * (rho[i] * enth[i] + q[i]);
  double Pfac = prefac * (P[i] + q[i]);
  for (size_t d = 0; d < DIM; ++d) {
    for (size_t d2 = 0; d2 < DIM + 1; ++d2) {
      for (size_t d3 = 0; d3 < DIM + 1; ++d3) {
        (*Srhs_ptr[d])[i] +=
            (vfac * uu[d2] * uu[d3] + Pfac * metU[d2][d3]) * dmet[d2][d3][d];
        // This is where I would put my metric time derivative terms if I had
        // any...
        // Ebar[i] += ...
      }
    }
  }
}
