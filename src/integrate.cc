// Copyright 2016 Matt Kinsey

// Handle the time integration
// TODO TODO TODO leapfrog doesnt work with mpi

#include "grsph.h"
#include "intpow.h"

namespace grsph {

int iter = 0;
double dt = 0;
double t = 0;
double tmax = 1;
int h5_out_every = 0;
int recalc_ORB_boxes_every = 1;
int update_ORB_boxes_every = 1;
int sort_every = 1;
int print_timers_every = 100;
int print_conservation_every = 1;
int print_basic_every = 1;
double clean_inside_r = 0.;  // todo this is very hackish
int maxtl = 0;

std::function<void()> rhs_func;
std::function<void()> integrator_func;

void rk2_step1(const size_t i);
void rk2_step2(const size_t i);

void rk3_step1(const size_t i);
void rk3_step2(const size_t i);
void rk3_step3(const size_t i);

void particle_leapfrog_start(const size_t i);
void particle_leapfrog_step(const size_t i);
};  // namespace grsph

// This is a wrapper function to take one full step by dt and
//   do all the fun things that come along with that
void grsph::take_step() {
  auto begin = clock::now();

  if (mpi_rank == 0 && verbose > 0 && print_basic_every &&
      fmod(iter, print_basic_every) == 0) {
    std::cout << "-----------------------------------------------------"
              << std::endl
              << "Taking step at time " << t << ", dt=" << dt << std::endl;
  }

  // Reset timers
  clear_timers();

  // Do the actual work, update from t to t+dt
  integrator_func();
  t += dt;

  // Check dt criteria
  particle_loop(check_dt);

  // IO and clear_ghosts
  if (h5_output_ghosts) {
    if (h5_out_every && fmod(iter, h5_out_every) == 0) output_h5part(t);
    clear_ghosts();
  } else {
    clear_ghosts();
    if (h5_out_every && fmod(iter, h5_out_every) == 0) output_h5part(t);
  }

  // Clean out particle with r < clean_inside_r
  // todo this is really hackish...
  if (clean_inside_r) {
    size_t np = x.size();
    for (size_t i = 0; i < np; ++i) {
      double r2 = 0.;
      for (size_t d = 0; d < DIM; ++d) {
        r2 += (*pos_ptr[d])[i] * (*pos_ptr[d])[i];
      }
      if (r2 + support_r * support_r * h[i] * h[i] <
          clean_inside_r * clean_inside_r) {
        ghost[i] = 1;
      }
    }
    clear_ghosts();
  }

  // Print conserved values
  if (print_conservation_every && fmod(iter, print_conservation_every) == 0)
    print_conservation();

  // Recalc proc boxes
  if (recalc_ORB_boxes_every && fmod(iter, recalc_ORB_boxes_every) == 0)
    calc_ORB_boxes();

  // Update boxes based on cpu timers
  if (update_ORB_boxes_every && fmod(iter, update_ORB_boxes_every) == 0)
    update_ORB_boxes();

  // Resync particles to mpi owners
  sync_particles_to_procs();

  // Sort particles
  if (sort_every && fmod(iter, sort_every) == 0) sort_particles();

  // Print timers
  if (print_timers_every && fmod(iter, print_timers_every) == 0 &&
      mpi_rank == 0)
    print_timers();

  iter++;

  if (mpi_rank == 0 && verbose > 0 && print_basic_every &&
      fmod(iter, print_basic_every) == 0) {
    auto end = clock::now();
    double step_time =
        std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin)
            .count();

    // todo step time should be an average of steps since last output
    // todo use chrono cast instead of division
    step_time /= 1000000000.;

    std::cout << step_time << "s " << step_time / x.size() / mpi_size
              << "s/(local # parts*mpi_size) [ " << dt / (step_time / 3600)
              << "t/h, done in " << (tmax - t) / dt * step_time << "s ]"
              << std::endl;
  }
}

// Check that dt statisfies criteria
void grsph::check_dt(const size_t i) {
  if (ghost[i]) return;

  double Srhs2 = 0;
  for (size_t d = 0; d < DIM; ++d) {
    Srhs2 += (*Srhs_ptr[d])[i] * (*Srhs_ptr[d])[i];
  }
  double Srhsmag = std::sqrt(Srhs2);

  double maxdt, dtf, dtcfl;
  dtf = 0.3 * std::sqrt(h[i] / Srhsmag);
  // is dtcfl right? its the newtonian version
  dtcfl = 0.3 * h[i] /
          (cs[i] + h[i] * std::abs(divv[i]) +
           1.2 * (alpha[i] * cs[i] + 2. * alpha[i] * h[i] * std::abs(divv[i])));
  maxdt = std::min(dtf, dtcfl);

  int newtl = static_cast<int>(std::log2(maxdt / dt));

  bool baddt = dt > maxdt;

  if (baddt) {
    std::cout << "dt=" << dt << " is too large!" << std::endl;
    std::cout << "dtf=" << dtf << std::endl;
    std::cout << "dtcfl=" << dtcfl << std::endl;
    assert(0);
  }

  if (newtl < tlevel[i]) {
    tlevel[i] = newtl;
  }
  if (newtl > tlevel[i]) {
    if (newtl < maxtl &&
        (iter % static_cast<int>(std::pow(2, tlevel[i] + 1))) == 0) {
      tlevel[i]++;
    }
  }
}

void grsph::tvd_rk2_step() {
  rhs_func();
  particle_loop(rk2_step1);

  rhs_func();
  particle_loop(rk2_step2);
}

void grsph::rk2_step1(const size_t i) {
  if (ghost[i]) return;

  // Store tmps and step by dt
  for (size_t d = 0; d < DIM; ++d) {
    (*postmp_ptr[d])[i] = (*pos_ptr[d])[i];
    (*Stmp_ptr[d])[i] = (*S_ptr[d])[i];
    (*pos_ptr[d])[i] += (*vel_ptr[d])[i] * dt;
    (*S_ptr[d])[i] += (*Srhs_ptr[d])[i] * dt;
  }

  Ebartmp[i] = Ebar[i];
  alphatmp[i] = alpha[i];
  Ebar[i] += Ebarrhs[i] * dt;
  alpha[i] += alpharhs[i] * dt;
}

void grsph::rk2_step2(const size_t i) {
  if (ghost[i]) return;

  for (size_t d = 0; d < DIM; ++d) {
    (*pos_ptr[d])[i] = 0.5 * (*postmp_ptr[d])[i] + 0.5 * (*pos_ptr[d])[i] +
                       0.5 * (*vel_ptr[d])[i] * dt;
    (*S_ptr[d])[i] = 0.5 * (*Stmp_ptr[d])[i] + 0.5 * (*S_ptr[d])[i] +
                     0.5 * (*Srhs_ptr[d])[i] * dt;
  }

  Ebar[i] = 0.5 * Ebartmp[i] + 0.5 * Ebar[i] + 0.5 * Ebarrhs[i] * dt;
  alpha[i] = 0.5 * alphatmp[i] + 0.5 * alpha[i] + 0.5 * alpharhs[i] * dt;
}

// Advance state arrays from t to t+dt using tvd rk3
void grsph::tvd_rk3_step() {
  rhs_func();
  particle_loop(rk3_step1);

  rhs_func();
  particle_loop(rk3_step2);

  rhs_func();
  particle_loop(rk3_step3);
}

void grsph::rk3_step1(const size_t i) {
  if (ghost[i]) return;

  // Store tmps and step by dt
  for (size_t d = 0; d < DIM; ++d) {
    (*postmp_ptr[d])[i] = (*pos_ptr[d])[i];
    (*Stmp_ptr[d])[i] = (*S_ptr[d])[i];
    (*pos_ptr[d])[i] += (*vel_ptr[d])[i] * dt;
    (*S_ptr[d])[i] += (*Srhs_ptr[d])[i] * dt;
  }

  Ebartmp[i] = Ebar[i];
  alphatmp[i] = alpha[i];
  Ebar[i] += Ebarrhs[i] * dt;
  alpha[i] += alpharhs[i] * dt;
}

void grsph::rk3_step2(const size_t i) {
  if (ghost[i]) return;

  for (size_t d = 0; d < DIM; ++d) {
    (*pos_ptr[d])[i] = 0.75 * (*postmp_ptr[d])[i] + 0.25 * (*pos_ptr[d])[i] +
                       0.25 * (*vel_ptr[d])[i] * dt;
    (*S_ptr[d])[i] = 0.75 * (*Stmp_ptr[d])[i] + 0.25 * (*S_ptr[d])[i] +
                     0.25 * (*Srhs_ptr[d])[i] * dt;
  }

  Ebar[i] = 0.75 * Ebartmp[i] + 0.25 * Ebar[i] + 0.25 * Ebarrhs[i] * dt;
  alpha[i] = 0.75 * alphatmp[i] + 0.25 * alpha[i] + 0.25 * alpharhs[i] * dt;
}

void grsph::rk3_step3(const size_t i) {
  if (ghost[i]) return;

  double div3 = 1. / 3.;

  for (size_t d = 0; d < DIM; ++d) {
    (*pos_ptr[d])[i] = div3 * (*postmp_ptr[d])[i] +
                       2. * div3 * (*pos_ptr[d])[i] +
                       2. * div3 * (*vel_ptr[d])[i] * dt;
    (*S_ptr[d])[i] = div3 * (*Stmp_ptr[d])[i] + 2. * div3 * (*S_ptr[d])[i] +
                     2. * div3 * (*Srhs_ptr[d])[i] * dt;
  }

  Ebar[i] =
      div3 * Ebartmp[i] + 2. * div3 * Ebar[i] + 2. * div3 * Ebarrhs[i] * dt;
  alpha[i] =
      div3 * alphatmp[i] + 2. * div3 * alpha[i] + 2. * div3 * alpharhs[i] * dt;
}

void grsph::leapfrog_step() {
  rhs_func();
  if (iter == 0) {
    particle_loop(particle_leapfrog_start);
    particle_loop(particle_leapfrog_step);
  } else {
    particle_loop(particle_leapfrog_step);
  }
}

void grsph::particle_leapfrog_start(const size_t i) {
  if (ghost[i]) return;

  for (size_t d = 0; d < DIM; ++d) {
    (*pos_ptr[d])[i] += (*vel_ptr[d])[i] * dt;
    (*Stmp_ptr[d])[i] = (*S_ptr[d])[i] + (*Srhs_ptr[d])[i] * dt / 2.0;
    (*S_ptr[d])[i] += (*Srhs_ptr[d])[i] * dt;
  }

  Ebartmp[i] = Ebar[i] + Ebarrhs[i] * dt / 2.0;
  Ebar[i] += Ebarrhs[i] * dt;
  alphatmp[i] = alpha[i] + alpharhs[i] * dt / 2.0;
  alpha[i] += alpharhs[i] * dt;
}

void grsph::particle_leapfrog_step(const size_t i) {
  if (ghost[i]) return;

  for (size_t d = 0; d < DIM; ++d) {
    (*pos_ptr[d])[i] += (*vel_ptr[d])[i] * dt;
    (*Stmp_ptr[d])[i] += (*Srhs_ptr[d])[i] * dt;
    (*S_ptr[d])[i] = (*Stmp_ptr[d])[i] + (*Srhs_ptr[d])[i] * dt / 2.0;
  }

  Ebartmp[i] += Ebarrhs[i] * dt;
  Ebar[i] = Ebartmp[i] + Ebarrhs[i] * dt / 2.0;
  alphatmp[i] += alpharhs[i] * dt;
  alpha[i] = alphatmp[i] + alpharhs[i] * dt / 2.0;
}

// TODO leapfrog tmp vars not commed by MPI (because tvd uses them as legit tmp
// vars)
