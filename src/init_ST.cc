// Copyright 2016 Matt Kinsey

// Initialize the 'surface tension' test, a triangular region of high density.

#include "grsph.h"

void grsph::init_STtest() {
#if dim2
  size_t np = x.size();

  for (size_t i = 0; i < np; ++i) {
    if (y[i] >= -std::sqrt(3.) / 4. &&
        y[i] <= std::sqrt(3.) * x[i] + std::sqrt(3.) / 4. &&
        y[i] <= -std::sqrt(3.) * x[i] + std::sqrt(3.) / 4.) {
      mass[i] = 4. / np;
      Dstar[i] = 1.;
    } else {
      mass[i] = 8. / np;
      Dstar[i] = 2.;
    }

    double v2 = 0.;
    for (size_t d = 0; d < DIM; ++d) {
      v2 += (*vel_ptr[d])[i] * (*vel_ptr[d])[i];
    }
    W[i] = 1. / std::sqrt(1. - v2);

    P[i] = 2.5;
    u[i] = P[i] * W[i] / (eos_gamma - 1.) / Dstar[i];
    h[i] = eta * std::pow(mass[i], 1. / DIM);
  }
#else
  assert(0);
#endif
}
