// Copyright 2016 Matt Kinsey

// This is a class represting a flat Minkowski spacetime

#ifndef SRC_MINKOWSKI_H_
#define SRC_MINKOWSKI_H_

#include "spacetime.h"

class Minkowski : public Spacetime {
 public:
  double lapse(const std::array<double, 3>& pos) override { return 1.0; }

  std::array<double, DIM> shift(const std::array<double, 3>& pos) override {
    std::array<double, DIM> beta = {};
    return beta;
  }

  std::array<std::array<double, DIM>, DIM> gammaij(
      const std::array<double, 3>& pos) override {
    std::array<std::array<double, DIM>, DIM> met = {{}};

    for (size_t i = 0; i < DIM; ++i) {
      met[i][i] = 1.;
    }

    return met;
  }

  std::array<std::array<double, DIM>, DIM> kij(
      const std::array<double, 3>& pos) override {
    std::array<std::array<double, DIM>, DIM> kij = {{}};
    return kij;
  }

  double det(const std::array<double, 3>& pos) override { return -1.; }

  double sdet(const std::array<double, 3>& pos) override { return 1.; }

  std::array<std::array<double, DIM + 1>, DIM + 1> gmunuU(
      const std::array<double, 3>& pos) override {
    std::array<std::array<double, DIM + 1>, DIM + 1> met = {{}};

    met[0][0] = -1.;

    for (size_t i = 1; i < DIM + 1; ++i) {
      met[i][i] = 1.;
    }

    return met;
  }

  std::array<std::array<double, DIM + 1>, DIM + 1> gmunuL(
      const std::array<double, 3>& pos) override {
    std::array<std::array<double, DIM + 1>, DIM + 1> met = {{}};

    met[0][0] = -1.;

    for (size_t i = 1; i < DIM + 1; ++i) {
      met[i][i] = 1.;
    }

    return met;
  }

  std::array<std::array<std::array<double, DIM>, DIM + 1>, DIM + 1> gmunuUdx(
      const std::array<double, 3>& pos) override {
    std::array<std::array<std::array<double, DIM>, DIM + 1>, DIM + 1> gdx = {
        {{}}};

    return gdx;
  }

  std::array<std::array<std::array<double, DIM>, DIM + 1>, DIM + 1> gmunudx(
      const std::array<double, 3>& pos) override {
    std::array<std::array<std::array<double, DIM>, DIM + 1>, DIM + 1> gdx = {
        {{}}};

    return gdx;
  }
};

#endif  // SRC_MINKOWSKI_H_
