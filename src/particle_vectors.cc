// Copyright 2016 Matt Kinsey

#include "grsph.h"

namespace grsph {

particle_vector<double> x, Sx, vx;
particle_vector<double> Sxrhs, xtmp, vxtmp, Sxtmp, C00;
#if dim2
particle_vector<double> y, Sy, vy;
particle_vector<double> Syrhs, ytmp, vytmp, Sytmp, C01, C10, C11;
#endif
#if dim3
particle_vector<double> z, Sz, vz;
particle_vector<double> Szrhs, ztmp, vztmp, Sztmp, C02, C12, C20, C21, C22;
#endif

particle_vector<double> mass, h, P, u;
particle_vector<double> rho;
particle_vector<double> omega, alpha;
particle_vector<double> nnnp;
particle_vector<double> ghost;
particle_vector<double> W, Ebar, Dstar, enth, divv, divvdt;
particle_vector<double> q, E, cs;
particle_vector<double> tlevel;

particle_vector<double> debug;

// rhs/tmp vars
particle_vector<double> Ebarrhs, alpharhs;
particle_vector<double> Ebartmp, alphatmp;

// ptr vars
#if DIM == 1
const std::array<particle_vector<double> *, DIM> pos_ptr = {{&x}};
const std::array<particle_vector<double> *, DIM> vel_ptr = {{&vx}};
const std::array<particle_vector<double> *, DIM> S_ptr = {{&Sx}};
const std::array<particle_vector<double> *, DIM> Srhs_ptr = {{&Sxrhs}};
const std::array<particle_vector<double> *, DIM> postmp_ptr = {{&xtmp}};
const std::array<particle_vector<double> *, DIM> veltmp_ptr = {{&vxtmp}};
const std::array<particle_vector<double> *, DIM> Stmp_ptr = {{&Sxtmp}};
const std::array<std::array<particle_vector<double> *, DIM>, DIM> C_ptr = {
    {{{&C00}}}};
const std::array<unsigned int, DIM + 1> large_primes = {{73856093, 19349663}};
#endif
#if DIM == 2
const std::array<particle_vector<double> *, DIM> pos_ptr = {{&x, &y}};
const std::array<particle_vector<double> *, DIM> vel_ptr = {{&vx, &vy}};
const std::array<particle_vector<double> *, DIM> S_ptr = {{&Sx, &Sy}};
const std::array<particle_vector<double> *, DIM> Srhs_ptr = {{&Sxrhs, &Syrhs}};
const std::array<particle_vector<double> *, DIM> postmp_ptr = {{&xtmp, &ytmp}};
const std::array<particle_vector<double> *, DIM> veltmp_ptr = {
    {&vxtmp, &vytmp}};
const std::array<particle_vector<double> *, DIM> Stmp_ptr = {{&Sxtmp, &Sytmp}};
const std::array<std::array<particle_vector<double> *, DIM>, DIM> C_ptr = {
    {{{&C00, &C01}}, {{&C10, &C11}}}};
const std::array<unsigned int, DIM + 1> large_primes = {
    {73856093, 19349663, 83492791}};
#endif
#if DIM == 3
const std::array<particle_vector<double> *, DIM> pos_ptr = {{&x, &y, &z}};
const std::array<particle_vector<double> *, DIM> vel_ptr = {{&vx, &vy, &vz}};
const std::array<particle_vector<double> *, DIM> S_ptr = {{&Sx, &Sy, &Sz}};
const std::array<particle_vector<double> *, DIM> Srhs_ptr = {
    {&Sxrhs, &Syrhs, &Szrhs}};
const std::array<particle_vector<double> *, DIM> postmp_ptr = {
    {&xtmp, &ytmp, &ztmp}};
const std::array<particle_vector<double> *, DIM> veltmp_ptr = {
    {&vxtmp, &vytmp, &vztmp}};
const std::array<particle_vector<double> *, DIM> Stmp_ptr = {
    {&Sxtmp, &Sytmp, &Sztmp}};
const std::array<std::array<particle_vector<double> *, DIM>, DIM> C_ptr = {
    {{{&C00, &C01, &C02}}, {{&C10, &C11, &C12}}, {{&C20, &C21, &C22}}}};
const std::array<unsigned int, DIM + 1> large_primes = {
    {73856093, 19349663, 83492791, 67867979}};
#endif

};  // namespace grsph
