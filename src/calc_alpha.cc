// Copyright 2016 Matt Kinsey

#include "grsph.h"

namespace grsph {

double alpha_min = 0;
double alpha_max = 1.5;
double N_noise = 50;
void particle_alpha(const size_t i);
};  // namespace grsph

// Wrapper function to calculate dissipation trigger, alpha
void grsph::calc_alpha() {
  start_timer("calc_alpha");
  particle_loop(particle_alpha);
  stop_timer("calc_alpha");
}

void grsph::particle_alpha(const size_t i) {
  // Calc diss triggers and then alpha/alpharhs
  // from http://arxiv.org/pdf/1405.6034v2.pdf
  if (ghost[i]) return;
  double S1 = 0.;
  double S2 = 0.;
  double Splus = 0.;
  double Sminus = 0.;
  int numSplus = 0;
  int numSminus = 0;

  for (const auto& ni : neighbors[i]) {
    double dist2 = fast_dist2(i, ni);

    // Skip particles outside of kernel
    double ihdist2 = support_r * support_r * h[i] * h[i];
    double nihdist2 = support_r * support_r * h[ni] * h[ni];

    if (dist2 > ihdist2 && dist2 > nihdist2) continue;

    S1 += divv[ni];
    S2 += std::abs(divv[ni]);

    if (divv[ni] > 0) {
      numSplus++;
      Splus += W[ni] * divv[ni];
    }

    if (divv[ni] < 0) {
      numSminus++;
      Sminus += -W[ni] * divv[ni];
    }
  }

  if (numSplus) Splus /= static_cast<double>(numSplus);
  if (numSminus) Sminus /= static_cast<double>(numSminus);

  // Calculate alpha_shock
  double Ashock = std::max(-divvdt[i], 0.);
  // double Ashock = std::max(-divv[i],0.);

  double alpha_shock =
      alpha_max * Ashock / (Ashock + cs[i] * cs[i] / h[i] / h[i]);

  // Calculate alpha_noise
  if (divv[i] < 0.) S1 *= -1.;
  double Nnoise1 = std::abs(S1 / S2 - 1.);
  if (std::isnan(Nnoise1)) Nnoise1 = 0.;  // is this needed still?
  double kappa1 = Nnoise1 / (Nnoise1 + N_noise);

  double Nnoise2 = std::sqrt(Splus * Sminus);
  double kappa2 = Nnoise2 / (Nnoise2 + 0.2 * cs[i] / h[i]);

  double alpha_noise = alpha_max * std::max(kappa1, kappa2);

  // Pick the max of alpha_noise and alpha_shock
  double alpha_tilde = std::max(
      alpha_shock, alpha_noise);  // todo bad name (just like alpha_max)

  // Instantly set alpha to the desired value
  if (alpha_tilde > alpha[i]) {
    alpha[i] = alpha_tilde;
    alphatmp[i] = alpha[i];
  }

  if (alpha[i] < alpha_min) {
    alpha[i] = alpha_min;
  }

  // Set rhs to decay
  double taua = 2. * h[i] / cs[i];
  alpharhs[i] = -alpha[i] / taua;
}
