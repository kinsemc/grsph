// Copyright 2016 Matt Kinsey

// This file is used to generate 'glass-like' ICs. TODO more info

#include "grsph.h"

namespace grsph {
bool init_glass = 0;
double perturb_max = 1.;
double glass_max_err = 999.;
int glass_out_every = 1;

double err;
double V0;
double rs;
double fmagmax;

void particle_calc_f(const size_t i);
void particle_push_glass(const size_t i);
};  // namespace grsph

void grsph::glassify_mesh(const double tol) {
  int total_np = global_np();
  size_t np = x.size();

  calc_global_box();

  err = -1.;

  double xlen = global_box[1] - global_box[0];
  rs = .2 * xlen / total_np;
  V0 = 2. * rs;
#if dim2
  double ylen = global_box[3] - global_box[2];
  rs = std::sqrt(xlen * ylen / total_np);
  V0 = 3.14159 * rs * rs;
//  V0 = 2. * std::sqrt(3.) * rs * rs;
#endif
#if dim3
  double zlen = global_box[5] - global_box[4];
  rs = std::pow(xlen * ylen * zlen / total_np, 1. / 3.);
  // V0 = xlen * ylen * zlen / total_np;
  V0 = 4. / 3. * 3.14159 * rs * rs * rs;
#endif

#pragma omp parallel for
  for (size_t i = 0; i < np; ++i) {
    h[i] = eta * std::pow(V0, 1. / DIM);
  }

  int giter = 0;
  do {
    // Clear the old ghosts before we get new ones
    clear_ghosts();
    calc_ORB_boxes();
    sync_particles_to_procs();

    // Build the ghost free hashtable
    build_hashtable();

    // Periodic ghosts
    if (periodic) {
      find_periodic_ghosts();
    }  // else {
    //      periodic_box = {{0,1,0,1,0,1}};
    //      periodic = true;
    //      find_periodic_ghosts();
    //    }

    // MPI sync ghosts
    find_ghosts();

    // Find neighbors
    build_neighbor_list();  // ONLY DO ONCE?!

    // Calculate and sync C for all particles
    start_timer("Ccalc");
    particle_loop(particle_rhs_Ccalc);

    for (int k = 0; k < DIM; ++k) {
      for (int d = 0; d < DIM; ++d) {
        ghost_sync_particlevec((*C_ptr[k][d]));
      }
    }
    stop_timer("Ccalc");

    particle_loop(particle_calc_f);

    clear_ghosts();
    np = x.size();

    if (giter == 0) {
      double local_fmagmax = -1.;

      if (!x.empty()) {
#pragma omp parallel for reduction(+ : local_fmagmax)
        for (size_t i = 0; i < np; ++i) {
          double fmag2 = 0;
          for (size_t d = 0; d < DIM; ++d) {
            fmag2 += (*vel_ptr[d])[i] * (*vel_ptr[d])[i];
          }
          double fmag = std::sqrt(fmag2);
          local_fmagmax = std::max(local_fmagmax, fmag);
        }
      } else {
        local_fmagmax = std::numeric_limits<double>::min();
      }
      MPI_Allreduce(&local_fmagmax, &fmagmax, 1, MPI_DOUBLE, MPI_MAX,
                    MPI_COMM_GRSPH);
    }

    particle_loop(particle_push_glass);

    sync_particles_to_procs();

    double local_err = -1;
    for (size_t i = 0; i < np; ++i) {
      double fmag2 = 0;
      for (size_t d = 0; d < DIM; ++d) {
        fmag2 += (*vel_ptr[d])[i] * (*vel_ptr[d])[i];
      }
      double fmag = std::sqrt(fmag2);
      local_err = std::max(.9 / fmagmax * fmag, local_err);
    }

    MPI_Allreduce(&local_err, &err, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_GRSPH);

    if (glass_out_every && giter % glass_out_every == 0) output_h5part(0);

    if (mpi_rank == 0)
      std::cout << "Glass iter #" << giter << ": err = " << err << std::endl;
    giter++;
  } while (err > tol);

  // Zero out the tmp \vec{v}
  for (size_t d = 0; d < DIM; ++d) {
#pragma omp parallel for
    for (size_t i = 0; i < np; ++i) {
      (*vel_ptr[d])[i] = 0.;
    }
  }

  // sync_particles_to_procs();
}

void grsph::particle_calc_f(const size_t i) {
  if (ghost[i]) return;

  // Put vec{f} in the v vars
  for (size_t d = 0; d < DIM; ++d) {
    (*vel_ptr[d])[i] = 0.;
  }

  for (const auto& ni : neighbors[i]) {
    double dist2 = fast_dist2(i, ni);
    double dist = std::sqrt(dist2);

    double weight = kernel(dist, h[i]);
    double nweight = kernel(dist, h[ni]);

    std::array<double, DIM> Ga, Gb;

    for (size_t k = 0; k < DIM; ++k) {
      Ga[k] = 0.;
      Gb[k] = 0.;
      for (size_t d = 0; d < DIM; ++d) {
        Ga[k] +=
            (*C_ptr[k][d])[i] * ((*pos_ptr[d])[ni] - (*pos_ptr[d])[i]) * weight;
        Gb[k] += (*C_ptr[k][d])[ni] * ((*pos_ptr[d])[ni] - (*pos_ptr[d])[i]) *
                 nweight;
      }

      (*vel_ptr[k])[i] += -.5 * (Ga[k] + Gb[k]);
    }
  }
}

void grsph::particle_push_glass(const size_t i) {
  if (ghost[i]) return;

  double r0;
  if (err == -1) {  // first iter
    r0 = .5 * rs;
  } else {
    r0 = 0.9 * rs;
  }

  for (size_t d = 0; d < DIM; ++d) {
    (*pos_ptr[d])[i] += r0 / fmagmax * (*vel_ptr[d])[i];
  }
}
