// Copyright 2016 Matt Kinsey

// TODO dont use this, its not vetted for correctness

#include "grsph.h"

void grsph::particle_grc2p_poly(const size_t i) {
  if (ghost[i]) return;

  std::array<double, 3> pos = {{}};
  for (size_t d = 0; d < DIM; ++d) {
    pos[d] = (*pos_ptr[d])[i];
  }

  double sdet = std::sqrt(spacetime->sdet(pos));
  auto metU = spacetime->gmunuU(pos);  // full metric g^munu
  double lapse = spacetime->lapse(pos);
  auto shift = spacetime->shift(pos);

  std::vector<std::vector<double>> gammaU(DIM, std::vector<double>(DIM));

  for (int ii = 1; ii < DIM + 1; ++ii) {
    for (int jj = 1; jj < DIM + 1; ++jj) {
      gammaU[ii - 1][jj - 1] =
          metU[ii][jj] + shift[ii - 1] * shift[jj - 1] / lapse / lapse;
    }
  }

  //  std::array<double, 3> Sv = {{Sx[i], Sy[i], Sz[i]}};
  std::array<double, DIM> SvU = {};

  double S2 = 0.;
  for (int ii = 0; ii < DIM; ++ii) {
    for (int jj = 0; jj < DIM; ++jj) {
      S2 += gammaU[ii][jj] * (*S_ptr[ii])[i] * (*S_ptr[jj])[i];
      SvU[ii] += gammaU[ii][jj] * (*S_ptr[jj])[i];
    }
  }

  double tmp = 0;
  for (int ii = 0; ii < DIM; ++ii) {
    tmp += shift[ii] * (*S_ptr[ii])[i];
  }

  E[i] = (Ebar[i] + tmp) / lapse;
  //  double Et = E[i] + sdet * q[i] / eos_gamma/ Dstar[i];
  //  double Et2 = Et * Et;
  //  double G = 1. - 1. / eos_gamma;

  // Newton-Raphson until we have W
  int nr_iter = 0;
  double W_g = W[i];
  double Dtmp = Dstar[i] / sdet;
  while (true) {
    // F is the func we need the root of (F(W) = 0)
    double rhotmp = Dtmp / W_g;
    double Ptmp = K * std::pow(rhotmp, eos_gamma);

    double F = (W_g * W_g - 1.) * (Dtmp * E[i] + Ptmp + q[i]) *
                   (Dtmp * E[i] + Ptmp + q[i]) +
               Dtmp * Dtmp * W_g * W_g * S2;

    double Fprime =
        -2. * (W_g * W_g - 1.) * eos_gamma * Ptmp *
            (Dtmp * E[i] + Ptmp + q[i]) / W_g +
        2. * W_g * (Dtmp * E[i] + Ptmp + q[i]) * (Dtmp * E[i] + Ptmp + q[i]) +
        2. * Dtmp * Dtmp * W_g * S2;

    double W_n = W_g - F / Fprime;

    // TODO is this right?
    if (std::abs(W_n - W_g) / W_n < c2p_eps) {
      W[i] = W_n * sdet;  // TODO sdet maybe? NEEDS TO BE CHECKED!
      rho[i] = Dstar[i] / sdet / W_n;
      P[i] = K * std::pow(rho[i], eos_gamma);
      enth[i] = (1. / W_n) * (E[i] + (P[i] + q[i]) / Dtmp) - q[i] / rho[i];
      u[i] = enth[i] - 1. - P[i] / rho[i];
      cs[i] = std::sqrt(K * eos_gamma * std::pow(rho[i], eos_gamma - 1.));

      if (P[i] < 0. || enth[i] < 1.) {
        std::cout << "S2=" << S2 << " "
                  << "W_n=" << W_n << " "
                  << "W_g=" << W_g << " "
                  << "F=" << F << " "
                  << "Fprime=" << Fprime << " " << std::endl;
        std::cout << "D=" << Dstar[i] << " "
                  << "rho=" << rho[i] << " "
                  << "P=" << P[i] << " "
                  << "S2=" << S2 << " "
                  << "u=" << u[i] << " "
                  << "enth=" << enth[i] << std::endl;

        assert(0);
      }

      // we may not need the vbar arrays...
      for (size_t d = 0; d < DIM; ++d) {
        double vbar = SvU[d] / (W_n * (enth[i] + q[i] / rho[i]));
        (*vel_ptr[d])[i] = lapse * vbar - shift[d];
      }

      //      double vbarx = SvU[0] / (W_n * (enth[i] + q[i] / rho[i]));
      //      double vbary = SvU[1] / (W_n * (enth[i] + q[i] / rho[i]));
      //      double vbarz = SvU[2] / (W_n * (enth[i] + q[i] / rho[i]));
      //      vx[i] = lapse * vbarx - shift[0];
      //      vy[i] = lapse * vbary - shift[1];
      //      vz[i] = lapse * vbarz - shift[2];
      break;  // break out of the while(1) loop
    }

    // relax it so we dont overshoot and wind up negative
    // that shouldnt be needed
    double ome = 1.;
    W_g = (1. - ome) * W_g + ome * W_n;

    if (nr_iter >= max_nr_iter || !std::isfinite(W_g) /*|| W_g < 1.*/) {
      std::cout << " grcon2prim root finding failed @ x=" << x[i] << " "
                << "W_n=" << W_n << " "
                << "W_g=" << W_g << " "
                << "F=" << F << " "
                << "Fprime=" << Fprime << " "
                << "nr_iter=" << nr_iter << std::endl;

      // if more verbose?
      std::cout << "D=" << Dstar[i] << " "
                << "Ebar=" << Ebar[i] << " "
                << "W=" << W[i] << " "
                << "S2=" << S2 << " "
                << "u=" << u[i] << " "
                << "ome=" << omega[i] << std::endl;

      // Fail
      assert(0);
    }
    nr_iter++;
  }
}

void grsph::particle_grp2c_poly(const size_t i) {
  std::array<double, 3> pos = {{}};
  for (size_t d = 0; d < DIM; ++d) {
    pos[d] = (*pos_ptr[d])[i];
  }

  double det = std::sqrt(spacetime->sdet(pos));
  double lapse = spacetime->lapse(pos);
  auto shift = spacetime->shift(pos);
  auto gammaij = spacetime->gammaij(pos);

  std::array<double, DIM> vb_ = {};
  double tmp = 0.;

  for (size_t m = 0; m < DIM; ++m) {
    for (size_t n = 0; n < DIM; ++n) {
      tmp += gammaij[m][n] * (*vel_ptr[m])[i] * (*vel_ptr[n])[i];
      vb_[m] += gammaij[m][n] * (*vel_ptr[n])[i];
    }
  }

  W[i] = 1. / std::sqrt(1. - tmp);

  rho[i] = Dstar[i] / det / W[i];
  //    P[i] = (eos_gamma- 1.) * rho[i] * u[i];
  P[i] = K * std::pow(rho[i], eos_gamma);
  u[i] = K * std::pow(rho[i], eos_gamma - 1.) / (eos_gamma - 1);
  enth[i] = 1. + u[i] + P[i] / rho[i];
  //    u[i] = enth[i] - 1. - P[i] / rho[i];

  // assumes q = 0
  for (size_t d = 0; d < DIM; ++d) {
    (*S_ptr[d])[i] = enth[i] * W[i] * vb_[d];
  }
  //    Sx[i] = enth[i] * W[i] * vb_[0];
  //    Sy[i] = enth[i] * W[i] * vb_[1];
  //    Sz[i] = enth[i] * W[i] * vb_[2];

  E[i] = enth[i] * W[i] - det * (P[i]) / Dstar[i];

  //    std::vector<double> S_i(3);
  //    S_i[0] = Sx[i];
  //    S_i[1] = Sy[i];
  //    S_i[2] = Sz[i];

  tmp = 0.;
  for (size_t m = 0; m < DIM; ++m) {
    tmp += shift[m] * (*S_ptr[m])[i];
  }

  Ebar[i] = lapse * E[i] - tmp;
}
