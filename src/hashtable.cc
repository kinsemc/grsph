// Copyright 2016 Matt Kinsey

// This file handles the hierarchical spatial hashing, building and
// querying the table.

#include <limits>

#include "grsph.h"

#include "hash_utils.h"

namespace grsph {
ALIGNED tbb::concurrent_unordered_multimap<unsigned int, size_t> hash_table;
ALIGNED tbb::concurrent_vector<tbb::concurrent_vector<size_t>,
                               tbb::cache_aligned_allocator<size_t>>
    neighbors;

const int halfmaxint = std::numeric_limits<int>::max() / 2;
const unsigned int table_size =
    static_cast<unsigned int>(next_prime(halfmaxint));
int l_min, l_max;
size_t nlvls;
std::vector<double> inverse_bin_sizes;

std::array<double, DIM> hash_coord_min;
const int inum = 1;  // shoud always be 1 TODO replace with 1...
const double nn_hfac = 1.5;

void particle_find_neighbors(const size_t i);
void neighbor_counts(const size_t i);

// NB: these rely on the fact that casting double->int is the same as floor
// Return the hash value for a particle, i, on level l.
unsigned int hash_part(const size_t i, const unsigned int l) {
  unsigned int key = 0;
  for (size_t d = 0; d < DIM; ++d) {
    key ^= static_cast<unsigned int>(((*pos_ptr[d])[i] - hash_coord_min[d]) *
                                     inverse_bin_sizes[l]) *
           large_primes[d];
  }
  key ^= l * large_primes[DIM];
  key %= table_size;

  return key;
}

// Return the hash of particle, i, on level l, plus an offset by an
// integer array of bins, offset[DIM]
unsigned int hash_part_offset(const size_t i,
                              const std::array<int, DIM> offsets,
                              const unsigned int l) {
  unsigned int key = 0;
  for (size_t d = 0; d < DIM; ++d) {
    key ^= static_cast<unsigned int>(((*pos_ptr[d])[i] - hash_coord_min[d]) *
                                         inverse_bin_sizes[l] +
                                     offsets[d]) *
           large_primes[d];
  }
  key ^= l * large_primes[DIM];
  key %= table_size;

  return key;
}

// Return the hash of integer index, ind[DIM], on level l
unsigned int hash_ind(const std::array<unsigned int, DIM> ind,
                      const unsigned int l) {
  unsigned int key = 0;
  for (size_t d = 0; d < DIM; ++d) {
    key ^= ind[d] * large_primes[d];
  }
  key ^= l * large_primes[DIM];
  key %= table_size;

  return key;
}
};  // namespace grsph

// Wrapper to build spatial hashtable
void grsph::build_hashtable() {
  start_timer("build_hashtable");

  // Update the lowest and highest required l level, l_min/max
  auto h_minmax = particle_minmax(&h);
  l_min = static_cast<int>(
      std::ceil(std::log2(nn_hfac * support_r * h_minmax.first)));
  l_max = static_cast<int>(
      std::ceil(std::log2(nn_hfac * support_r * h_minmax.second)));

  nlvls = l_max - l_min + 1;

  // Precalculate the inverse bin sizes
  inverse_bin_sizes.resize(nlvls);
  for (size_t l = 0; l < nlvls; ++l) {
    double bin_size = std::pow(2., static_cast<int>(l + l_min));
    inverse_bin_sizes[l] = 1. / bin_size;
  }

  // Find hash coord origin so all particles are in the positive domain
  for (size_t d = 0; d < DIM; ++d) {
    hash_coord_min[d] = particle_min(pos_ptr[d]);
    hash_coord_min[d] -= 5. * static_cast<double>(inum * std::pow(2., l_max));
  }

  // Clear old hash table
  hash_table.clear();

  // This will trigger is there is a nan in h[]
  assert(nlvls < 1000);

  // Insert all the particles into hash_table
  particle_loop(hash_table_insert);

  stop_timer("build_hashtable");
}

// Wrapper to build complete neighbor list
void grsph::build_neighbor_list() {
  start_timer("build_neighbor_list");

  size_t np = x.size();
  neighbors.resize(np);

  // Empty old neighbor lists
  for (auto& neighborl : neighbors) {
    neighborl.clear();
  }

  // Find all neighbors
  particle_loop(particle_find_neighbors);

  // Count neighbors
  particle_loop(neighbor_counts);
  stop_timer("build_neighbor_list");
}

// Insert particle i into the hashtable
void grsph::hash_table_insert(const size_t i) {
  // Calculate key
  double s = nn_hfac * support_r * h[i];
  unsigned int l = static_cast<unsigned int>(std::ceil(std::log2(s)) - l_min);
  unsigned int key = hash_part(i, l);

  // Insert into table
  hash_table.insert(std::make_pair(key, i));
}

// Query the hashtable for particle i's neighbors and put them in neighbors[i]
void grsph::particle_find_neighbors(const size_t i) {
  // Check each level in the table
  for (size_t l = 0; l < nlvls; ++l) {
    // Calculate our offsets,
    // we should always be within plus/minus one because of the l levels
    std::array<int, 2 * DIM> irange;
    for (int d = 0; d < 2 * DIM; ++d) {
      irange[d] = (d % 2 == 0 ? -inum : inum);
    }

    // Get all the index offsets we need and query them
    auto ind_range = enum_irange<int>(irange);
    for (const auto& offset : ind_range) {
      // Calculate the key of our offset bin
      unsigned int key = hash_part_offset(i, offset, l);

      // Query the bin with our key
      auto range = hash_table.equal_range(key);

      // Precalculate some values we need to check for relevent neighbors
      double support_r2 = nn_hfac * nn_hfac * support_r * support_r;
      double support_rh2 = support_r2 * h[i] * h[i];

      // Check all particles that were in our bin
      for (auto it = range.first; it != range.second; ++it) {
        unsigned int ni = it->second;

        // Dont include yourself in your list of neighbors
        if (i == ni) continue;

        // Check that i and ni have a nonzero interaction
        double dist2 = fast_dist2(i, ni);

        if (dist2 <= support_rh2) {
          // if we arent a ghost count ni as our neighbor
          if (!ghost[i]) {
            neighbors[i].push_back(ni);
          }
          // Make sure that ni knows we are a neighbor if ni woudn't have
          // counted us
          if (dist2 > support_r2 * h[ni] * h[ni] && !ghost[ni]) {
            neighbors[ni].push_back(i);
          }
        }
      }
    }
  }
}

// Fill in nnnp[i] with neighbors.size
void grsph::neighbor_counts(const size_t i) {
  nnnp[i] = static_cast<double>(neighbors[i].size());
}

/*
inline int pos2int(double pos, double coord_min, double inv_bin_size) {
  return static_cast<int>((pos - coord_min) * inv_bin_size);
}

void grsph::position_find_neighbors(const std::array<double, DIM> pos) {
  for (size_t l = 0; l < nlvls; ++l) {
    //    double bin_size = std::pow(2., (int)l + l_min);

    // We should always be within one because of the l levels
    std::array<int, 2 * DIM> irange;
    for (int dd = 0; dd < 2 * DIM; ++dd) {
      irange[dd] = (dd % 2 == 0 ? -inum : inum);
    }

    auto ind_range = enum_irange<int>(irange);

    for (const auto& offset : ind_range) {
      unsigned int key = hash_part_offset(i, offset, l);

      auto range = hash_table.equal_range(key);

      double support_r2 = nn_hfac * nn_hfac * support_r * support_r;
      double support_rh2 = nn_hfac * nn_hfac * support_r2 * h[i] * h[i];
      for (auto it = range.first; it != range.second; ++it) {
        unsigned int ni = it->second;

        // Dont include yourself in your list of neighbors
        if (i == ni) continue;

        double dist2 = fast_dist2(i, ni);

        if (dist2 <= support_rh2) {
          if (!ghost[i]) neighbors[i].push_back(ni);
          if (dist2 > support_r2 * h[ni] * h[ni] && !ghost[ni]) {
            neighbors[ni].push_back(i);
          }
        }
      }
    }
  }
}
*/
