// Copyright 2016 Matt Kinsey

// a wrapper for loops over the particles. Fallback to TBB if theres no OMP

#include "grsph.h"

#ifndef _OPENMP
#include <tbb/blocked_range.h>
#include <tbb/parallel_for.h>
// #include <tbb/task_scheduler_init.h>
#endif

namespace grsph {

#ifndef _OPENMP
struct tbb_executor {
 public:
  explicit tbb_executor(const particle_func_t tbb_func) : tbb_func_(tbb_func) {}

  void operator()(const tbb::blocked_range<size_t> r) const {
    for (size_t i = r.begin(); i != r.end(); ++i) {
      tbb_func_(i);
    }
  }

 private:
  particle_func_t tbb_func_;
};
#endif

};  // namespace grsph

void grsph::particle_loop(const particle_func_t particle_func) {
  size_t np = x.size();

// Try to fallback to tbb if theres no openmp
// Since were using tbb headers we better have it...
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic, 5)
  for (size_t i = 0; i < np; ++i) {
    particle_func(i);
  }
#else
  size_t chunk_size = 50;  // this should be dynamic
  tbb_executor tbbExec(particle_func);
  tbb::parallel_for(tbb::blocked_range<size_t>(0, np, chunk_size), tbbExec);
#endif
}
