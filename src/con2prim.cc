// Copyright 2016 Matt Kinsey

// Just some wrappers for the con2prim and prim2con loops. They operate on
// the single particles functions defined by c2p_func and p2c_func.

#include "grsph.h"

namespace grsph {
double c2p_eps = std::pow(10.0, -4);
particle_func_t c2p_func;  // ptr to particle_con2prim function
particle_func_t p2c_func;  // ptr to particle_prim2con function

double eos_gamma = 5. / 3.;
double K = 0.001;
};  // namespace grsph

// Wrapper for con2prim
void grsph::con2prim() {
  start_timer("con2prim");
  particle_loop(c2p_func);
  stop_timer("con2prim");
}

// Wrapper for prim2con
void grsph::prim2con() {
  start_timer("prim2con");
  particle_loop(p2c_func);
  stop_timer("prim2con");
}
