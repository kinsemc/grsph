// Copyright 2016 Matt Kinsey

// This file contains a bunch of routines related to initialization and
// memory handling for our system of particles

//#include <sys/types.h>
//#include <sys/stat.h>
#include <fstream>

#include "grsph.h"
#include "particle.h"

namespace grsph {

Spacetime *spacetime;
// Schwarzschild spacetime(0.);
// Kerr spacetime = Kerr(0,0);
bool periodic = false;
int max_nr_iter = 2000;
bool print_boxes = false;
std::string io_dir;  // = "./";
int verbose = 1;
box global_box;
box periodic_box;
double eta;
double support_r;
kernel_func_t kernel;    // ptr to kernel
kernel_func_t Dkernel;   // ptr to kernel deriv w.r.t. r
kernel_func_t Dhkernel;  // ptr to kernel deriv w.r.t. h

std::vector<particle_vector<double> *> particle_vectors;

};  // namespace grsph

void grsph::init_grsph() {
  // Init MPI
  int mpi_inited;
  MPI_Initialized(&mpi_inited);

  if (!mpi_inited) {
    MPI_Init(nullptr, nullptr);
  }

  MPI_Comm_dup(MPI_COMM_WORLD, &MPI_COMM_GRSPH);

  MPI_Comm_rank(MPI_COMM_GRSPH, &mpi_rank);
  MPI_Comm_size(MPI_COMM_GRSPH, &mpi_size);

  assert(mpi_rank >= 0 && mpi_size >= 1);

  init_mpi_particle_t();

  std::cout << "GRSPH initialized on rank " << mpi_rank << ". Compiled with "
            << DIM << " dimensions" << std::endl;

  MPI_Barrier(MPI_COMM_GRSPH);

  /* BROKEN?
  // Read GRSPH_THREADS (for tbb)
  char const* tmp = getenv( "GRSPH_THREADS" );
  if ( tmp != NULL ) {
    std::string tmpstr( tmp );
    int nthreads = std::stoi(tmpstr,nullptr);
    std::cout << "Forcing " << nthreads << " threads!" << std::endl;
    tbb::task_scheduler_init init(nthreads);
  }
  */

  // Create io_dir if it doesnt exist
  // todo this doesnt mkdir -p, it needs to recurse the tree
  // int status;
  // status = mkdir(io_dir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  // assert(!status);

  // Init ascii output file
  if (cons_csv && cons_csv_overwrite) {
    std::ofstream consfile;
    consfile.open(cons_filename, std::ofstream::out | std::ofstream::trunc);
    consfile.close();
  }
}

// allocate storage for a particle array with given size and register it in the
// list of arrays
template <typename T>
void grsph::alloc_particle_array(particle_vector<T> *a, const size_t size,
                                 const T init_val) {
  if (size > 0) a->resize(size, init_val);
  particle_vectors.push_back(a);
}

// todo add an alloc for non ghost array
// it'll make things more obviously intentioned while saving some mem

void grsph::alloc(const size_t np) {
  // todo conditionally alloc (some of these are only used in some cases)

  // Base particle state (i.e. evolved vars)
  for (size_t d = 0; d < DIM; ++d) {
    alloc_particle_array(pos_ptr[d], np, 0.);
    alloc_particle_array(S_ptr[d], np, 0.);
    alloc_particle_array(vel_ptr[d], np, 0.);
    alloc_particle_array(Srhs_ptr[d], np, 0.);
    alloc_particle_array(postmp_ptr[d], np, 0.);
    alloc_particle_array(veltmp_ptr[d], np, 0.);
    alloc_particle_array(Stmp_ptr[d], np, 0.);
    for (size_t d2 = 0; d2 < DIM; ++d2) {
      alloc_particle_array(C_ptr[d][d2], np, 0.);
    }
  }

  alloc_particle_array(&mass, np, 1.);
  alloc_particle_array(&Ebar, np, 0.);

  // derived (needed for ghosts but not mpi move, can be recalced)
  // NB: con2prim isnt currently run on ghosts
  alloc_particle_array(&Dstar, np, 1.);
  alloc_particle_array(&h, np, 1.);

  alloc_particle_array(&rho, np, 0.);
  alloc_particle_array(&P, np, 0.);
  alloc_particle_array(&u, np, 0.);
  alloc_particle_array(&omega, np, 1.);
  alloc_particle_array(&alpha, np, 0.);
  alloc_particle_array(&W, np, 1.);
  alloc_particle_array(&enth, np, 0.);
  alloc_particle_array(&divv, np, 0.);
  alloc_particle_array(&divvdt, np, 0.);
  alloc_particle_array(&q, np, 0.);
  alloc_particle_array(&cs, np, 1.);

  // Used by the integrator
  alloc_particle_array(&tlevel, np, 0.);
  alloc_particle_array(&alpharhs, np, 0.);
  alloc_particle_array(&Ebarrhs, np, 0.);
  alloc_particle_array(&alphatmp, np, 0.);
  alloc_particle_array(&Ebartmp, np, 0.);

  // only in con2prim and output
  alloc_particle_array(&E, np, 0.);  // does this need this scope?

  // derived during calcD just for output
  alloc_particle_array(&nnnp, np, 0.);

  // Misc
  alloc_particle_array(&debug, np, 0.);

  // Ghost flag
  ghost.resize(np, 0);

  // this needs to be rethought because now all these vars get processed in var
  // 'arrays'
  // (they arent important but we just do this to dealloc them for ghosts...
  // there was a 'mem leak' in that the arrays keep getting added to without
  // removing the ghosts)
}

// add reserve_part?

particle_t grsph::pushpop_particle(const size_t i, const particle_t &p) {
  particle_t out;
  out = get_particle(i);
  replace_particle(i, p);
  return out;
}

void grsph::replace_particle(const size_t i, const particle_t &p) {
  for (size_t d = 0; d < DIM; ++d) {
    (*pos_ptr[d])[i] = p.pos[d];
    (*S_ptr[d])[i] = p.Svec[d];
    (*vel_ptr[d])[i] = p.vel[d];
    for (size_t d2 = 0; d2 < DIM; ++d2) {
      (*C_ptr[d][d2])[i] = p.C[d * DIM + d2];
    }
  }

  mass[i] = p.mass;
  h[i] = p.h;
  rho[i] = p.rho;
  P[i] = p.P;
  q[i] = p.q;
  u[i] = p.u;
  omega[i] = p.omega;
  alpha[i] = p.alpha;

  W[i] = p.W;
  Ebar[i] = p.Ebar;

  Dstar[i] = p.Dstar;
  enth[i] = p.enth;
  divv[i] = p.divv;
  divvdt[i] = 0.;  // p.divvdt;
  ghost[i] = 0;

  nnnp[i] = 0;
  E[i] = 0.;
  tlevel[i] = 0;
  debug[i] = 0.;

  cs[i] = p.cs;
}

size_t grsph::push_particle(const particle_t &p, const bool ghostp) {
  for (size_t d = 0; d < DIM; ++d) {
    (*pos_ptr[d]).push_back(p.pos[d]);
    (*S_ptr[d]).push_back(p.Svec[d]);
    (*vel_ptr[d]).push_back(p.vel[d]);
    for (size_t d2 = 0; d2 < DIM; ++d2) {
      (*C_ptr[d][d2]).push_back(p.C[d * DIM + d2]);
    }
  }

  mass.push_back(p.mass);
  h.push_back(p.h);
  rho.push_back(p.rho);
  P.push_back(p.P);
  q.push_back(p.q);
  u.push_back(p.u);
  omega.push_back(p.omega);
  alpha.push_back(p.alpha);

  W.push_back(p.W);
  Ebar.push_back(p.Ebar);

  Dstar.push_back(p.Dstar);
  enth.push_back(p.enth);
  divv.push_back(p.divv);
  divvdt.push_back(0. /*p.divvdt*/);
  ghost.push_back(ghostp);

  nnnp.push_back(0);
  E.push_back(0.);

  tlevel.push_back(0);

  cs.push_back(p.cs);

  for (size_t d = 0; d < DIM; ++d) {
    (*postmp_ptr[d]).push_back(0.);
    (*veltmp_ptr[d]).push_back(0.);
    (*Stmp_ptr[d]).push_back(0.);
    (*Srhs_ptr[d]).push_back(0.);
  }

  alpharhs.push_back(0.);
  Ebarrhs.push_back(0.);
  Ebartmp.push_back(0.);
  alphatmp.push_back(0.);

  debug.push_back(0.);

  return x.size() - 1;
}

particle_t grsph::get_particle(const size_t i) {
  particle_t p;

  for (size_t d = 0; d < DIM; ++d) {
    p.pos[d] = (*pos_ptr[d])[i];
    p.Svec[d] = (*S_ptr[d])[i];
    p.vel[d] = (*vel_ptr[d])[i];
    for (size_t d2 = 0; d2 < DIM; ++d2) {
      p.C[d * DIM + d2] = (*C_ptr[d][d2])[i];
    }
  }

  p.mass = mass[i];
  p.h = h[i];
  p.rho = rho[i];
  p.P = P[i];
  p.q = q[i];
  p.omega = omega[i];
  p.u = u[i];
  p.alpha = alpha[i];
  p.W = W[i];
  p.Ebar = Ebar[i];
  p.Dstar = Dstar[i];
  p.enth = enth[i];
  p.divv = divv[i];
  p.cs = cs[i];
  return p;
}

// grsph::rm_part(int i) {
//}
