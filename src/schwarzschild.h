// Copyright 2016 Matt Kinsey

// The spacetime of a BH in Schwarzschild coordinates

#ifndef SRC_SCHWARZSCHILD_H_
#define SRC_SCHWARZSCHILD_H_

#include <cmath>
#include <vector>

#include "spacetime.h"

class Schwarzschild : public Spacetime {
  // private:
  // double Mh;

 public:
  Schwarzschild() { Mh = 0.; }

  Schwarzschild(double M) { Mh = M; }

  double lapse(const std::array<double, 3>& pos) override {
    double r2 = 0.;
    for (const auto& elem : pos) {
      r2 += elem * elem;
    }
    double r = std::sqrt(r2);

    return std::sqrt(1. - 2. * Mh / r);
  }

  std::array<double, DIM> shift(const std::array<double, 3>& pos) override {
    std::array<double, DIM> beta = {{0}};

    return beta;
  }

  std::array<std::array<double, DIM>, DIM> gammaij(
      const std::array<double, 3>& pos) override {
    double r2 = 0.;
    for (const auto& elem : pos) {
      r2 += elem * elem;
    }

    double r = std::sqrt(r2);

    assert(DIM == 1);
    std::array<std::array<double, DIM>, DIM> met;

    met[0][0] = 1. / (1. - 2. * Mh / r);

    return met;
  }

  std::array<std::array<double, DIM>, DIM> kij(
      const std::array<double, 3>& pos) override {
    std::array<std::array<double, DIM>, DIM> kij = {{{{0}}}};

    return kij;
  }

  double det(const std::array<double, 3>& pos) override { return -1.; }

  double sdet(const std::array<double, 3>& pos) override {
    double alp = this->lapse(pos);
    return 1. / alp / alp;
  }

  std::array<std::array<double, DIM + 1>, DIM + 1> gmunuU(
      const std::array<double, 3>& pos) override {
    double r2 = 0.;
    for (const auto& elem : pos) {
      r2 += elem * elem;
    }
    double r = std::sqrt(r2);

    std::array<std::array<double, DIM + 1>, DIM + 1> met = {{{{0}}}};

    double lapse = this->lapse(pos);
    met[0][0] = -1. / lapse / lapse;
    met[1][1] = 1. - 2. * Mh / r;

    return met;
  }

  std::array<std::array<double, DIM + 1>, DIM + 1> gmunuL(
      const std::array<double, 3>& pos) override {
    double r2 = 0.;
    for (const auto& elem : pos) {
      r2 += elem * elem;
    }

    double r = std::sqrt(r2);
    double lapse = this->lapse(pos);

    std::array<std::array<double, DIM + 1>, DIM + 1> met = {{{{0}}}};
    met[0][0] = -lapse * lapse;
    met[1][1] = 1. / (1. - 2. * Mh / r);

    return met;
  }

  std::array<std::array<std::array<double, DIM>, DIM + 1>, DIM + 1> gmunuUdx(
      const std::array<double, 3>& pos) override {
    //  double gdx[4][4][3];
    std::array<std::array<std::array<double, DIM>, DIM + 1>, DIM + 1> gdx = {
        {{{{{0}}}}}};

    double r2 = 0.;
    for (const auto& elem : pos) {
      r2 += elem * elem;
    }
    double r = std::sqrt(r2);

    double tmp = 1. + 2. * Mh / r;

    gdx[0][0][0] = -2. * Mh / (tmp * tmp * r * r);
    gdx[1][1][0] = -2. * Mh / r / r;

    return gdx;
  }

  std::array<std::array<std::array<double, DIM>, DIM + 1>, DIM + 1> gmunudx(
      const std::array<double, 3>& pos) override {
    std::array<std::array<std::array<double, DIM>, DIM + 1>, DIM + 1> gdx = {
        {{{{{0}}}}}};

    double r2 = 0.;
    for (const auto& elem : pos) {
      r2 += elem * elem;
    }
    double r = std::sqrt(r2);

    double tmp = 1. + 2. * Mh / r;

    gdx[0][0][0] = 2. * Mh / (r * r);
    gdx[1][1][0] = 2. * Mh / r / r / tmp / tmp;

    return gdx;
  }
};

#endif  // SRC_SCHWARZSCHILD_H_
