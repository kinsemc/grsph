// Copyright 2016 Matt Kinsey

// TODO TODO TODO this is wrong for non flat metrics

#include "grsph.h"

namespace grsph {
void particle_divv(const size_t i);
};  // namespace grsph

void grsph::calc_divv() {
  start_timer("calc_divv");
  particle_loop(particle_divv);
  stop_timer("calc_divv");
}

void grsph::particle_divv(const size_t i) {
  if (ghost[i]) return;

  double divvtmp = divv[i];
  divv[i] = 0.;

  std::array<double, 3> pos = {{0}};
  for (size_t d = 0; d < DIM; ++d) {
    pos[d] = (*pos_ptr[d])[i];
  }
  auto gammaij = spacetime->gammaij(pos);

  for (const auto& ni : neighbors[i]) {
    double dist2 = fast_dist2(i, ni);
    double dist = std::sqrt(dist2);

    double weight = kernel(dist, h[i]);
    double nweight = kernel(dist, h[ni]);

    double vijdotGmean = 0.;

    for (size_t k = 0; k < DIM; ++k) {
      double Ga = 0.;
      double Gb = 0.;
      for (size_t d = 0; d < DIM; ++d) {
        Ga +=
            (*C_ptr[k][d])[i] * ((*pos_ptr[d])[ni] - (*pos_ptr[d])[i]) * weight;
        Gb += (*C_ptr[k][d])[ni] * ((*pos_ptr[d])[ni] - (*pos_ptr[d])[i]) *
              nweight;
      }
      double Gmean = .5 * (Ga + Gb);
      // TODO why isnt Ga a vector?
      // TODO TODO TODO vij should be gamma_ij (v_a - v_b)^i (v_a - v_b)^j
      double vij = (*vel_ptr[k])[i] - (*vel_ptr[k])[ni];
      vijdotGmean += vij * Gmean;
    }

    divv[i] += -mass[ni] * vijdotGmean;
  }

  divv[i] /= Dstar[i];  // why not Dstar[i]?
  divvdt[i] = (divv[i] - divvtmp) / dt;
}
