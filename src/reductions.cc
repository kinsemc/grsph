// Copyright 2016 Matt Kinsey

// These are some common reductions performed on the global set of particles

#include "grsph.h"

// Calculate the total number of particles across all ranks
int grsph::global_np(bool include_ghosts) {
  int local_np = 0, total_np = 0;

  if (include_ghosts) {
    local_np = x.size();
  } else {
    size_t np = x.size();
#pragma omp parallel for reduction(+ : local_np)
    for (size_t i = 0; i < np; ++i) {
      if (ghost[i]) continue;
      local_np++;
    }
  }

  MPI_Allreduce(&local_np, &total_np, 1, MPI_INT, MPI_SUM, MPI_COMM_GRSPH);

  return total_np;
}

// Calculate the min of pvec over all ranks
double grsph::particle_min(particle_vector<double>* pvec) {
  double local_min, global_min;

  if (!(*pvec).empty()) {
    local_min = *std::min_element((*pvec).begin(), (*pvec).end());
  } else {
    local_min = std::numeric_limits<double>::max();
  }

  MPI_Allreduce(&local_min, &global_min, 1, MPI_DOUBLE, MPI_MIN,
                MPI_COMM_GRSPH);

  return global_min;
}

// Calculate the max of pvec over all ranks
double grsph::particle_max(particle_vector<double>* pvec) {
  double local_max, global_max;

  if (!(*pvec).empty()) {
    local_max = *std::max_element((*pvec).begin(), (*pvec).end());
  } else {
    local_max = std::numeric_limits<double>::min();
  }

  MPI_Allreduce(&local_max, &global_max, 1, MPI_DOUBLE, MPI_MAX,
                MPI_COMM_GRSPH);

  return global_max;
}

// Calculate the minmax pair of pvec over all ranks
std::pair<double, double> grsph::particle_minmax(
    particle_vector<double>* pvec) {
  double local_min, local_max;
  std::pair<double, double> global_minmax;

  if (!(*pvec).empty()) {
    auto local_minmax = std::minmax_element((*pvec).begin(), (*pvec).end());
    local_min = *local_minmax.first;
    local_max = *local_minmax.second;
  } else {
    local_min = std::numeric_limits<double>::max();
    local_max = std::numeric_limits<double>::min();
  }

  MPI_Allreduce(&local_min, &global_minmax.first, 1, MPI_DOUBLE, MPI_MIN,
                MPI_COMM_GRSPH);
  MPI_Allreduce(&local_max, &global_minmax.second, 1, MPI_DOUBLE, MPI_MAX,
                MPI_COMM_GRSPH);

  return global_minmax;
}

// Calculate the mean value of pvec over all ranks
double grsph::particle_mean(particle_vector<double>* pvec) {
  size_t np = (*pvec).size();
  int total_np = global_np(false);

  // local sum
  double local_sum = 0.;
#pragma omp parallel for reduction(+ : local_sum)
  for (size_t i = 0; i < np; ++i) {
    if (ghost[i]) continue;
    local_sum += (*pvec)[i];
  }

  double global_sum = 0.;
  MPI_Allreduce(&local_sum, &global_sum, 1, MPI_DOUBLE, MPI_SUM,
                MPI_COMM_GRSPH);

  return global_sum / total_np;
}
