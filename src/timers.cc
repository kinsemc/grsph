// Copyright 2016 Matt Kinsey

// Some basic timing utilities for grsph, using chrono

#include <chrono>
#include <iomanip>
#include <ios>
#include <map>

#include "grsph.h"

namespace grsph {
std::map<std::string, clock::time_point> start_timers = {{}};
std::map<std::string, clock::time_point> end_timers = {{}};
clock::time_point global_timer_start;
clock::time_point rhs_start;
std::chrono::nanoseconds rhs_duration = std::chrono::nanoseconds::zero();
std::vector<double> time_per_cpu;
};  // namespace grsph

// ======================================================================
// These 3 functions are meant to keep track of how long the RHS is taking.
// Start it before doing calculations but make sure you stop it before a
// blocking MPI call. Restart once the MPI callc is finished.
// This information is then synced and used to load balance.
// ======================================================================

// Start rhs timer
void grsph::start_rhs_timing() { rhs_start = clock::now(); }

// Stop rhs timer and log elapsed time
void grsph::stop_rhs_timing() {
  auto rhs_stop = clock::now();
  rhs_duration += rhs_stop - rhs_start;
}

// Sync rhs times across MPI ranks
void grsph::sync_rhs_times() {
  time_per_cpu.resize(mpi_size);
  double rhstime = rhs_duration.count();
  MPI_Allgather(&rhstime, 1, MPI_DOUBLE, time_per_cpu.data(), 1, MPI_DOUBLE,
                MPI_COMM_GRSPH);
  rhs_duration = std::chrono::nanoseconds::zero();
}

// =====================================================================
// These are just generic timers to monitor how long various routines are
// taking.
// =====================================================================

// Create a timer named tname and start it
void grsph::start_timer(const std::string tname) {
  if (end_timers.count(tname) != 0) {
    auto start_time = start_timers[tname];
    auto end_time = end_timers[tname];

    start_timers.erase(tname);
    end_timers.erase(tname);
    auto tp = clock::now() - (end_time - start_time);
    start_timers.emplace(tname, tp);

  } else {
    auto tp = clock::now();
    start_timers.emplace(tname, tp);
  }
}

// Stop the timer named tname
void grsph::stop_timer(const std::string tname) {
  if (start_timers.count(tname) == 0) {
    std::cout << "ERROR: Attempting to stop a timer, " << tname
              << ", that doesn't exist!" << std::endl;
    assert(0);
  }

  auto tp = clock::now();
  end_timers.emplace(tname, tp);
}

// Print out the timer info (and make it pretty)
void grsph::print_timers() {
  std::cout << "Timers on rank " << mpi_rank << ":" << std::endl;

  int total_duration = 0;

  clock::time_point global_timer_stop = clock::now();

  auto global_elapsed = global_timer_stop - global_timer_start;

  auto global_duration =
      std::chrono::duration_cast<std::chrono::milliseconds>(global_elapsed)
          .count();

  // Calculate the durations and some stuff related to output
  size_t max_name_len = 0;
  std::vector<std::pair<std::string, int>> sorted_durations;
  for (const auto& timer_pair : end_timers) {
    auto tname = timer_pair.first;
    if (tname.length() > max_name_len) {
      max_name_len = tname.length();
    }

    auto end_time = timer_pair.second;
    auto start_time = start_timers[timer_pair.first];

    auto timer_duration = end_time - start_time;

    auto timer_time =
        std::chrono::duration_cast<std::chrono::milliseconds>(timer_duration)
            .count();

    std::pair<std::string, int> newpair = std::make_pair(tname, timer_time);
    sorted_durations.push_back(newpair);
  }

  // Sort the durations
  std::sort(sorted_durations.begin(), sorted_durations.end(),
            [](const std::pair<std::string, int>& lhs,
               const std::pair<std::string, int>& rhs) {
              return lhs.second > rhs.second;
            });

  // Print header
  std::cout << std::left << std::setprecision(1) << std::fixed << "  "
            << std::setw(max_name_len) << "Timer name"
            << ": " << std::setw(5) << std::right << "Duration"
            << "  [ " << std::setw(4) << std::right << "% of total"
            << " ]" << std::endl;

  // Print each timer
  for (const auto& duration_pair : sorted_durations) {
    auto tname = duration_pair.first;
    auto timer_duration = duration_pair.second;

    auto timer_percent = static_cast<double>(timer_duration) /
                         static_cast<double>(global_duration) * 100.;

    total_duration += timer_duration;

    std::cout << std::left << std::setprecision(1) << std::fixed << "  "
              << std::setw(max_name_len) << tname << ": " << std::setw(5)
              << std::right << timer_duration << " ms"
              << "  [ " << std::setw(4) << std::right << timer_percent << "% ]"
              << std::endl;
  }

  auto total_percent = static_cast<double>(total_duration) /
                       static_cast<double>(global_duration) * 100.;

  std::cout << "Timer sum on rank " << mpi_rank << ": " << total_duration
            << " ms"
            << " ( of a possible: " << global_duration << " ms ["
            << total_percent << "%] )" << std::endl;
  std::cout << std::resetiosflags(std::ios::fixed) << std::setprecision(6);
}

// Clear all our timers
void grsph::clear_timers() {
  start_timers.clear();
  end_timers.clear();
  global_timer_start = clock::now();
}
