// Copyright 2016 Matt Kinsey

// This is a purely virtual class used to represent the spacetime in the code

#ifndef SRC_SPACETIME_H_
#define SRC_SPACETIME_H_

class Spacetime {
 public:
  double Mh, aa;  // todo scope down to kerr

  virtual double lapse(const std::array<double, 3>& pos) = 0;

  virtual std::array<double, DIM> shift(const std::array<double, 3>& pos) = 0;

  virtual std::array<std::array<double, DIM>, DIM> gammaij(
      const std::array<double, 3>& pos) = 0;

  virtual std::array<std::array<double, DIM>, DIM> kij(
      const std::array<double, 3>& pos) = 0;

  virtual double det(const std::array<double, 3>& pos) = 0;

  virtual double sdet(const std::array<double, 3>& pos) = 0;

  virtual std::array<std::array<double, DIM + 1>, DIM + 1> gmunuU(
      const std::array<double, 3>& pos) = 0;

  virtual std::array<std::array<double, DIM + 1>, DIM + 1> gmunuL(
      const std::array<double, 3>& pos) = 0;

  virtual std::array<std::array<std::array<double, DIM>, DIM + 1>, DIM + 1>
  gmunuUdx(const std::array<double, 3>& pos) = 0;

  virtual std::array<std::array<std::array<double, DIM>, DIM + 1>, DIM + 1>
  gmunudx(const std::array<double, 3>& pos) = 0;
};

#endif  // SRC_SPACETIME_H_
