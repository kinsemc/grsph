// Copyright 2016 Matt Kinsey

#define _USE_MATH_DEFINES  // expose M_PI
#include <math.h>

#include <functional>

#include "grsph.h"
#include "intpow.h"

namespace grsph {
std::function<void()> mesh_func;
std::array<double, DIM> init_shift;
std::array<double, DIM> init_scale;
};  // namespace grsph

// init uniform [0,1) PRNG
std::mt19937::result_type seed = 1338;
auto real_rand = std::bind(std::uniform_real_distribution<double>(0., 1.),
                           std::mt19937(seed));

// init uniform [-1,1] PRNG
auto plusminus_rand = std::bind(std::uniform_real_distribution<double>(-1, 1),
                                std::mt19937(seed));

// Wrapper to init mesh and perform some ops from cfg
void grsph::init_mesh() {
  if (mpi_rank == 0) {
    mesh_func();
    shift_mesh(init_shift);
    scale_mesh(init_scale);
  } else {
    alloc(0);
  }

  // Update l_min/max for calc_ORB_boxes
  auto h_minmax = particle_minmax(&h);
  l_min = static_cast<int>(
      std::ceil(std::log2(nn_hfac * support_r * h_minmax.first)));
  l_max = static_cast<int>(
      std::ceil(std::log2(nn_hfac * support_r * h_minmax.second)));

  if (init_glass) {
    bool tmp_periodic = false;
    if (!periodic) {
      periodic = true;
      for (size_t d = 0; d < DIM; ++d) {
        periodic_box[2 * d] = 0.;
        periodic_box[2 * d + 1] = 1.;
      }
      tmp_periodic = true;
    }

    perturb_mesh(perturb_max);
    glassify_mesh(glass_max_err);

    if (tmp_periodic) periodic = false;
  }
  /*
    if (clean_inside_r) {
      size_t np = x.size();
      for (size_t i = 0; i < np; ++i) {
        double r2 = 0.;
        for (size_t d = 0; d < DIM; ++d) {
          r2 += (*pos_ptr[d])[i] * (*pos_ptr[d])[i];
        }
        if (r2 <
            clean_inside_r * clean_inside_r) {
          ghost[i] = 1;
        }
      }
      clear_ghosts();
    }
  */
  // shift_mesh(init_shift);
  // scale_mesh(init_scale);

  //  calc_ORB_boxes();

  //  sync_particles_to_procs();

  //  clear_ghosts();
  //  MPI_Barrier(MPI_COMM_GRSPH);
  //  sort_particles();
}

void grsph::init_empty_mesh() {
  std::cout << "Not initializing mesh!" << std::endl;
  return;
}

// Shift all positions by (xshift,yshift,zshift)
void grsph::shift_mesh(const std::array<double, DIM> shift) {
  std::cout << "Shifting mesh by (";
  for (size_t d = 0; d < DIM; ++d) {
    std::cout << shift[d] << ",";
  }
  std::cout << ")... ";

  size_t np = x.size();

  for (size_t d = 0; d < DIM; ++d) {
    for (size_t i = 0; i < np; ++i) {
      (*pos_ptr[d])[i] += shift[d];
    }
  }
  std::cout << "done" << std::endl;
}

// Scale all positions scalefac*(x,y,z)
void grsph::scale_mesh(const std::array<double, DIM> scale) {
  std::cout << "Scaling mesh by (";
  for (size_t d = 0; d < DIM; ++d) {
    std::cout << scale[d] << ",";
  }
  std::cout << ")... ";

  double hfac2 = 0;
  double hfac = 0;
  for (size_t d = 0; d < DIM; ++d) {
    hfac2 += scale[d] * scale[d];
  }
  hfac = std::sqrt(hfac2);

  size_t np = x.size();

  for (size_t d = 0; d < DIM; ++d) {
    for (size_t i = 0; i < np; ++i) {
      (*pos_ptr[d])[i] *= scale[d];
    }
  }

  for (size_t i = 0; i < np; ++i) {
    h[i] *= hfac;
  }

  std::cout << "done" << std::endl;
}

// Init np random positions in [0,1)^DIM
void grsph::init_rand_mesh(const size_t np) {
  if (verbose) {
    std::cout << "Initializing " << np << " random particles in [0,1]^" << DIM
              << "... ";
  }

  alloc(np);

  for (size_t d = 0; d < DIM; ++d) {
    for (size_t i = 0; i < np; ++i) {
      (*pos_ptr[d])[i] = real_rand();
    }
  }

  for (size_t i = 0; i < np; ++i) {
    h[i] = 4. / np;
  }

  if (verbose) {
    std::cout << "done" << std::endl;
  }
}

// convert particle positions of the form [0,1)^DIM
// to a mesh with radial density profile given by the CDF F(rvec)
void grsph::init_radial_mesh_from_table(std::vector<double> rvec,
                                        std::vector<double> F) {
  std::cout << "Initializing radial density CDF... ";

  if (!x.size()) return;
  // todo better DIM handling
  for (size_t i = 0; i < x.size(); ++i) {
    double X = x[i];
#if dim2
    double Y = y[i];
#endif
#if dim3
    double Z = z[i];
#endif

    double rr = -1000000;
    for (size_t j = 0; j < F.size(); ++j) {
      if (X > F[j] && X < F[j + 1]) {
        double slope = (F[j + 1] - F[j]) / (rvec[j + 1] - rvec[j]);
        rr = rvec[j] + (X - F[j]) / slope;
      }
    }

    h[i] = 4.;

#if DIM == 1
    x[i] = rr;
#endif
#if DIM == 2
    double phi = Y * 2. * M_PI;
    x[i] = rr * cos(phi);
    y[i] = rr * sin(phi);
#endif
#if DIM == 3
    double phi = Y * 2. * M_PI;
    double theta = acos(2. * Z - 1);  // measured from the equator
    x[i] = rr * cos(phi) * sin(theta);
    y[i] = rr * sin(phi) * sin(theta);
    z[i] = rr * cos(theta);
#endif
  }

  std::cout << "done" << std::endl;
}

void grsph::init_gaussian_mesh(const size_t np, const double stddev) {
  init_rand_mesh(np);

  if (!x.size()) return;

  std::mt19937 gen(seed);
  std::normal_distribution<double> d(0., stddev);

  for (size_t i = 0; i < x.size(); ++i) {
#if dim2
    double Y = y[i];
#endif
#if dim3
    double Z = z[i];
#endif

    double rr = std::abs(d(gen));
    double prob = 1. / stddev / std::sqrt(2 * M_PI) *
                  exp(-rr * rr / (2. * stddev * stddev));

    double area = 2.;
#if dim2
    area = 2. * M_PI * rr;
#endif
#if dim3
    area = 4. * M_PI * rr * rr;
#endif

    mass[i] = 1. / np;
    rho[i] = prob / area;
    Dstar[i] = rho[i];
    h[i] =
        4. * eta * std::pow(mass[i] / Dstar[i], 1. / static_cast<double>(DIM));

    // todo why are these here?!
    P[i] = 1.;
    u[i] = 1.;

#if DIM == 1
    x[i] = rr;
#endif
#if DIM == 2
    double phi = Y * 2. * M_PI;
    x[i] = rr * cos(phi);
    y[i] = rr * sin(phi);
#endif
#if DIM == 3
    double phi = Y * 2. * M_PI;
    double theta = acos(2. * Z - 1);  // measured from the equator
    x[i] = rr * cos(phi) * sin(theta);
    y[i] = rr * sin(phi) * sin(theta);
    z[i] = rr * cos(theta);
#endif
  }
}

// Perturb all positions by [0,max_disp)
void grsph::perturb_mesh(const double max_disp) {
  std::cout << "Perturbing mesh by [0," << max_disp << ") ... ";

  size_t np = x.size();

  for (size_t d = 0; d < DIM; ++d) {
    for (size_t i = 0; i < np; ++i) {
      (*pos_ptr[d])[i] += max_disp * plusminus_rand();
    }
  }
  std::cout << "done" << std::endl;
}

// Generate a periodic hex lattice on [0,1)^2 with nx particles spanning x
void grsph::hex_lattice(const int nx) {
#if DIM == 2
  if (periodic) {
    assert(!(nx & 1));  // make sure nx is even so the mesh is periodic
  }

  // Calculate ny
  int ny = nx * 2. / std::sqrt(3);
  if (periodic) {
    double xsize = periodic_box[1] - periodic_box[0];
    double ysize = periodic_box[3] - periodic_box[2];

    ny = ysize / xsize * nx * 2. / std::sqrt(3);
  }
  if (ny & 1) ny++;

  double xscale = 1. / nx;
  double yscale = 2. / std::sqrt(3) / ny;

  alloc(nx * ny);
  int pind = 0;
  for (int iy = 0; iy < ny; ++iy) {
    double ytmp = 0.5 * std::sqrt(3.) * iy;
    for (int ix = 0; ix < nx; ++ix) {
      double xtmp = (iy & 1) ? (ix + 0.5) : ix;

      x[pind] = xtmp * xscale;
      y[pind] = ytmp * yscale;
      pind++;
    }
  }

  // double vol = 3. * std::sqrt(3.) / 8. * xscale * yscale;
  for (int i = 0; i < nx * ny; ++i) {
    Dstar[i] = 1.;
    mass[i] = 1.;
    h[i] = eta;
  }

// TODO this should be its own func
#elif DIM == 1
  alloc(nx);
  for (int ix = 0; ix < nx; ++ix) {
    x[ix] = static_cast<double>(ix) / static_cast<double>(nx);
  }
#else
  assert(0);
#endif
}

void grsph::cubic_lattice(const size_t nx) {
#if DIM == 3
  assert(DIM == 3);

  double sep = 1. / nx;

  alloc(nx * nx * nx);

  for (size_t k = 0; k < nx; ++k) {
    for (size_t j = 0; j < nx; ++j) {
      for (size_t i = 0; i < nx; ++i) {
        size_t ind = i + j * nx + k * nx * nx;
        x[ind] = i * sep;
        y[ind] = j * sep;
        z[ind] = k * sep;
        h[ind] = eta * sep;
      }
    }
  }
  output_h5part(0);
#else
  assert(0);
#endif
}

void grsph::calc_global_box() {
  box gbox;

  // Make sure we have l_max defined
  double h_max = particle_max(&h);
  l_max = static_cast<int>(std::ceil(std::log2(nn_hfac * support_r * h_max)));

  double move_dist = 2. * std::pow(2., l_max);

  // Find local coord min/max
  for (int d = 0; d < DIM; ++d) {
    auto coord_minmax = particle_minmax(pos_ptr[d]);

    gbox[2 * d] = coord_minmax.first;
    gbox[2 * d] -= inum * move_dist;

    gbox[2 * d + 1] = coord_minmax.second;
    gbox[2 * d + 1] += inum * move_dist;
  }

  global_box = gbox;
}
