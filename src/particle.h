// Copyright 2016 Matt Kinsey

// This is the particle struct that MPI uses to communicate particles
// TODO this can be stripped down, it holds redundant info
// just need to calc prims after each send (they are marked with !)

#ifndef SRC_PARTICLE_H_
#define SRC_PARTICLE_H_

typedef struct {
  double pos[DIM];
  double vel[DIM];  // !
  double mass;
  double h;
  double rho;  // !
  double P;    // !
  double q;
  double u;  // !
  double omega;
  double alpha;
  double Svec[DIM];
  double W;  // !
  double Ebar;
  double Dstar;
  double enth;  // !
  double divv;
  double cs;  // !
  double C[DIM * DIM];
} particle_t;

#endif  // SRC_PARTICLE_H_
