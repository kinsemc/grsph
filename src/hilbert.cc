// Copyright 2016 Matt Kinsey

// Reorder indices based on a hilbert curve, useful for maintaining cache
// locality.

#include <algorithm>
#include <limits>
#include <vector>

#include "Hilbert.hpp"
#include "grsph.h"

typedef uint64_t hkey;

// Struct containing a particle's hilbert key and current index
struct hilbert_data {
  hkey key;
  size_t index;
};

// A comparator for sorting hilbert_data by key
bool compare_hkey(const hilbert_data &a, const hilbert_data &b) {
  return a.key < b.key;
}

// Calculate all the hilbert keys then reorder the particles (in place)
void grsph::sort_particles() {
  start_timer("sort_particles");
  size_t np = x.size();

  std::vector<hilbert_data> hdv(np);
  double inv_bin_size =
      1. / std::pow(2., (l_min + l_min) / 2);  // is this ideal?
  //  double bin_size = std::pow(2., l_min);

  // Fill in array of (index, key) pairs
  for (size_t i = 0; i < np; ++i) {
    hilbert_data hd;
    hd.index = i;

    // Find int index of particle
    const size_t hilbertdim = ((DIM == 1) ? DIM + 1 : DIM);

    int bits = std::numeric_limits<hkey>::digits;

    CFixBitVec bitvec[hilbertdim];
    int ms[hilbertdim] = {bits};
    for (size_t d = 0; d < DIM; d++) {
      bitvec[d] = ((*pos_ptr[d])[i] - hash_coord_min[d]) * inv_bin_size;
      // ms[d] = bits;  // std::max(ms[d],bitvec[d].msb());
    }

    // Calculate hilbert key, hkey // todo rename
    CFixBitVec hkey;
    Hilbert::coordsToCompactIndex(bitvec, ms, hilbertdim, hkey);

    hd.key = hkey.rack();

    hdv[i] = hd;
  }

  // Sort hdv by key value
  std::sort(hdv.begin(), hdv.end(), compare_hkey);

  // Find each particles new id
  std::vector<size_t> id(np);
  for (size_t i = 0; i < np; ++i) id[hdv[i].index] = i;

  // Reorder particles in place
  size_t idsource, idtmp, dest;
  particle_t psource, ptmp;

  for (size_t i = 0; i < np; ++i) {
    if (id[i] != i) {
      psource = get_particle(i);
      idsource = id[i];
      dest = id[i];

      while (1) {
        ptmp = get_particle(dest);
        idtmp = id[dest];

        replace_particle(dest, psource);
        id[dest] = idsource;

        if (dest == i) {
          break;
        }

        psource = ptmp;
        idsource = idtmp;
        dest = idsource;
      }
    }
  }

  stop_timer("sort_particles");
}
