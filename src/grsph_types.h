// Copyright 2016 Matt Kinsey

#ifndef SRC_GRSPH_TYPES_H_
#define SRC_GRSPH_TYPES_H_

#include <vector>

namespace grsph {

// ============================================
// GRSPH Types
// ============================================
template <typename T>
// using particle_vector = std::vector<T>;
// using particle_vector = ALIGNED std::vector<T>;
using particle_vector = std::vector<T, tbb::cache_aligned_allocator<T>>;
using box = std::array<double, 2 * DIM>;
using particle_func_t = std::function<void(const size_t)>;
using kernel_func_t = std::function<double(const double, const double)>;

using clock = std::chrono::high_resolution_clock;
};  // namespace grsph

#endif  // SRC_GRSPH_TYPES_H_
