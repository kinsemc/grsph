// Copyright 2016 Matt Kinsey

// TODO really needs some doc

#include <mpi.h>

#include <algorithm>
#include <unordered_set>

#include "grsph.h"
#include "hash_utils.h"

namespace grsph {
// vector of [proc][index] for which particles we owe remote ranks
std::vector<std::unordered_set<size_t>> mpi_ghost_tables;
// ordered list of ghost indices so we can loop over them in order
std::vector<size_t> mpi_ghost_indices;

// map of which particles are needed by the periodic bounds
std::unordered_set<size_t> periodic_ghost_table;
// ordered list of periodic ghost indices so we can loop over them in order
std::vector<size_t> periodic_ghost_indices;
};  // namespace grsph

// Erase ghost=true particle from all the particle arrays using Erase-Remove
// idiom aka pack the good values at the front and just .resize away the
// leftovers
void grsph::clear_ghosts() {
  start_timer("clear_ghosts");

  if (ghost.empty()) return;

  for (auto& array : particle_vectors) {
    // Pack all the !ghost values to the front of the array
    size_t last = 0;
    size_t np = ghost.size();
    for (size_t i = 0; i < np; ++i, ++last) {
      while (ghost[i]) {
        ++i;
        if (i >= np) break;
      }
      if (i >= np) break;
      (*array)[last] = (*array)[i];
    }
    // Resize to just the non ghosts
    array->resize(last);
  }

  // Clean up ghost[] array itself
  ghost.erase(std::remove(ghost.begin(), ghost.end(), 1), ghost.end());

  periodic_ghost_indices.clear();
  mpi_ghost_indices.clear();

  stop_timer("clear_ghosts");
}

// Sync a single particle_vector to all ghosts
void grsph::ghost_sync_particlevec(particle_vector<double>& pvec) {
  start_timer("ghost_sync_particlevec");

  if (periodic) {
    size_t ind = 0;
    for (const auto& i : periodic_ghost_table) {
      size_t gind = periodic_ghost_indices[ind];
      assert(ghost[gind]);
      pvec[gind] = pvec[i];
      ind++;
    }
  }

  if (mpi_size == 1) {
    stop_timer("ghost_sync_particlevec");
    return;
  }

  particle_vector<double> sendvec;
  std::vector<int> sendcounts;
  std::vector<int> senddisps;
  sendcounts.resize(mpi_size, 0);
  senddisps.resize(mpi_size, 0);
  size_t sendsize = 0;

  for (int proc = 0; proc < mpi_size; ++proc) {
    if (proc == mpi_rank) continue;
    const auto& mpi_ghost_table = mpi_ghost_tables[proc];
    sendsize += mpi_ghost_table.size();
    sendcounts[proc] += mpi_ghost_table.size();
  }

  sendvec.resize(sendsize);

  size_t tmpind = 0;
  for (int proc = 0; proc < mpi_size; ++proc) {
    if (proc == mpi_rank) continue;
    const auto& mpi_ghost_table = mpi_ghost_tables[proc];
    for (const auto& i : mpi_ghost_table) {
      sendvec[tmpind] = pvec[i];
      tmpind++;
    }
  }

  std::vector<int> recvcounts;
  std::vector<int> recvdisps;
  recvcounts.resize(mpi_size, 0);
  recvdisps.resize(mpi_size, 0);

  MPI_Alltoall(sendcounts.data(), 1, MPI_INT, recvcounts.data(), 1, MPI_INT,
               MPI_COMM_GRSPH);

  int totrecvsize = recvcounts[0];
  for (int proc = 1; proc < mpi_size; ++proc) {
    senddisps[proc] = sendcounts[proc - 1] + senddisps[proc - 1];
    recvdisps[proc] = recvcounts[proc - 1] + recvdisps[proc - 1];
    totrecvsize += recvcounts[proc];
  }

  std::vector<double> recvvec;
  recvvec.resize(totrecvsize);

  MPI_Alltoallv(sendvec.data(), sendcounts.data(), senddisps.data(), MPI_DOUBLE,
                recvvec.data(), recvcounts.data(), recvdisps.data(), MPI_DOUBLE,
                MPI_COMM_GRSPH);

  int recvsize = recvvec.size();
  for (int i = 0; i < recvsize; ++i) {
    size_t gind = mpi_ghost_indices[i];
    assert(ghost[gind]);
    pvec[gind] = recvvec[i];
  }

  stop_timer("ghost_sync_particlevec");
}

// todo move to mpi.cc so that mpi_particle_t can be local?
// This functions performs all-to-all based on the values in mpi_ghost_tables
void grsph::send_ghosts() {
  if (mpi_size == 1) return;

  start_timer("send_ghosts");

  std::vector<particle_t> sendparts;
  std::vector<int> sendcounts;
  std::vector<int> senddisps;
  sendcounts.resize(mpi_size, 0);
  senddisps.resize(mpi_size, 0);

  for (int proc = 0; proc < mpi_size; ++proc) {
    if (proc == mpi_rank) continue;
    const auto& mpi_ghost_table = mpi_ghost_tables[proc];
    for (const auto& i : mpi_ghost_table) {
      sendparts.push_back(get_particle(i));
      sendcounts[proc]++;
    }
  }

  std::vector<int> recvcounts;
  std::vector<int> recvdisps;
  recvcounts.resize(mpi_size, 0);
  recvdisps.resize(mpi_size, 0);

  MPI_Alltoall(sendcounts.data(), 1, MPI_INT, recvcounts.data(), 1, MPI_INT,
               MPI_COMM_GRSPH);

  int totrecvsize = recvcounts[0];
  for (int proc = 1; proc < mpi_size; ++proc) {
    senddisps[proc] = sendcounts[proc - 1] + senddisps[proc - 1];
    recvdisps[proc] = recvcounts[proc - 1] + recvdisps[proc - 1];
    totrecvsize += recvcounts[proc];
  }

  std::vector<particle_t> recvparts;
  recvparts.resize(totrecvsize);

  MPI_Alltoallv(sendparts.data(), sendcounts.data(), senddisps.data(),
                mpi_particle_t, recvparts.data(), recvcounts.data(),
                recvdisps.data(), mpi_particle_t, MPI_COMM_GRSPH);

  mpi_ghost_indices.clear();

  for (const auto& p : recvparts) {
    size_t gind = push_particle(p, true);
    mpi_ghost_indices.push_back(gind);
    hash_table_insert(gind);
  }
  stop_timer("send_ghosts");
}

// move to hashutil?
inline int pos2int(double pos, double coord_min, double inv_bin_size) {
  return static_cast<int>((pos - coord_min) * inv_bin_size);
}

// This function fills in mpi_ghost_tables then does a send
void grsph::find_ghosts() {
  // If only one proc, were done
  if (mpi_size == 1) return;

  start_timer("find_ghosts");

  mpi_ghost_tables.resize(mpi_size);  // TODO move to init

  for (auto& mpi_ghost_table : mpi_ghost_tables) {
    mpi_ghost_table.clear();
  }

  for (int proc = 0; proc < mpi_size; ++proc) {
    if (proc == mpi_rank) continue;

    box b = boxes[mpi_rank];
    box rb = boxes[proc];

    for (size_t d = 0; d < 2 * DIM; ++d) {
      size_t rd =
          (d % 2 == 0
               ? d + 1
               : d - 1);  // set rd to the opposite bound in the same dim as d

      // calc the overlap of the two boxes (needed for the dims that we arent
      // currently checking)
      box gb;

      for (size_t bd = 0; bd < DIM; ++bd) {
        gb[2 * bd] = std::max(b[2 * bd], rb[2 * bd]);
        gb[2 * bd + 1] = std::min(b[2 * bd + 1], rb[2 * bd + 1]);
      }

      gb[d] = rb[d];
      gb[rd] = rb[d];

      for (size_t l = 0; l < nlvls; ++l) {
        std::array<unsigned int, 2 * DIM> irange;

        for (int dd = 0; dd < 2 * DIM; ++dd) {
          int ii = (dd % 2 == 0 ? -inum : inum);
          irange[dd] = static_cast<unsigned int>(
              pos2int(rb[dd], hash_coord_min[(size_t)(dd / 2)],
                      inverse_bin_sizes[l]) +
              ii);  // dd/2 relies on int truncation!
        }

        auto ind_range = enum_irange<unsigned int>(irange);

        for (auto ind : ind_range) {
          unsigned int key = hash_ind(ind, l);

          auto range = hash_table.equal_range(key);

          for (auto it = range.first; it != range.second; ++it) {
            size_t ni = it->second;
            mpi_ghost_tables[proc].insert(ni);
          }
        }
      }
    }
  }

  stop_timer("find_ghosts");
  send_ghosts();
}

// Fill in periodic_ghosts_table with the needed ghosts
void grsph::find_periodic_ghosts() {
  // enforce_periodic_bounds();

  start_timer("find_periodic_ghosts");

  periodic_ghost_indices.clear();

  std::vector<size_t> dimghosts;  // Track new ghosts in each dim (x,y,z)

  for (size_t d = 0; d < 2 * DIM; ++d) {
    size_t rd =
        (d % 2 == 0
             ? d + 1
             : d - 1);  // set rd to the opposite bound in the same dim as d

    std::unordered_set<size_t> periodic_ghost_table;

    for (size_t l = 0; l < nlvls; ++l) {
      std::array<unsigned int, 2 * DIM> irange;

      for (size_t dd = 0; dd < 2 * DIM; ++dd) {
        int ii = (dd % 2 == 0 ? -inum : inum);
        irange[dd] = static_cast<unsigned int>(
            pos2int(periodic_box[dd], hash_coord_min[(dd / 2)],
                    inverse_bin_sizes[l]) +
            ii);  // dd/2 relies on int truncation!
      }

      // set the rd bound
      int ii = (rd % 2 == 0 ? -inum : inum);
      irange[rd] = static_cast<unsigned int>(pos2int(periodic_box[d],
                                                     hash_coord_min[(d / 2)],
                                                     inverse_bin_sizes[l]) +
                                             ii);

      auto ind_range = enum_irange<unsigned int>(irange);

      for (const auto& ind : ind_range) {
        unsigned int key = hash_ind(ind, l);

        auto range = hash_table.equal_range(key);

        for (auto it = range.first; it != range.second; ++it) {
          size_t ni = it->second;
          periodic_ghost_table.insert(ni);
        }
      }
    }

    // Move particles to opposite boundry and create ghost
    double move_dist = periodic_box[rd] - periodic_box[d];
    for (const auto& i : periodic_ghost_table) {
      particle_t p = get_particle(i);
      p.pos[d / 2] += move_dist;
      size_t gind = push_particle(p, true);
      dimghosts.push_back(gind);
    }

    // After we complete a dim (x, y, or z) insert the ghosts into the table
    if (d % 2 == 1) {
      for (const auto& i : dimghosts) {
        periodic_ghost_indices.push_back(i);
        hash_table_insert(i);
      }
      dimghosts.clear();
    }
  }

  stop_timer("find_periodic_ghosts");
}
