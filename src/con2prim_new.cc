// Copyright 2016 Matt Kinsey

// Find the primatives from teh conservatives by iterating on the enth

#include <gsl/gsl_errno.h>
#include <gsl/gsl_roots.h>

#include "grsph.h"

struct enth_params {
  double S2, e, eos_gamma, SdotBeta, alp;
};

double enth_func(double enth, void *params) {
  struct enth_params *p = (struct enth_params *)params;

  double S2 = p->S2;
  double e = p->e;
  double eos_gamma = p->eos_gamma;
  double SdotBeta = p->SdotBeta;
  double alp = p->alp;

  double W = std::sqrt(1 + S2 / enth / enth);

  return ((alp * (eos_gamma - 1.) * enth) +
          eos_gamma * (enth * enth + S2 - enth * W * (e + SdotBeta))) /
             (alp * (eos_gamma - 1.) * enth) -
         enth;
}

double enth_func_deriv(double enth, void *params) {
  struct enth_params *p = (struct enth_params *)params;

  double S2 = p->S2;
  double e = p->e;
  double eos_gamma = p->eos_gamma;
  double SdotBeta = p->SdotBeta;
  double alp = p->alp;

  double W = std::sqrt(1 + S2 / enth / enth);

  return (e * eos_gamma * S2 - alp * (eos_gamma - 1.) * enth * enth * enth * W +
          eos_gamma * enth * enth * enth * W - eos_gamma * enth * S2 * W +
          eos_gamma * S2 * SdotBeta) /
         (alp * (eos_gamma - 1.) * enth * enth * enth * W);
}

void enth_fdf(double enth, void *params, double *f, double *df) {
  struct enth_params *p = (struct enth_params *)params;

  double S2 = p->S2;
  double e = p->e;
  double eos_gamma = p->eos_gamma;
  double SdotBeta = p->SdotBeta;
  double alp = p->alp;

  double W = std::sqrt(1 + S2 / enth / enth);

  *f = ((alp * (-1 + eos_gamma) * enth) +
        eos_gamma * (enth * enth + S2 - enth * W * (e + SdotBeta))) /
           (alp * (eos_gamma - 1.) * enth) -
       enth;
  *df = (e * eos_gamma * S2 - alp * (eos_gamma - 1.) * enth * enth * enth * W +
         eos_gamma * enth * enth * enth * W - eos_gamma * enth * S2 * W +
         eos_gamma * S2 * SdotBeta) /
        (alp * (eos_gamma - 1.) * enth * enth * enth * W);
}

void grsph::particle_grc2p_new(const size_t i) {
  if (ghost[i]) return;

  std::array<double, 3> pos = {{}};
  for (size_t d = 0; d < DIM; ++d) {
    pos[d] = (*pos_ptr[d])[i];
  }

  double sdet = std::sqrt(spacetime->sdet(pos));
  auto metU = spacetime->gmunuU(pos);  // full metric g^munu
  double lapse = spacetime->lapse(pos);
  auto shift = spacetime->shift(pos);

  // TODO get this from spacetime->..
  std::vector<std::vector<double>> gammaU(DIM, std::vector<double>(DIM));

  for (int ii = 1; ii < DIM + 1; ++ii) {
    for (int jj = 1; jj < DIM + 1; ++jj) {
      gammaU[ii - 1][jj - 1] =
          metU[ii][jj] + shift[ii - 1] * shift[jj - 1] / lapse / lapse;
    }
  }

  // S^i
  std::array<double, DIM> SvU = {};

  double S2 = 0.;
  for (int ii = 0; ii < DIM; ++ii) {
    for (int jj = 0; jj < DIM; ++jj) {
      S2 += gammaU[ii][jj] * (*S_ptr[ii])[i] * (*S_ptr[jj])[i];
      SvU[ii] += gammaU[ii][jj] * (*S_ptr[jj])[i];
    }
  }

  double SdotBeta = 0;
  for (int ii = 0; ii < DIM; ++ii) {
    SdotBeta += shift[ii] * (*S_ptr[ii])[i];
  }

  E[i] = (Ebar[i] + SdotBeta) / lapse;

  int nr_iter = 0;

  struct enth_params params = {S2, Ebar[i], eos_gamma, SdotBeta, lapse};

  gsl_function_fdf FDF;
  FDF.f = &enth_func;
  FDF.df = &enth_func_deriv;
  FDF.fdf = &enth_fdf;
  FDF.params = &params;

  const gsl_root_fdfsolver_type *T;
  gsl_root_fdfsolver *s;
  T = gsl_root_fdfsolver_steffenson;
  s = gsl_root_fdfsolver_alloc(T);

  gsl_root_fdfsolver_set(s, &FDF, enth[i]);

  int status;
  double enth_tmp = -1.;
  do {
    nr_iter++;

    enth_tmp = enth[i];

    status = gsl_root_fdfsolver_iterate(s);

    enth[i] = gsl_root_fdfsolver_root(s);

    status = gsl_root_test_delta(enth[i], enth_tmp, 0, c2p_eps);

    assert(nr_iter < max_nr_iter);
  } while (status == GSL_CONTINUE);

  gsl_root_fdfsolver_free(s);

  W[i] = std::sqrt(1. + S2 / enth[i] / enth[i]);
  rho[i] = Dstar[i] / sdet / W[i];
  P[i] = Dstar[i] / lapse / sdet * (enth[i] * W[i] - Ebar[i] - SdotBeta);

  if (P[i] < 0.) {
    std::cout << "S2=" << S2 << " "
              << "SdotBeta=" << SdotBeta << " "
              << "Ebar=" << Ebar[i] << " "
              << "P=" << P[i] << " "
              << "rho=" << rho[i] << " "
              << "q=" << q[i] << " "
              << "enth_tmp=" << enth_tmp << " ";
    for (size_t d = 0; d < DIM; ++d) {
      std::cout << (*pos_ptr[d])[i] << ",";
    }

    std::cout << std::endl;
    // std::cerr << "HIT PRESSURE FLOOR" << std::endl;
    assert(0);
  }

  u[i] = enth[i] - 1. - P[i] / rho[i];
  cs[i] = std::sqrt(eos_gamma * P[i] / rho[i] / enth[i]);

  // Fill in v^i
  for (size_t d = 0; d < DIM; ++d) {
    double vbar = SvU[d] / (W[i] * enth[i]);
    (*vel_ptr[d])[i] = lapse * vbar - shift[d];
  }
}
