// Copyright 2016 Matt Kinsey

#include <sys/stat.h>
#include <unordered_map>

#include <H5hut.h>
#include "grsph.h"

namespace grsph {
int io_iter = 0;
std::string h5_filename = "particles.h5part";
bool h5_overwrite = false;
bool h5_output_ghosts = false;
std::vector<std::string> h5_outvars = {{}};
std::unordered_map<std::string, grsph::particle_vector<double>*> var_names = {
    {"x", &x},
    {"S^x", &Sx},
    {"v^x", &vx},
#if dim2
    {"y", &y},
    {"v^y", &vy},
    {"S^y", &Sy},
#endif
#if dim3
    {"z", &z},
    {"S^z", &Sz},
    {"v^z", &vz},
#endif
    {"mass", &mass},
    {"h", &h},
    {"rho", &rho},
    {"P", &P},
    {"u", &u},
    {"omega", &omega},
    {"alpha", &alpha},
    {"nnn", &nnnp},
    {"cs", &cs},
    {"D", &Dstar},
    {"E", &E},
    {"Ebar", &Ebar},
    {"W", &W},
    {"divv", &divv},
    {"q", &q},
    // {"index", &index},
    // proc
    {"enth", &enth},
    {"debug", &debug},
    {"tl", &tlevel},
    {"ghost", &ghost}};
};  // namespace grsph

// TODO add checkpoint/recovery (just serialize the whole class??)

inline bool file_exists(const char* name) {
  struct stat buffer;
  if (stat(name, &buffer) == -1) return 0;
  return 1;
}

void write_pvec_to_h5(h5_file_t* file, const std::string name,
                      grsph::particle_vector<double>* pvec) {
  H5PartWriteDataFloat64(file, name.c_str(),
                         reinterpret_cast<double*>(pvec->data()));
}

void grsph::output_h5part(double t) {
  start_timer("output_h5part");
  int total_num_part;
  int local_num_part = x.size();

  MPI_Allreduce(&local_num_part, &total_num_part, 1, MPI_INT, MPI_SUM,
                MPI_COMM_GRSPH);

  std::string filename =
      io_dir + h5_filename;  // + "_" + std::to_string(mpi_rank);

  std::vector<int> numpart_per_proc;
  numpart_per_proc.resize(mpi_size);

  MPI_Allgather(&local_num_part, 1, MPI_INT, numpart_per_proc.data(), 1,
                MPI_INT, MPI_COMM_GRSPH);

  h5_file_t* file;

  particle_vector<double> procarray(local_num_part);
  std::fill(procarray.begin(), procarray.begin() + local_num_part, mpi_rank);

  const char* io_filename = filename.c_str();

  if (io_iter == 0 && mpi_rank == 0) {
    if (h5_overwrite && file_exists(io_filename)) {
      remove(io_filename);
      std::cout << "Overwriting h5part file: " << io_filename << std::endl;
    }
    assert(!file_exists(io_filename));
  }

  if (mpi_rank == 0 && verbose > 0)
    std::cout << "Outputting " << total_num_part << " particles at time " << t
              << " to " << filename << "... ";

  // Calculate the index of the particles (output as 'index')
  particle_vector<double> particle_vector_index(local_num_part);
  for (int i = 0; i < local_num_part; ++i) {
    particle_vector_index[i] = static_cast<double>(i);
  }

  // Open file
  file = H5OpenFile(io_filename, H5_O_APPEND, MPI_COMM_GRSPH);
  //  file = H5OpenFile(io_filename, H5_VFD_MPIIO_IND, MPI_COMM_GRSPH);

  // Set iteration in file
  H5SetStep(file, io_iter);

  // Set the number of particles this rank will output
  H5PartSetNumParticles(file, local_num_part);

  // Write arrays
  write_pvec_to_h5(file, "x", &x);
#if DIM == 1  // this is bad but it tricks the h5part reader in visit into
  // working
  write_pvec_to_h5(file, "y", &x);
#endif
#if dim2
  write_pvec_to_h5(file, "y", &y);
#endif
#if dim3
  write_pvec_to_h5(file, "z", &z);
#endif
  write_pvec_to_h5(file, "index", &particle_vector_index);

  for (std::string outvar : h5_outvars) {
    if (var_names.count(outvar) > 0) {
      write_pvec_to_h5(file, outvar, var_names[outvar]);
    }
  }

  if (maxtl > 0) {
    write_pvec_to_h5(file, "tl", &tlevel);
  }

  if (mpi_size > 1) {
    write_pvec_to_h5(file, "proc", &procarray);
  }

  if (h5_output_ghosts) {
    write_pvec_to_h5(file, "ghost", &ghost);
  }

  // this is broken with parHDF5...
  H5WriteStepAttribFloat64(file, "time", reinterpret_cast<double*>(&t), 1);

  // Close File
  H5CloseFile(file);

  io_iter++;
  stop_timer("output_h5part");
  if (mpi_rank == 0 && verbose > 0) std::cout << "done" << std::endl;
}
