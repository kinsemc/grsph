// Copyright 2016 Matt Kinsey

#include "grsph.h"
#include "intpow.h"

// This is just a utility to calculate the distance^2 between two particles
// using intpow.h.

// Calculate the distance^2 between particle i and ni without using pow()
double grsph::fast_dist2(const size_t i, const size_t ni) {
  double dist2 = 0;
  for (size_t d = 0; d < DIM; ++d) {
    double xij = (*pos_ptr[d])[i] - (*pos_ptr[d])[ni];
    dist2 += xij * xij;
    // dist2 += intpow_l((*pos_ptr[d])[i] - (*pos_ptr[d])[ni], 2);
  }
  return dist2;
}
