// Copyright 2016 Matt Kinsey

#include <gsl/gsl_errno.h>
#include <gsl/gsl_roots.h>

#include "grsph.h"
#include "intpow.h"

namespace grsph {

double nr_f(double h_g, void *params);
double nr_df(double h_g, void *params);
void nr_fdf(double h_g, void *params, double *f, double *df);
};  // namespace grsph

struct nr_params {
  size_t i;
};

double grsph::nr_f(double h_g, void *params) {
  struct nr_params *p = (struct nr_params *)params;

  size_t i = p->i;
  double dDdh = 0.;
  Dstar[i] = 0.;

  for (const auto &ni : neighbors[i]) {
    double dist2 = fast_dist2(i, ni);
    double distance = std::sqrt(dist2);

    double weight = kernel(distance, h_g);
    double Dhweight = Dhkernel(distance, h_g);

    Dstar[i] += mass[ni] * weight;
    dDdh += mass[ni] * Dhweight;
  }

  omega[i] = 1. + h_g / Dstar[i] / DIM * dDdh;

  return mass[i] * intpow_l(eta / h_g, DIM) - Dstar[i];
}

double grsph::nr_df(double h_g, void *params) {
  struct nr_params *p = (struct nr_params *)params;

  size_t i = p->i;
  double dDdh = 0.;
  Dstar[i] = 0.;

  for (const auto &ni : neighbors[i]) {
    double dist2 = fast_dist2(i, ni);
    double distance = std::sqrt(dist2);

    double weight = kernel(distance, h_g);
    double Dhweight = Dhkernel(distance, h_g);

    Dstar[i] += mass[ni] * weight;
    dDdh += mass[ni] * Dhweight;
  }

  omega[i] = 1. + h_g / Dstar[i] / DIM * dDdh;

  return -DIM / h_g * Dstar[i] - dDdh;
}

void grsph::nr_fdf(double h_g, void *params, double *f, double *df) {
  struct nr_params *p = (struct nr_params *)params;

  size_t i = p->i;
  double dDdh = 0.;
  Dstar[i] = 0.;

  for (const auto &ni : neighbors[i]) {
    double dist2 = fast_dist2(i, ni);
    double distance = std::sqrt(dist2);

    double weight = kernel(distance, h_g);
    double Dhweight = Dhkernel(distance, h_g);

    Dstar[i] += mass[ni] * weight;
    dDdh += mass[ni] * Dhweight;
  }

  omega[i] = 1. + h_g / Dstar[i] / DIM * dDdh;

  *f = mass[i] * intpow_l(eta / h_g, DIM) - Dstar[i];
  *df = -DIM / h_g * Dstar[i] - dDdh;
}

void grsph::particle_D_h_NR_gsl(const size_t i) {
  if (ghost[i]) return;

  // I think this is probably a bad idea but is worth looking into more (we
  // already converge quickly...)
  //    // make h guess dhdt*dt (if you already have dN/dt)
  //    h[i] += 1./DIM*h[i]*divv[i]*dt;

  // Newton-Raphson on h until consistent with D
  int nr_iter = 0;

  struct nr_params params = {i};

  //  gsl_function F;
  //  F.function = &W_func;
  //  F.params = &params;

  gsl_function_fdf FDF;
  FDF.f = &nr_f;
  FDF.df = &nr_df;
  FDF.fdf = &nr_fdf;
  FDF.params = &params;

  const gsl_root_fdfsolver_type *T;
  gsl_root_fdfsolver *s;
  T = gsl_root_fdfsolver_steffenson;
  s = gsl_root_fdfsolver_alloc(T);

  gsl_root_fdfsolver_set(s, &FDF, h[i]);

  int status;
  double h_tmp = -1.;
  // double h_old = h[i];
  do {
    nr_iter++;
    h_tmp = h[i];

    status = gsl_root_fdfsolver_iterate(s);

    h[i] = gsl_root_fdfsolver_root(s);
    status = gsl_root_test_delta(h[i], h_tmp, 0, h_eps);

    // TODO assert doth/h
    // double hdot = (h[i] - h_old) / dt;

    // if (h[i] / h_old > nn_hfac) std::cout << x[i] << std::endl;
    // assert nn_hfac is large enough that we don't miss neighbors
    // assert(h[i] / h_old < nn_hfac);
    // exit if we can't converge
    assert(nr_iter < max_nr_iter);
  } while (status == GSL_CONTINUE);

  double dDdh = 0.;
  Dstar[i] = 0.;

  for (const auto &ni : neighbors[i]) {
    double dist2 = fast_dist2(i, ni);
    double distance = std::sqrt(dist2);

    double weight = kernel(distance, h[i]);
    double Dhweight = Dhkernel(distance, h[i]);

    Dstar[i] += mass[ni] * weight;
    dDdh += mass[ni] * Dhweight;
  }

  omega[i] = 1. + h[i] / Dstar[i] / DIM * dDdh;

  gsl_root_fdfsolver_free(s);
}
