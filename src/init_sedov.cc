// Copyright 2016 Matt Kinsey

// Init sedov blast wave

#include "grsph.h"

void grsph::init_sedov(double P0, double D0, double E0, double dr) {
  size_t gnp = global_np();
  size_t np = x.size();

#pragma omp parallel for
  // this breaks clang <3.8, its a serious wtf bug in the compiler
  for (size_t i = 0; i < np; ++i) {
    mass[i] = support_r * eta / gnp;
    W[i] = 1.;
    P[i] = P0;
    h[i] = 2 * eta * std::pow(mass[i] / D0, 1. / DIM);
  }

  calc_init_D();

  double D_mean = particle_mean(&Dstar);
  np = x.size();
#pragma omp parallel for
  for (size_t i = 0; i < np; ++i) {
    mass[i] *= D0 / D_mean;
    h[i] = eta * std::pow(mass[i] / D0, 1. / DIM);
  }
  // output_h5part(0);
  calc_init_D();

  double Pcenter =
      3. * (eos_gamma - 1.) * E0 / ((DIM + 1.) * M_PI * std::pow(dr, DIM));

  np = x.size();
#pragma omp parallel for
  for (size_t i = 0; i < np; ++i) {
    double r2 = 0.;
    for (size_t d = 0; d < DIM; ++d) {
      r2 += (*pos_ptr[d])[i] * (*pos_ptr[d])[i];
    }
    double r = std::sqrt(r2);

    if (r < dr) P[i] = Pcenter;

    u[i] = P[i] * W[i] / (eos_gamma - 1.) / Dstar[i];
  }
}
