// Copyright 2016 Matt Kinsey

// TODO alot of these function names dont make sense anymore

#include "grsph.h"
//#include "utils.h"

namespace grsph {

MPI_Comm MPI_COMM_GRSPH;
int mpi_rank;
int mpi_size;
std::vector<box> boxes;
std::vector<std::vector<box>> box_tree;
size_t particle_find_rank(size_t i);

std::vector<std::vector<int>> boxsplitdim;
};  // namespace grsph

/* still testing this guy (aka its not working)
 void grsph::rmpart(int i) {
  for (auto& elem : particle_vectors) {
    elem->erase(elem->begin() + i);
  }
}
*/

// Check if a pos (x,y,z) is in a box [b]. Return bool
inline bool checkbox(const std::array<double, DIM>& pos, const grsph::box& b) {
  bool in_box = true;

  for (size_t d = 0; d < DIM; ++d) {
    in_box &= (pos[d] > b[2 * d]) && (pos[d] <= b[2 * d + 1]);
  }

  return in_box;
}

// return the rank that particle i should be living on
size_t grsph::particle_find_rank(size_t i) {
  std::array<double, DIM> pos;
  for (size_t d = 0; d < DIM; ++d) {
    pos[d] = (*pos_ptr[d])[i];
  }

  // Precheck that it belongs on current proc. This is very likely.
  bool inside_mbox = checkbox(pos, boxes[mpi_rank]);

  if (inside_mbox) {
    return mpi_rank;
  } else {
    for (int proc = 0; proc < mpi_size; ++proc) {
      if (proc == mpi_rank) continue;
      bool inside_pbox = checkbox(pos, boxes[proc]);

      if (inside_pbox) {
        return proc;
      }
    }
    assert(0);
  }
}

// Send particles to the rank that owns thier box
void grsph::sync_particles_to_procs() {
  int np = x.size();
  clear_ghosts();

  if (periodic) enforce_periodic_bounds();

  start_timer("sync_particles_to_procs");

  std::vector<particle_t> sendparts;
  std::vector<int> sendcounts;
  std::vector<int> senddisps;
  sendcounts.resize(mpi_size, 0);
  senddisps.resize(mpi_size, 0);

  std::vector<std::vector<int>> sync_tables;
  sync_tables.resize(mpi_size);

  for (int i = 0; i < np; ++i) {
    int proc = particle_find_rank(i);
    // if proc is -1 then we are outside all the boxes
    if (proc == -1) {
      std::cout << "Couldn't find box for " << i << " ";
      for (size_t d = 0; d < DIM; ++d) {
        std::cout << (*pos_ptr[d])[i] << ",";
      }
      std::cout << std::endl;

      std::cout << global_box[0] << " " << global_box[1] << " " << global_box[2]
                << " " << global_box[3] << std::endl;
      print_boxesf();
      assert(proc != -1);
    }

    if (proc != mpi_rank && proc != -1) {
      sync_tables[proc].push_back(i);
      ghost[i] = true;
    }
  }

  for (int proc = 0; proc < mpi_size; ++proc) {
    if (proc == mpi_rank) continue;
    const auto& sync_table = sync_tables[proc];
    for (const auto& i : sync_table) {
      sendparts.push_back(get_particle(i));
      sendcounts[proc]++;
    }
  }

  clear_ghosts();

  std::vector<int> recvcounts;
  std::vector<int> recvdisps;
  recvcounts.resize(mpi_size, 0);
  recvdisps.resize(mpi_size, 0);

  MPI_Alltoall(sendcounts.data(), 1, MPI_INT, recvcounts.data(), 1, MPI_INT,
               MPI_COMM_GRSPH);

  int totrecvsize = recvcounts[0];
  for (int proc = 1; proc < mpi_size; ++proc) {
    senddisps[proc] = sendcounts[proc - 1] + senddisps[proc - 1];
    recvdisps[proc] = recvcounts[proc - 1] + recvdisps[proc - 1];
    totrecvsize += recvcounts[proc];
  }

  std::vector<particle_t> recvparts;
  recvparts.resize(totrecvsize);

  MPI_Alltoallv(sendparts.data(), sendcounts.data(), senddisps.data(),
                mpi_particle_t, recvparts.data(), recvcounts.data(),
                recvdisps.data(), mpi_particle_t, MPI_COMM_GRSPH);

  for (const auto& p : recvparts) {
    push_particle(p, false);
  }
  stop_timer("sync_particles_to_procs");
}

// function to update bisection pos's in the box_tree based on exec time of
// each rank

void grsph::update_ORB_boxes() {
  start_timer("update_ORB_boxes");
  sync_rhs_times();

  double maxrhstime =
      *std::max_element(time_per_cpu.begin(), time_per_cpu.end());

  std::vector<double> wait_times;
  wait_times.resize(mpi_size);
  for (int p = 0; p < mpi_size; ++p) {
    wait_times[p] = maxrhstime - time_per_cpu[p];
    wait_times[p] /= maxrhstime;
  }

  double min_binsize = std::pow(2., l_min);
  double K = min_binsize;
  double r_max = std::pow(2., l_min);

  int nlev = std::log2(mpi_size);
  for (int n = 1; n <= nlev; ++n) {
    int nlevboxes = std::pow(2, n);

    //    for (int bi = 0; bi < box_tree[n - 1].size(); ++bi) {
    for (int bi = 0; bi < nlevboxes / 2; ++bi) {
      box curbox = box_tree[n - 1][bi];
      size_t maxdim = boxsplitdim[n - 1][bi];

      double delta_w_inner = 0;
      for (int p = 0; p < mpi_size; ++p) {
        if (!((p ^ bi) % (nlevboxes))) {
          delta_w_inner += wait_times[p];
        } else if (!((p ^ (bi ^ (nlevboxes / 2))) % (nlevboxes))) {
          delta_w_inner -= wait_times[p];
        }
      }

      double r = 0;
      double Kdw = K * delta_w_inner;
      if (std::abs(Kdw) < r_max) {
        r = Kdw;
      } else if (Kdw > r_max) {
        r = r_max;
      } else if (Kdw < -r_max) {
        r = -r_max;
      }

      double med = box_tree[n][bi][2 * maxdim + 1];
      box b1 = curbox;
      box b2 = curbox;

      b1[2 * maxdim + 1] = med + r;
      b2[2 * maxdim] = med + r;

      box_tree[n][bi] = b1;
      box_tree[n][bi ^ (nlevboxes / 2)] = b2;
    }
  }

  for (int p = 0; p < mpi_size; ++p) {
    boxes[p] = box_tree[nlev][p];
  }

  stop_timer("update_ORB_boxes");
}

void grsph::enforce_periodic_bounds() {
  if (periodic) {
    size_t np = x.size();
    for (size_t i = 0; i < np; ++i) {
      std::array<double, DIM> pos;
      for (size_t d = 0; d < DIM; ++d) {
        pos[d] = (*pos_ptr[d])[i];
      }

      bool not_in_pbox = !checkbox(pos, periodic_box);

      if (not_in_pbox) {
        for (size_t d = 0; d < DIM; ++d) {
          if ((*pos_ptr[d])[i] <= periodic_box[2 * d]) {
            (*pos_ptr[d])[i] += periodic_box[2 * d + 1] - periodic_box[2 * d];
          }
          if ((*pos_ptr[d])[i] > periodic_box[2 * d + 1]) {
            (*pos_ptr[d])[i] -= periodic_box[2 * d + 1] - periodic_box[2 * d];
          }
        }
      }
    }
  }
}

// This function does ORB over the whole domain
void grsph::calc_ORB_boxes() {
  start_timer("calc_ORB_boxes");
  // assert that we have 2^n procs
  assert(std::log2(mpi_size) == std::ceil(std::log2(mpi_size)));
  int nlev = std::log2(mpi_size);

  calc_global_box();

  box_tree.clear();
  boxsplitdim.clear();
  box_tree.resize(nlev + 1);
  boxsplitdim.resize(
      nlev +
      1);  // this only needs to be nlev but leads to a segfault on one proc

  box_tree[0].resize(1);
  box_tree[0][0] = global_box;

  // Handle the case where we only have one proc
  if (nlev == 0) {
    boxes.resize(mpi_size);
    for (int p = 0; p < mpi_size; ++p) {
      boxes[p] = box_tree[nlev][p];
    }

    stop_timer("calc_ORB_boxes");
    return;
  }

  boxsplitdim[0].resize(1);

  particle_vector<size_t> last_box;
  last_box.resize(x.size(), 0);

  double min_binsize = std::pow(2., l_min);

  for (int n = 1; n <= nlev; ++n) {
    std::vector<box> levboxes;
    int nlevboxes = std::pow(2, n);
    levboxes.resize(nlevboxes);
    boxsplitdim[n - 1].resize(std::pow(2, n - 1));

    for (size_t bi = 0; bi < box_tree[n - 1].size(); ++bi) {
      box curbox = box_tree[n - 1][bi];

      std::array<double, DIM> boxsizes;
      for (size_t d = 0; d < DIM; ++d) {
        boxsizes[d] = curbox[2 * d + 1] - curbox[2 * d];
      }

      auto maxdimsize = std::max_element(boxsizes.begin(), boxsizes.end());

      // max dim is the longest dim, akak the one we are gonna chop
      int maxdim = std::distance(boxsizes.begin(), maxdimsize);
      boxsplitdim[n - 1][bi] = maxdim;

      particle_vector<double>* posarray_ptr;

      posarray_ptr = pos_ptr[maxdim];

      // Find the mean coord pos in the maxdim dir
      double local_pos_sum = 0.;
      int local_np_box = 0;
      size_t np = x.size();
      // #pragma omp parallel for reduction(+ : local_pos_sum, local_np_box)
      for (size_t i = 0; i < np; ++i) {
        if (ghost[i] || last_box[i] != bi) continue;
        local_pos_sum = local_pos_sum + (*posarray_ptr)[i];
        local_np_box = local_np_box + 1;
      }
      // just reduce over curbox
      // int inreduce = local_np_box > 0;
      // MPI_Comm inreduce_comm;
      // if(inreduce) {
      //  MPI_Comm_split(MPI_COMM_GRSPH, inreduce, 0 , &inreduce_comm);
      // }else {
      //  MPI_Comm_split(MPI_COMM_GRSPH, MPI_UNDEFINED, 0 , &inreduce_comm);
      // }

      double global_pos_sum;
      //      MPI_Allreduce(&local_pos_sum, &global_pos_sum, 1, MPI_DOUBLE,
      //      MPI_SUM,
      //                    inreduce_comm);
      MPI_Allreduce(&local_pos_sum, &global_pos_sum, 1, MPI_DOUBLE, MPI_SUM,
                    MPI_COMM_GRSPH);

      int global_np_box;
      //      MPI_Allreduce(&local_np_box, &global_np_box, 1, MPI_INT, MPI_SUM,
      //                    inreduce_comm);
      MPI_Allreduce(&local_np_box, &global_np_box, 1, MPI_INT, MPI_SUM,
                    MPI_COMM_GRSPH);

      // MPI_Comm_free(&inreduce_comm);

      double med = global_pos_sum / global_np_box;

      double boundmed =
          std::round((med - global_box[2 * maxdim]) / min_binsize) *
              min_binsize +
          global_box[2 * maxdim];
      // std::cout << boundmed << " " <<med<< std::endl;
      med = boundmed;

      // Chop box in 2 at the middle pos
      box b1 = curbox;
      box b2 = curbox;

      b1[2 * maxdim + 1] = med;
      b2[2 * maxdim] = med;

      levboxes[bi] = b1;
      levboxes[bi ^ (nlevboxes / 2)] = b2;
      // #pragma omp parallel for
      for (size_t i = 0; i < np; ++i) {
        if (ghost[i] || last_box[i] != bi) continue;
        if ((*posarray_ptr)[i] > med)
          last_box[i] = bi ^ (nlevboxes / 2);
        else
          last_box[i] = bi;
      }
      //      boxsplitdim[n-1][bi] = maxdim;
    }
    box_tree[n] = levboxes;
  }

  box bb = box_tree[nlev][mpi_rank];

  if (print_boxes) {
    std::cout << mpi_rank << ": [ ";
    for (const auto& elem : bb) {
      std::cout << elem << ",";
    }
    std::cout << " ]" << std::endl;
  }

  boxes.resize(mpi_size);
  for (int p = 0; p < mpi_size; ++p) {
    boxes[p] = box_tree[nlev][p];
  }

  // TODO reorder boxes via peano
  stop_timer("calc_ORB_boxes");
}

void grsph::print_boxesf() {
  for (int p = 0; p < mpi_size; ++p) {
    box bb = boxes[p];

    std::cout << p << ": [ ";
    for (const auto& elem : bb) {
      std::cout << elem << ",";
    }
    std::cout << " ]" << std::endl;
  }
}
