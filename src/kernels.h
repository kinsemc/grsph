// Copyright 2016 Matt Kinsey

// This header contains the kernel functions and there derivatives.
// Currently implemented are the cubic and quartic splines as well as
// the Wendland C6 kernel. They are dimensionally agnostic.

#ifndef SRC_KERNELS_H_
#define SRC_KERNELS_H_

#define _USE_MATH_DEFINES  // expose M_PI
#include <math.h>

#include "intpow.h"

// Since these are being called by a func pointer, they never get inlined :/

inline double cubic_spline(const double r, const double h) {
  double rnorm = r / h;
  if (rnorm > 2.0) return 0.0;
#if (DIM == 1)
  double c_n = 2. / 3. / h;
#elif (DIM == 2)
  double c_n = 10. / 7. / M_PI / h / h;
#elif (DIM == 3)
  double c_n = 1 / M_PI / h / h / h;
#endif
  if (rnorm > 1.0) return c_n * 0.25 * intpow_l((2. - rnorm), 3);
  return c_n * (1.0 - 1.5 * rnorm * rnorm + 0.75 * rnorm * rnorm * rnorm);
}

inline double Dcubic_spline(const double r, const double h) {
  double rnorm = r / h;
  if (rnorm > 2.0) return 0.0;
#if (DIM == 1)
  double c_n = 2. / 3. / h / h;
#elif (DIM == 2)
  double c_n = 10. / 7. / M_PI / h / h / h;
#elif (DIM == 3)
  double c_n = 1 / M_PI / h / h / h / h;
#endif
  if (rnorm > 1.0) return c_n * -3.0 * 0.25 * intpow_l((rnorm - 2.), 2);
  return c_n * 0.25 * (-12.0 * rnorm + 9.0 * rnorm * rnorm);
}

inline double Dhcubic_spline(const double r, const double h) {
  return -r / h * Dcubic_spline(r, h) - DIM / h * cubic_spline(r, h);
}

inline double quartic_spline(const double r, const double hh) {
  double S = r / hh;
  if (S > 2.0) return 0.0;
#if (DIM == 1)
  double alpha_d = 1. / hh;
#elif (DIM == 2)
  double alpha_d = 15. / 7. / M_PI / hh / hh;
#elif (DIM == 3)
  double alpha_d = 315. / 208. / M_PI / hh / hh / hh;
#endif
  return alpha_d * (2. / 3. - 9. / 8. * S * S + 19. / 24. * S * S * S -
                    5. / 32. * S * S * S * S);
}

inline double Dquartic_spline(const double r, const double hh) {
  double S = r / hh;
  if (S > 2.0) return 0.0;
#if (DIM == 1)
  double alpha_d = 1. / hh / hh;
#elif (DIM == 2)
  double alpha_d = 15. / 7. / M_PI / hh / hh / hh;
#elif (DIM == 3)
  double alpha_d = 315. / 208. / M_PI / hh / hh / hh / hh;
#endif
  return alpha_d * (-9. / 4. * S + 19. / 8. * S * S - 5. / 8. * S * S * S);
}

inline double Dhquartic_spline(const double r, const double hh) {
  return -r / hh * Dquartic_spline(r, hh) - DIM / hh * quartic_spline(r, hh);
}

// This is wendland C6 (http://xxx.tau.ac.il/pdf/1204.2471v1.pdf)
inline double wendland(const double r, const double hh) {
  double S = r / hh;
  if (S > 1.0) return 0.0;
#if (DIM == 1)
  double alpha_d = 55. / 32. / hh;
  return alpha_d * intpow_l(1. - S, 7) *
         (1. + 7. * S + 19. * S * S + 21. * S * S * S);
#elif (DIM == 2)
  double alpha_d = 78. / 7. / M_PI / hh / hh;
  return alpha_d * intpow_l(1. - S, 8) *
         (1. + 8. * S + 25. * S * S + 32. * S * S * S);
#elif (DIM == 3)
  double alpha_d = 1365. / 64 / M_PI / hh / hh / hh;
  return alpha_d * intpow_l(1. - S, 8) *
         (1. + 8. * S + 25. * S * S + 32. * S * S * S);
#endif
}

inline double Dwendland(const double r, const double hh) {
  double S = r / hh;
  if (S > 1.0) return 0.0;
#if (DIM == 1)
  double alpha_d = 55. / 32. / hh;
  return alpha_d * (intpow_l(1. - S, 7) * (1 / r) *
                        (7. * S + 38. * S * S + 63. * S * S * S) -
                    7. / hh * intpow_l(1. - S, 6) *
                        (1. + 7. * S + 19. * S * S + 21. * S * S * S));
#elif (DIM == 2)
  double alpha_d = 78. / 7. / M_PI / hh / hh;
  return alpha_d * (intpow_l(1. - S, 8) * (1 / r) *
                        (8. * S + 50. * S * S + 96. * S * S * S) -
                    8. / hh * intpow_l(1. - S, 7) *
                        (1. + 8. * S + 25. * S * S + 32. * S * S * S));
#elif (DIM == 3)
  double alpha_d = 1365. / 64 / M_PI / hh / hh / hh;
  return alpha_d * (intpow_l(1. - S, 8) * (1 / r) *
                        (8. * S + 50. * S * S + 96. * S * S * S) -
                    8. / hh * intpow_l(1. - S, 7) *
                        (1. + 8. * S + 25. * S * S + 32. * S * S * S));
#endif
}

inline double Dhwendland(const double r, const double hh) {
  return -r / hh * Dwendland(r, hh) - DIM / hh * wendland(r, hh);
}

// "New" quartic spline
// (http://www.sciencedirect.com/science/article/pii/S0377042702008695)

#endif  // SRC_KERNELS_H_
