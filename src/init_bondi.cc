// Copyright 2016 Matt Kinsey

// Initalizes the Bondi solution in Schwarzschild coordinates
// TODO TODO TODO there are multiple unfixed issues in this file.

#include <gsl/gsl_errno.h>
#include <gsl/gsl_roots.h>
#include <fstream>
#include <iostream>

#include "grsph.h"

double bondi_K = 1.;  // TODO make param

// TODO this has some magic numbers in it...
// TODO needs a calc_init_D...
void grsph::init_bondi(const double rc, const double K, const double Rmin,
                       const double Rmax) {
  bondi_K = K;

  // Init radial mesh for CDF
  int N = 1000000;
  // Rmin = 2.;
  double dr = (Rmax - Rmin) / N;
  std::vector<double> rvec;
  for (int i = 0; i < N; ++i) {
    rvec.push_back(Rmin + (i + 1) * dr);
  }
  // output_h5part(0);
  // Calc CDF
  std::vector<double> F = bondi_cum_dens(rc, rvec);

  // Distribute Particles based on CDF
  init_radial_mesh_from_table(rvec, F);
  // output_h5part(0);
  // Calc initial prims
  init_bondi_prims(rc);
}

struct bondi_root_params {
  double n, m, r, C1, C2;
};

double bondi_root_func(double x, void *params) {
  struct bondi_root_params *p = (struct bondi_root_params *)params;

  double n = p->n;
  double m = p->m;
  double r = p->r;
  double C1 = p->C1;
  double C2 = p->C2;
  double T = x;

  return (
      (1 + (1 + n) * T) * (1 + (1 + n) * T) *
          (1 - 2 * m / r + C1 * C1 / (r * r * r * r * std::pow(T, 2. * n))) -
      C2);
}

double deriv_bondi_root_func(double x, void *params) {
  struct bondi_root_params *p = (struct bondi_root_params *)params;

  double n = p->n;
  double m = p->m;
  double r = p->r;
  double C1 = p->C1;
  // double C2 = p->C2;
  double T = x;

  return 2 * (n + 1) * ((n + 1) * T + 1) *
             (C1 * C1 / (std::pow(T, (2. * n)) * r * r * r * r) - (2 * m) / r +
              1) -
         (2 * C1 * C1 * n * std::pow(T, (-(2. * n) - 1.)) *
          std::pow(((n + 1.) * T + 1.), 2.)) /
             r / r / r / r;
  //  return
  //  ((1+(1+n)*T)*(1+(1+n)*T)*(1-2*m/r+C1*C1/(r*r*r*r*std::pow(T,2*n)))-C2);
}

void bondi_fdf(double x, void *params, double *y, double *dy) {
  struct bondi_root_params *p = (struct bondi_root_params *)params;

  double n = p->n;
  double m = p->m;
  double r = p->r;
  double C1 = p->C1;
  double C2 = p->C2;
  double T = x;

  *y = ((1 + (1 + n) * T) * (1 + (1 + n) * T) *
            (1 - 2 * m / r + C1 * C1 / (r * r * r * r * std::pow(T, 2 * n))) -
        C2);
  *dy = 2 * (n + 1) * ((n + 1) * T + 1) *
            (C1 * C1 / (std::pow(T, (2. * n)) * r * r * r * r) - (2 * m) / r +
             1) -
        (2 * C1 * C1 * n * std::pow(T, (-(2. * n) - 1.)) *
         std::pow(((n + 1.) * T + 1.), 2.)) /
            r / r / r / r;
}
/*
std::vector<double> grsph::bondi_dens(double rc, std::vector<double> rvec) {
  double m = spacetime->Mh;
  double n = 1. / (eos_gamma- 1.);

  double uc = std::sqrt(m / (2 * rc));  // uc = std::sqrt(m/2*rc)
  double vc = std::sqrt(uc * uc / (1 - 3 * uc * uc));
  double Tc = -n * vc * vc / ((n + 1.) * (n * vc * vc - 1.));

  double C1 = uc * rc * rc * std::pow(Tc, n);
  double C2 =
      (1 + (1 + n) * Tc) * (1 + (1 + n) * Tc) *
      (1 - 2 * m / rc + C1 * C1 / (rc * rc * rc * rc * std::pow(Tc, 2. * n)));

  std::vector<double> bondi_rho;
  size_t nr = rvec.size();
  bondi_rho.resize(nr);
  for (size_t i = 0; i < nr; ++i) {
    double r = rvec[i];
    // Find T(r) via root finding
    int status;
    int iter = 0;
    const gsl_root_fdfsolver_type *gsl_T;
    gsl_root_fdfsolver *s;

    gsl_function_fdf FDF;
    struct bondi_root_params params = {n, m, r, C1, C2};
    FDF.f = &bondi_root_func;
    FDF.df = &deriv_bondi_root_func;
    FDF.fdf = &bondi_fdf;
    FDF.params = &params;

    double T = Tc;

    gsl_T = gsl_root_fdfsolver_newton;
    s = gsl_root_fdfsolver_alloc(gsl_T);
    gsl_root_fdfsolver_set(s, &FDF, T);

    do {
      iter++;
      status = gsl_root_fdfsolver_iterate(s);
      double Ttmp = T;
      T = gsl_root_fdfsolver_root(s);
      status = gsl_root_test_delta(T, Ttmp, 0, 1e-8);
    } while (status == GSL_CONTINUE && iter < max_nr_iter);

    gsl_root_fdfsolver_free(s);

    // Now that we have T=P/rho we can set the other vars
    //rho =
    bondi_rho.push_back(bondi_K*std::pow(T, n));  // K = 1
  }
  return bondi_rho;
}
*/
std::vector<double> grsph::bondi_cum_dens(double rc, std::vector<double> rvec) {
  std::cout << "Calculating rho CDF for Bondi Soln with r_c=" << rc << "... ";
  double m = spacetime->Mh;
  double n = 1. / (eos_gamma - 1.);

  double uc = std::sqrt(m / (2. * rc));  // uc = std::sqrt(m/2*rc)
  double Vc = std::sqrt(uc * uc / (1 - 3 * uc * uc));
  double Tc = -n * Vc * Vc / ((n + 1.) * (n * Vc * Vc - 1.));

  double C1 = uc * rc * rc * std::pow(Tc, n);
  double C2 =
      (1 + (1 + n) * Tc) * (1 + (1 + n) * Tc) *
      (1 - 2 * m / rc + C1 * C1 / (rc * rc * rc * rc * std::pow(Tc, 2. * n)));

  std::ofstream file;
  file.open("bondi.dat");

  std::vector<double> bondi_D;
  size_t nr = rvec.size();
  bondi_D.resize(nr);
  for (size_t i = 0; i < nr; ++i) {
    double r = rvec[i];
    //    std::cout << r << std::endl;
    // Find T(r) via root finding
    int status;
    int iter = 0;
    const gsl_root_fdfsolver_type *gsl_T;
    gsl_root_fdfsolver *s;

    gsl_function_fdf FDF;
    struct bondi_root_params params = {n, m, r, C1, C2};
    FDF.f = &bondi_root_func;
    FDF.df = &deriv_bondi_root_func;
    FDF.fdf = &bondi_fdf;
    FDF.params = &params;

    double T = Tc;

    gsl_T = gsl_root_fdfsolver_newton;
    s = gsl_root_fdfsolver_alloc(gsl_T);
    gsl_root_fdfsolver_set(s, &FDF, T);

    do {
      iter++;
      status = gsl_root_fdfsolver_iterate(s);
      double Ttmp = T;
      T = gsl_root_fdfsolver_root(s);
      status = gsl_root_test_delta(T, Ttmp, 0, 1e-8);
    } while (status == GSL_CONTINUE && iter < max_nr_iter);

    gsl_root_fdfsolver_free(s);

    // Now that we have T=P/rho we can set the other vars
    double bondi_rho = bondi_K * std::pow(T, n);  // TODO ? K = 1
    double ur = C1 / (bondi_rho * r * r);
    double bondi_P = bondi_rho * T;
    double bondi_u =
        bondi_P / (bondi_rho * (eos_gamma - 1.));  // TODO THIS DEPENDS ON EOS!

    // enth[i] = 1. + u[i] + P[i]/rho[i];

    std::array<double, 3> pos = {{0.}};
    pos[0] = r;
    std::array<double, DIM> Ui = {{}};
    for (size_t d = 0; d < DIM; ++d) {
      //      pos[d] = (*pos_ptr[d])[i];
      Ui[d] = -ur * pos[d] / r;
    }

    auto lapse = spacetime->lapse(pos);
    auto betai = spacetime->shift(pos);
    auto gammaij = spacetime->gammaij(pos);

    double quad_a = 0, quad_b = 0, quad_c = 0;
    quad_a += -lapse * lapse;
    for (size_t di = 0; di < DIM; ++di) {
      for (size_t dj = 0; dj < DIM; ++dj) {
        quad_a += gammaij[di][dj] * betai[di] * betai[dj];
        quad_b += 2. * gammaij[di][dj] * betai[di] * Ui[dj];
        quad_c += gammaij[di][dj] * Ui[di] * Ui[dj];
        // std::cout << gammaij[di][dj] << " ";
      }
    }
    // std::cout << std::endl;
    quad_c += 1.;

    double rootp =
        (-quad_b + std::sqrt(quad_b * quad_b - 4. * quad_a * quad_c)) /
        (2. * quad_a);
    double rootm =
        (-quad_b - std::sqrt(quad_b * quad_b - 4. * quad_a * quad_c)) /
        (2. * quad_a);

    double Ut = std::max(rootp, rootm);
    double sdet = std::sqrt(spacetime->sdet(pos));
    // W[i] = 1. / std::sqrt(1. - V2);
    double bondi_W = lapse * Ut;
    bondi_D[i] = sdet * bondi_W * bondi_rho;
    file << r << " " << bondi_D[i] << " " << bondi_W << " " << bondi_u << " "
         << Ui[0] / Ut << " " << bondi_P << " " << bondi_rho << std::endl;
  }
  std::vector<double> cumF;
  cumF.resize(nr);
  cumF[0] = 0.;
  for (size_t i = 1; i < nr; ++i) {
    double dr = rvec[i] - rvec[i - 1];
    cumF[i] = cumF[i - 1] +
              dr / 2. * (rvec[i] * bondi_D[i] + rvec[i - 1] * bondi_D[i - 1]) /
                  rvec[i];  // *rvec[i]; //!!!!!!!!!!!!! /r!
  }

  // Normalize by the max value in cumF
  double maxF = cumF[nr - 1];

  for (size_t i = 0; i < nr; ++i) {
    cumF[i] /= maxF;
  }

  std::cout << "done" << std::endl;
  // for (size_t i = 0; i < rvec.size(); ++i) {
  // std::cout << rvec[i] << " " << cumF[i] << std::endl;
  //}
  file.close();
  return cumF;
}

// void grsph::init_bondi_h() {
//  for( int i = 0; i < x.size(); ++i) {

//  }
//}
// this is in BL coords...
void grsph::init_bondi_prims(double rc) {
  std::cout << "Initializing hydro vars for Bondi Soln with r_c=" << rc
            << "... ";
  double m = spacetime->Mh;
  double n = 1. / (eos_gamma - 1.);

  double uc = std::sqrt(m / (2 * rc));
  double vc = std::sqrt(uc * uc / (1 - 3 * uc * uc));
  double Tc = n * vc * vc / ((n + 1.) * (1. - n * vc * vc));

  double C1 = uc * rc * rc * std::pow(Tc, n);
  double C2 =
      (1 + (1 + n) * Tc) * (1 + (1 + n) * Tc) *
      (1 - 2 * m / rc + C1 * C1 / (rc * rc * rc * rc * std::pow(Tc, 2. * n)));

  double np = x.size();
  for (size_t i = 0; i < x.size(); ++i) {
    // these might not be right b/c of the metric
    // check their usage below
    double r2 = 0;
    for (size_t d = 0; d < DIM; ++d) {
      r2 += (*pos_ptr[d])[i] * (*pos_ptr[d])[i];
    }
    double r = std::sqrt(r2);

    // Find T(r) via root finding
    int status;
    int iter = 0;
    const gsl_root_fdfsolver_type *gsl_T;
    gsl_root_fdfsolver *s;

    gsl_function_fdf FDF;
    struct bondi_root_params params = {n, m, r, C1, C2};
    // std::cout << n << " " << m << " " << r << " " << C1 << " " << C2 <<
    // std::endl;
    // std::cout << x[i] << " " << y[i] << " " << z[i] << std::endl;
    FDF.f = &bondi_root_func;
    FDF.df = &deriv_bondi_root_func;
    FDF.fdf = &bondi_fdf;
    FDF.params = &params;

    double T = Tc;

    gsl_T = gsl_root_fdfsolver_steffenson;
    s = gsl_root_fdfsolver_alloc(gsl_T);
    gsl_root_fdfsolver_set(s, &FDF, T);

    do {
      iter++;
      status = gsl_root_fdfsolver_iterate(s);
      double Ttmp = T;
      T = gsl_root_fdfsolver_root(s);
      status = gsl_root_test_delta(T, Ttmp, 0, 1e-8);
    } while (status == GSL_CONTINUE && iter < max_nr_iter);

    gsl_root_fdfsolver_free(s);

    //    double R = r;  // WHY?

    // Now that we have T=P/rho we can set the other vars
    rho[i] = bondi_K * std::pow(T, n);  // K = 1
    double ur = C1 / (rho[i] * r * r);
    P[i] = rho[i] * T;
    u[i] = P[i] / (rho[i] * (eos_gamma - 1.));  // TODO THIS DEPENDS ON EOS!

    enth[i] = 1. + u[i] + P[i] / rho[i];

    std::array<double, 3> pos = {{0.}};
    std::array<double, DIM> Ui = {{}};
    for (size_t d = 0; d < DIM; ++d) {
      pos[d] = (*pos_ptr[d])[i];
      Ui[d] = -ur * (*pos_ptr[d])[i] / r;
    }

    auto lapse = spacetime->lapse(pos);
    auto betai = spacetime->shift(pos);
    auto gammaij = spacetime->gammaij(pos);

    double quad_a = 0, quad_b = 0, quad_c = 0;
    quad_a += -lapse * lapse;
    for (size_t di = 0; di < DIM; ++di) {
      for (size_t dj = 0; dj < DIM; ++dj) {
        quad_a += gammaij[di][dj] * betai[di] * betai[dj];
        quad_b += 2. * gammaij[di][dj] * betai[di] * Ui[dj];
        quad_c += gammaij[di][dj] * Ui[di] * Ui[dj];
        // std::cout << gammaij[di][dj] << " ";
      }
    }
    // std::cout << std::endl;
    quad_c += 1.;

    double rootp =
        (-quad_b + std::sqrt(quad_b * quad_b - 4. * quad_a * quad_c)) /
        (2. * quad_a);
    double rootm =
        (-quad_b - std::sqrt(quad_b * quad_b - 4. * quad_a * quad_c)) /
        (2. * quad_a);

    double Ut = std::max(rootp, rootm);
    // std::cout <<  Ut << " " << lapse << std::endl;
    for (size_t d = 0; d < DIM; ++d) {
      (*vel_ptr[d])[i] = Ui[d] / Ut;
    }

    double V2 = 0;
    for (size_t di = 0; di < DIM; ++di) {
      for (size_t dj = 0; dj < DIM; ++dj) {
        V2 += gammaij[di][dj] * ((*vel_ptr[di])[i] + betai[di]) *
              ((*vel_ptr[dj])[i] + betai[dj]) / lapse / lapse;
      }
    }
    debug[i] = std::sqrt(V2);
    //    double gCovtt = -(1. - 2. / r);
    //    double gContt = 1. / gCovtt;
    //    double gamma_b = std::sqrt(1. + gCovtt * ur * ur);
    //    double alpha_b = -1. / std::sqrt(-gContt);
    //    double V = alpha_b * ur / gamma_b;

    //    double v2 = 0.;
    //    std::array<double, 3> pos = {{}};
    //    for (size_t d = 0; d < DIM; ++d) {
    //      (*vel_ptr[d])[i] = V * (*pos_ptr[d])[i] / r;
    //      v2 += (*vel_ptr[d])[i] * (*vel_ptr[d])[i];
    //      pos[d] = (*pos_ptr[d])[i];
    //    }

    double sdet = std::sqrt(spacetime->sdet(pos));
    // W[i] = 1. / std::sqrt(1. - V2);
    W[i] = lapse * Ut;
    Dstar[i] = sdet * W[i] * rho[i];
    mass[i] = 200. / np;  /// sdet/W[i];
    h[i] = eta * std::pow(mass[i] / Dstar[i], 1. / DIM);
    cs[i] = std::sqrt(eos_gamma * P[i] / rho[i] / enth[i]);
  }
  output_h5part(0);
  calc_init_D();

  double avg_D_fac = 0;
  for (size_t i = 0; i < x.size(); ++i) {
    std::array<double, 3> pos = {{}};
    for (size_t d = 0; d < DIM; ++d) {
      pos[d] = (*pos_ptr[d])[i];
    }

    double sdet = std::sqrt(spacetime->sdet(pos));
    double D_goal = sdet * W[i] * rho[i];
    // mass[i] *= Dstar[i]/D_goal;
    avg_D_fac += Dstar[i] / D_goal;
    // h[i] = eta * std::pow(mass[i] / Dstar[i], 1. / DIM);
  }

  avg_D_fac /= x.size();

  for (size_t i = 0; i < x.size(); ++i) {
    mass[i] /= avg_D_fac;
    Dstar[i] /= avg_D_fac;
  }

  std::cout << "done" << std::endl;
}
