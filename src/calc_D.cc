// Copyright 2016 Matt Kinsey

#include "grsph.h"
#include "intpow.h"

namespace grsph {

particle_func_t calc_D_func;
double h_eps = std::pow(10.0, -4);
};  // namespace grsph

// Wrapper function for calc_D_func
void grsph::calc_D() {
  start_timer("calc_Dstar");
  particle_loop(calc_D_func);
  stop_timer("calc_Dstar");
}

void grsph::particle_D_h_NR(const size_t i) {
  if (ghost[i]) return;

  // I think this is probably a bad idea but is worth looking into more (we
  // already converge quickly...)
  //    // make h guess dhdt*dt (you already have dD/dt)
  //    h[i] += 1./DIM*h[i]*divv[i]*dt;

  // Newton-Raphson on h until consistent with D
  int nr_iter = 0;

  while (true) {
    Dstar[i] = 0.0;
    double dDdh = 0.0;

    for (const auto& ni : neighbors[i]) {
      double dist2 = fast_dist2(i, ni);
      double distance = std::sqrt(dist2);

      double weight = kernel(distance, h[i]);
      double Dhweight = Dhkernel(distance, h[i]);

      Dstar[i] += mass[ni] * weight;
      dDdh += mass[ni] * Dhweight;
    }

    omega[i] = 1. + h[i] / Dstar[i] / DIM * dDdh;

    double zeta = mass[i] * intpow_l(eta / h[i], DIM) - Dstar[i];

    if (std::abs(zeta / DIM / Dstar[i] / omega[i]) < h_eps) {
      break;  // break out of while(true)
    }

    // Update h using deriv
    h[i] *= (1. + zeta / DIM / Dstar[i] / omega[i]);

    if (nr_iter >= max_nr_iter || !std::isfinite(h[i]) || h[i] <= 0.) {
      std::cout << "N/h root finding failed @ x=" << x[i] << " "
                << "i=" << i << " "
                << "h=" << h[i] << " "
                << "N_nn=" << nnnp[i] << " "
                << "Dstar=" << Dstar[i] << " "
                << "dDdh=" << dDdh << " " << std::endl;

      assert(0);
      // TODO assert nn_hfac and hdot
    }
    nr_iter++;
  }
}
