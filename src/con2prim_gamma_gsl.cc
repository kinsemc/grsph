// Copyright 2016 Matt Kinsey

// con2prim for a gamma law EoS using gsl
// root finding is performed on the lorentz factor, W

#include <gsl/gsl_errno.h>
#include <gsl/gsl_roots.h>

#include "grsph.h"

struct W_params {
  double S2, Et, G;
};

double W_func(double W, void *params) {
  struct W_params *p = (struct W_params *)params;

  double S2 = p->S2;
  double Et = p->Et;
  double Et2 = Et * Et;
  double G = p->G;

  return (S2 - Et2) * W * W * W * W + 2. * G * Et * W * W * W +
         (Et2 - 2. * G * S2 - G * G) * W * W - 2. * G * Et * W +
         G * G * (1. + S2);
}

double W_func_deriv(double W, void *params) {
  struct W_params *p = (struct W_params *)params;

  double S2 = p->S2;
  double Et = p->Et;
  double Et2 = Et * Et;
  double G = p->G;

  return 4. * (S2 - Et2) * W * W * W + 6. * G * Et * W * W +
         2. * (Et2 - 2. * G * S2 - G * G) * W - 2. * G * Et;
}

void W_fdf(double W, void *params, double *f, double *df) {
  struct W_params *p = (struct W_params *)params;

  double S2 = p->S2;
  double Et = p->Et;
  double Et2 = Et * Et;
  double G = p->G;

  *f = (S2 - Et2) * W * W * W * W + 2. * G * Et * W * W * W +
       (Et2 - 2. * G * S2 - G * G) * W * W - 2. * G * Et * W +
       G * G * (1. + S2);
  *df = 4. * (S2 - Et2) * W * W * W + 6. * G * Et * W * W +
        2. * (Et2 - 2. * G * S2 - G * G) * W - 2. * G * Et;
}

void grsph::particle_grc2p_gamma_gsl(const size_t i) {
  if (ghost[i]) return;

  std::array<double, 3> pos = {{0.}};
  for (size_t d = 0; d < DIM; ++d) {
    pos[d] = (*pos_ptr[d])[i];
  }

  double sdet = std::sqrt(spacetime->sdet(pos));
  auto metU = spacetime->gmunuU(pos);  // full metric g^munu
  double lapse = spacetime->lapse(pos);
  auto shift = spacetime->shift(pos);

  // todo get this from spacetime->..
  std::vector<std::vector<double>> gammaU(DIM, std::vector<double>(DIM));

  for (int ii = 1; ii < DIM + 1; ++ii) {
    for (int jj = 1; jj < DIM + 1; ++jj) {
      gammaU[ii - 1][jj - 1] =
          metU[ii][jj] + shift[ii - 1] * shift[jj - 1] / lapse / lapse;
    }
  }

  // S^i
  std::array<double, DIM> SvU = {};

  double S2 = 0.;
  debug[i] = 0.;
  for (int ii = 0; ii < DIM; ++ii) {
    for (int jj = 0; jj < DIM; ++jj) {
      S2 += gammaU[ii][jj] * (*S_ptr[ii])[i] * (*S_ptr[jj])[i];
      SvU[ii] += gammaU[ii][jj] * (*S_ptr[jj])[i];
    }
  }

  double tmp = 0;
  for (int ii = 0; ii < DIM; ++ii) {
    tmp += shift[ii] * (*S_ptr[ii])[i];
  }

  E[i] = (Ebar[i] + tmp) / lapse;
  double Et = E[i] + sdet * q[i] / eos_gamma / Dstar[i];
  double G = 1. - 1. / eos_gamma;

  int nr_iter = 0;

  struct W_params params = {S2, Et, G};
  // gsl_function F;
  // F.function = &W_func;
  // F.params = &params;

  gsl_function_fdf FDF;
  FDF.f = &W_func;
  FDF.df = &W_func_deriv;
  FDF.fdf = &W_fdf;
  FDF.params = &params;

  //  const gsl_root_fsolver_type *T;
  //  gsl_root_fsolver *s;
  //  T = gsl_root_fsolver_brent;
  const gsl_root_fdfsolver_type *T;
  gsl_root_fdfsolver *s;
  T = gsl_root_fdfsolver_steffenson;
  s = gsl_root_fdfsolver_alloc(T);

  gsl_root_fdfsolver_set(s, &FDF, W[i]);
  //  s = gsl_root_fsolver_alloc(T);

  //  double W_lo = 1.;
  //  double W_hi = 1000000.;
  //  gsl_root_fsolver_set(s, &F, W_lo, W_hi);

  int status;
  double W_tmp = -1.;
  do {
    nr_iter++;

    W_tmp = W[i];

    status = gsl_root_fdfsolver_iterate(s);
    //    status = gsl_root_fsolver_iterate(s);
    //    W_tmp = gsl_root_fsolver_root(s);
    //    W_lo = gsl_root_fsolver_x_lower(s);
    //    W_hi = gsl_root_fsolver_x_upper(s);
    //    double W_eps = std::pow(10.0, con2primexp);
    // Im pretty sure this should be rel not abs (flip the last two)

    //    status = gsl_root_test_interval(W_lo, W_hi, W_eps * W_tmp
    //    ,0.);

    W[i] = gsl_root_fdfsolver_root(s);
    status = gsl_root_test_delta(W[i], W_tmp, 0, c2p_eps);

    assert(nr_iter < max_nr_iter);
  } while (status == GSL_CONTINUE);

  gsl_root_fdfsolver_free(s);

  //  W[i] = W_tmp;
  rho[i] = Dstar[i] / sdet / W[i];
  //      P[i] = G * ((rho[i] * Et * W_n - rho[i] * G) / (W_n * W_n -
  //      G) -
  //                  q[i] - rho[i]);
  P[i] =
      rho[i] * G * ((Et * W[i] - G) / (W[i] * W[i] - G) - q[i] / rho[i] - 1.);
  //      P[i] = 1. / (1. + 1. / (eos_gamma- 1)) *
  //             (rho[i] * (Et * W_n - G) / (W_n * W_n - G) - q[i] -
  //             rho[i]);
  //      enth[i] = (Et * W_n - G) / (W_n * W_n - G) - q[i] / rho[i];
  //    double W_eps = std::pow(10.0, con2primexp);
  // if (P[i] < 0. && P[i] > -W_eps) P[i] = 0.;
  if (P[i] < 0.) {
    std::cout << "S2=" << S2 << " "
              << "Et=" << Et << " "
              << "G=" << G << " "
              << "P=" << P[i] << " "
              << "rho=" << rho[i] << " "
              << "q=" << q[i] << " "
              << "W=" << W_tmp << " ";
    for (size_t d = 0; d < DIM; ++d) {
      std::cout << (*pos_ptr[d])[i] << ",";
    }

    std::cout << std::endl;
    // std::cerr << "HIT PRESSURE FLOOR" << std::endl;
    assert(0);
  }

  enth[i] = 1. + P[i] / G / rho[i];
  u[i] = enth[i] - 1. - P[i] / rho[i];
  cs[i] = std::sqrt(eos_gamma * P[i] / rho[i] / enth[i]);

  debug[i] = S2 / enth[i] / enth[i] / W[i] / W[i];

  // Fill in v^i
  for (size_t d = 0; d < DIM; ++d) {
    double vbar = SvU[d] / (W[i] * (enth[i] + q[i] / rho[i]));
    (*vel_ptr[d])[i] = lapse * vbar - shift[d];
  }
}
