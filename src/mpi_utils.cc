// Copyright 2016 Matt Kinsey

#include "grsph.h"

#define MPI_STRUCT_SIZE 18

namespace grsph {
MPI_Datatype mpi_particle_t;
};  // namespace grsph

// Init the MPI data struct for a particle (def in particle.h)
void grsph::init_mpi_particle_t() {
  int blocklengths[MPI_STRUCT_SIZE] = {DIM, DIM, 1, 1, 1, 1, 1, 1, 1,
                                       1,   DIM, 1, 1, 1, 1, 1, 1, DIM * DIM};
  MPI_Datatype types[MPI_STRUCT_SIZE] = {
      MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE,
      MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE,
      MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE};

  MPI_Aint offsets[MPI_STRUCT_SIZE];

  offsets[0] = offsetof(particle_t, pos);
  offsets[1] = offsetof(particle_t, vel);
  offsets[2] = offsetof(particle_t, mass);
  offsets[3] = offsetof(particle_t, h);
  offsets[4] = offsetof(particle_t, rho);
  offsets[5] = offsetof(particle_t, P);
  offsets[6] = offsetof(particle_t, q);
  offsets[7] = offsetof(particle_t, u);
  offsets[8] = offsetof(particle_t, omega);
  offsets[9] = offsetof(particle_t, alpha);
  offsets[10] = offsetof(particle_t, Svec);
  offsets[11] = offsetof(particle_t, W);
  offsets[12] = offsetof(particle_t, Ebar);
  offsets[13] = offsetof(particle_t, Dstar);
  offsets[14] = offsetof(particle_t, enth);
  offsets[15] = offsetof(particle_t, divv);
  offsets[16] = offsetof(particle_t, cs);
  offsets[17] = offsetof(particle_t, C);

  MPI_Type_create_struct(MPI_STRUCT_SIZE, blocklengths, offsets, types,
                         &mpi_particle_t);
  MPI_Type_commit(&mpi_particle_t);
}
