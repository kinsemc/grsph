// Copyright 2016 Matt Kinsey

// These are two routines to calculate x^n when n is a positive integer.
// This operation gets called so frequently in the code that we force
// optimization. Left to it's own devices, the compilers tested still
// use the same method as when n is a double. For small n, just multiplying
// is much faster.

#ifndef SRC_INTPOW_H_
#define SRC_INTPOW_H_

// Computes x^n, where n is a natural number.
inline double intpow(double x, unsigned int n) {
  // n = 2*d + r. x^n = (x^2)^d * x^r.
  unsigned d = n >> 1;
  unsigned r = n & 1;
  double x_2_d = d == 0 ? 1 : intpow(x * x, d);
  double x_r = r == 0 ? 1 : x;
  return x_2_d * x_r;
}

// The linear implementation.
inline double intpow_l(double x, unsigned int n) {
  double y = 1;
  for (unsigned int i = 0; i < n; ++i) y *= x;
  return y;
}

// Trying running with this one in vtune. Its ~5x slower
// inline double intpow_l(double x, unsigned int n) { return std::pow(x, n); }

#endif  // SRC_INTPOW_H_
