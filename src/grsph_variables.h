// Copyright 2016 Matt Kinsey

// TODO TODO TODO organize this

#ifndef SRC_GRSPH_VARIABLES_H_
#define SRC_GRSPH_VARIABLES_H_

// TBB types
#include <tbb/concurrent_vector.h>
// #include <tbb/concurrent_unordered_set.h>
#include <tbb/cache_aligned_allocator.h>
#include <tbb/concurrent_unordered_map.h>

#include <string>
#include <vector>

#include "grsph_types.h"

namespace grsph {

// ============================================
// Global GRSPH Variables // todo there are too many...
// ============================================

// General config vars
extern bool periodic;
extern double clean_inside_r;
extern int iter;
extern double dt;
extern double t;
extern double tmax;
extern int h5_out_every;
extern int recalc_ORB_boxes_every;
extern int update_ORB_boxes_every;
extern int sort_every;
extern int print_timers_every;
extern int print_conservation_every;
extern int print_basic_every;
extern int max_nr_iter;  // max newton's method iters
extern MPI_Comm MPI_COMM_GRSPH;

// mesh
extern std::function<void()> mesh_func;
extern std::array<double, DIM> init_shift;
extern std::array<double, DIM> init_scale;
extern bool init_glass;
extern double perturb_max;
extern double glass_max_err;
extern int glass_out_every;

// hydro init
extern std::function<void()> init_hydro_prims;

// Hashtable and related variables
extern ALIGNED tbb::concurrent_unordered_multimap<unsigned int, size_t>
    hash_table;
extern ALIGNED tbb::concurrent_vector<tbb::concurrent_vector<size_t>,
                                      tbb::cache_aligned_allocator<size_t>>
    neighbors;  // maybe worth replacing with a return...
extern std::array<double, DIM> hash_coord_min;
extern std::vector<double> inverse_bin_sizes;
extern const unsigned int table_size;
extern int l_min, l_max;
extern size_t nlvls;
extern const int inum;  // shoud always be 1
extern const double nn_hfac;

// Kernel variables (definitions in kernel.h)
// these dont have sensible defaults without knowing the kernel
extern double eta;
extern double support_r;
extern kernel_func_t kernel;    // ptr to kernel
extern kernel_func_t Dkernel;   // ptr to kernel deriv w.r.t. r
extern kernel_func_t Dhkernel;  // ptr to kernel deriv w.t.t. h

// EoS variables
extern double eos_gamma;          // for poly and gamma
extern double K;                  // for poly
extern double c2p_eps;            // root finding relative tolerance
extern particle_func_t c2p_func;  // ptr to particle_con2prim function
extern particle_func_t p2c_func;  // ptr to particle_prim2con function

// MPI variables
extern MPI_Datatype mpi_particle_t;
void init_mpi_particle_t();
extern int mpi_rank;  // todo size_t
extern int mpi_size;  // todo size_t
extern std::vector<box> boxes;
extern box global_box;
extern box periodic_box;
extern bool print_boxes;
extern std::vector<double> time_per_cpu;

// Integrator variables
extern int maxtl;  // max allowed timelevel (dt = 2^tl * dt_base)
extern std::function<void()> rhs_func;
extern std::function<void()> integrator_func;

// D calc vars
extern double h_eps;                 // root finding relative tolerance
extern particle_func_t calc_D_func;  // ptr to particle_calc_D function

// Dissipation trigger variables
extern double alpha_min;  // min value for dissipation param alpha
extern double alpha_max;  // max value for dissipation param alpha
extern double N_noise;    // value for noise trigger

// IO variables
extern std::string io_dir;  // full path to output directory
// io_h5part.cc
extern std::string h5_filename;  // filename of h5part file
extern bool h5_overwrite;        // overwrite h5part file?
extern bool h5_output_ghosts;    // include ghosts in h5part output?
extern std::vector<std::string> h5_outvars;
extern std::unordered_map<std::string, grsph::particle_vector<double> *>
    var_names;
// io_stdout.cc
extern int verbose;  // vebosity level
// rename these, more like 'reductions' than cons values
extern bool cons_csv;
extern bool cons_csv_overwrite;
extern std::string cons_filename;

extern Spacetime *spacetime;  // Pure virtual class to make calls against
extern Kerr kerr;
extern Minkowski minkowski;
extern Schwarzschild schwarzschild;

// Particle vec declarations
extern std::vector<particle_vector<double> *> particle_vectors;

extern particle_vector<double> x, Sx, vx;
extern particle_vector<double> Sxrhs, xtmp, vxtmp, Sxtmp, C00;
#if dim2
extern particle_vector<double> y, Sy, vy;
extern particle_vector<double> Syrhs, ytmp, vytmp, Sytmp, C01, C10, C11;
#endif
#if dim3
extern particle_vector<double> z, Sz, vz;
extern particle_vector<double> Szrhs, ztmp, vztmp, Sztmp, C02, C12, C20, C21,
    C22;
#endif

// TODO organize these somehow, this is all mixed up
extern particle_vector<double> mass, h, P, u;
extern particle_vector<double> rho;
extern particle_vector<double> omega, alpha;
extern particle_vector<double> nnnp;
extern particle_vector<double> ghost;
extern particle_vector<double> W, Ebar, Dstar, enth, divv, divvdt;
extern particle_vector<double> q, E, cs;
extern particle_vector<double> tlevel;

extern particle_vector<double> debug;

// rhs/tmp vars
extern particle_vector<double> Ebarrhs, alpharhs;
extern particle_vector<double> Ebartmp, alphatmp;

// ptr vars
extern const std::array<particle_vector<double> *, DIM> pos_ptr, vel_ptr, S_ptr,
    Srhs_ptr, postmp_ptr, veltmp_ptr, Stmp_ptr;
extern const std::array<std::array<particle_vector<double> *, DIM>, DIM> C_ptr;
extern const std::array<unsigned int, DIM + 1> large_primes;
};  // namespace grsph

#endif  // SRC_GRSPH_VARIABLES_H_
