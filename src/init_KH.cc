// Copyright 2016 Matt Kinsey

// Initalize Kelvin-Helmholtz
// TODO add some smoothing between the fluid regions

#include "grsph.h"

double fermi_func(double A, double B, double yt, double dy, double y) {
  return (A - B) / (exp((y - yt) / dy) + 1.) + B;
}

double double_step(double A1, double A2, double A3, double y1, double y2,
                   double dy, double y) {
  return fermi_func(A1, A2, y1, dy, y) + fermi_func(A2, A3, y2, dy, y) - A2;
}

void grsph::init_KH(const double vy0) {
#if DIM == 2

  size_t np = x.size();
  size_t gnp = global_np();

  for (size_t i = 0; i < np; ++i) {
    // Set dens and vx via fermi_funcs
    // todo might need a better measure of dy
    double dy = eta * std::pow(4. / gnp, 1. / DIM);
    dy *= 2;
    mass[i] = double_step(4. / gnp, 8. / gnp, 4. / gnp, -.5, .5, dy, y[i]);
    Dstar[i] = double_step(1., 2., 1., -.5, .5, dy, y[i]);
    vx[i] = double_step(-.2, .2, -.2, -.5, .5, dy, y[i]);

    // Perturb vy
    vy[i] = vy0 * sin(2. * M_PI * x[i]);

    // Calculate the other prims
    double v2 = 0.;
    for (size_t d = 0; d < DIM; ++d) {
      v2 += (*vel_ptr[d])[i] * (*vel_ptr[d])[i];
    }
    W[i] = 1. / std::sqrt(1. - v2);

    P[i] = 1.;
    u[i] = P[i] * W[i] / (eos_gamma - 1.) / Dstar[i];
    h[i] = eta * std::pow(mass[i], 1. / DIM);
  }
#else
  assert(0);
#endif
}
