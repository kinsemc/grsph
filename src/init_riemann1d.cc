// Copyright 2016 Matt Kinsey

#include "grsph.h"

void grsph::init_riemann1d(double Dleft, double Dright, double Pleft,
                           double Pright, double psep, double dsmooth,
                           double xminmax, double scale) {
  double lb = -xminmax * scale;
  double ub = xminmax * scale;
  double xc = (lb + ub) / 2.;

  psep *= scale;

  // TODO magic number resulting from the kernel...
  Dleft *= 1.78105809099;
  Dright *= 1.78105809099;
  Pleft *= 1.78105809099 * scale;
  Pright *= 1.78105809099 * scale;

  double ul = Pleft / ((eos_gamma - 1) * Dleft);
  double ur = Pright / ((eos_gamma - 1) * Dright);

  double massp = Dright * psep;
  double psepl = massp / Dleft;

  int ntot = (xc - lb) / psepl + (ub - xc) / psep;
  int nl = static_cast<int>((xc - lb) / psepl);
  int nr = static_cast<int>((ub - xc) / psep);

  alloc(ntot);
  x[0] = lb + 0.5 * psepl;
  x[1] = lb + 1.5 * psepl;

  for (int i = 0; i < 2; ++i) {
    rho[i] = Dleft;
    u[i] = ul;
    //    h[i] = eta*2.*massp/Dleft;
    //    mass[i] = massp/(1+ul);
    mass[i] = massp;
  }

  int j = 1;
  ntot = nl + nr;
  while (j < ntot) {
    double dx0 = massp / rho[j];
    double xhalf = x[j] + 0.5 * dx0;
    double delta = xhalf / psep;
    j++;
    double denshalf;
    if (delta < -dsmooth) {
      denshalf = Dleft;
    } else if (delta > dsmooth) {
      denshalf = Dright;
    } else {
      double exx = exp(delta);
      denshalf = (Dleft + Dright * exx) / (1. + exx);
    }

    double dxhalf = massp / denshalf;
    double dx1 = 2. * dxhalf - dx0;
    x[j] = xhalf + 0.5 * dx1;

    delta = (x[j] - xc) / psep;
    if (delta < -dsmooth) {
      rho[j] = Dleft;
      u[j] = ul;
    } else if (delta > dsmooth) {
      rho[j] = Dright;
      u[j] = ur;
    } else {
      double exx = exp(delta);
      rho[j] = (Dleft + Dright * exx) / (1. + exx);
      P[j] = (Pleft + Pright * exx) / (1. + exx);
      u[j] = (ul + ur * exx) / (1. + exx);
    }
    mass[j] = massp;
  }
  for (int i = 0; i < ntot; ++i) {
    //    Ebar[i] = 1.+u[i];
    Dstar[i] = rho[i] / (1. + u[i]);
    //    P[i] = (gamma-1.)*D[i]*u[i];
    h[i] = eta * mass[i] / rho[i];
  }
  // output_h5part(0);
  if (periodic) {
    periodic_box[1] = x[ntot - 1] + .5 * (x[ntot - 1] - x[ntot - 2]);
  }
  calc_init_D();

  //  double D_mean = particle_mean(&Dstar);
  // #pragma omp parallel for
  //  for (size_t i = 0; i < ntot; ++i) {
  //    mass[i] /= D_mean;
  //    h[i] = eta * std::pow(mass[i], 1. / (double)DIM);
  //  }

  //  calc_init_D();
  for (int i = 0; i < ntot; ++i) {
    //    Ebar[i] = 1.+u[i];
    //    P[i] = (gamma-1.)*D[i]*u[i];
    h[i] = eta * mass[i] / Dstar[i];
  }
  calc_init_D();
}
