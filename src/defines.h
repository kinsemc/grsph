// Copyright 2016 Matt Kinsey

// Some preprocessor directives related the dimensionality of the compile
// and alignment of vectors

#ifndef SRC_DEFINES_H_
#define SRC_DEFINES_H_

// Some DIM helpers

#define dim2 DIM >= 2
#define dim3 DIM >= 3

// Array alignment for vectorization

#define ALIGNTO 64

#if defined(__ICC)
#define ALIGNED __declspec(align(ALIGNTO))
#else
#if defined(__GNUC__)
#define ALIGNED __attribute__((aligned(ALIGNTO)))
#endif
#endif

#endif  // SRC_DEFINES_H_
