// Copyright 2016 Matt Kinsey

// Initialize a Gresho-Chen vortex

#include "grsph.h"

void grsph::init_vortex(double scalefac, double P0init, double Dmag) {
#if dim2
  size_t np = x.size();

  // Using the notation from rosswogs accuracy paper
  double R1 = 0.2 * scalefac;
  double P0 = .05 * scalefac * P0init;
  double v0 = 1. * scalefac;

  int total_np = global_np();

#pragma omp parallel for
  for (size_t i = 0; i < np; ++i) {
    double r2 = 0.;
    for (size_t d = 0; d < DIM; ++d) {
      r2 += (*pos_ptr[d])[i] * (*pos_ptr[d])[i];
    }
    double r = std::sqrt(r2);
    double xfac = y[i] / r;
    double yfac = -x[i] / r;

    double uu = r / R1;

    double vmag, Pmag;
    if (uu <= 1.) {
      vmag = uu;
      Pmag = .5 * v0 * v0 * uu * uu;
    } else if (uu <= 2.) {
      vmag = 2. - uu;
      Pmag = 4. * v0 * v0 * (uu * uu / 8. - uu + log(uu) + 1.);
    } else {
      vmag = 0.;
      Pmag = 4. * v0 * v0 * (log(2) - .5);
    }

    vmag *= v0;
    Pmag += P0;

    P[i] = Pmag;
    vx[i] = vmag * xfac;
    vy[i] = vmag * yfac;

    double v2 = 0.;
    for (size_t d = 0; d < DIM; ++d) {
      v2 += (*vel_ptr[d])[i] * (*vel_ptr[d])[i];
    }
    W[i] = 1. / std::sqrt(1. - v2);

    mass[i] = eta * 1. / total_np;
    mass[i] *= scalefac * scalefac;
    h[i] = eta * std::pow(mass[i] / Dmag, 1. / DIM);  // todo cast dim?
  }

  calc_init_D();

  double D_mean = particle_mean(&Dstar);
#pragma omp parallel for
  for (size_t i = 0; i < x.size(); ++i) {
    mass[i] /= D_mean / Dmag;
    h[i] = eta * std::pow(mass[i] / Dstar[i], 1. / DIM);  // todo cast dim?
  }

  calc_init_D();

#pragma omp parallel for
  for (size_t i = 0; i < x.size(); ++i) {
    u[i] = P[i] * W[i] / (eos_gamma - 1.) / Dstar[i];
  }
#else
  assert(0);
#endif
}
