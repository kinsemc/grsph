// Copyright 2016 Matt Kinsey

// This is very simple code to analytically invert small matrices
// todo there is some potential optimization in the 3x3 available using tmp vars

#ifndef SRC_INVERT_MATRIX_H_
#define SRC_INVERT_MATRIX_H_

#include <assert.h>

std::array<std::array<double, DIM>, DIM> invert_dim_matrix(
    const std::array<std::array<double, DIM>, DIM> m) {
  std::array<std::array<double, DIM>, DIM> r;

#if (DIM == 1)

  r[0][0] = 1. / m[0][0];

#elif (DIM == 2)

  double det = m[0][0] * m[1][1] - m[0][1] * m[1][0];
  assert(det != 0.);

  double idet = 1. / det;

  r[0][0] = idet * m[1][1];
  r[1][0] = idet * -m[1][0];
  r[0][1] = idet * -m[0][1];
  r[1][1] = idet * m[0][0];

#elif (DIM == 3)

  double det = m[0][0] * (m[1][1] * m[2][2] - m[1][2] * m[2][1]) -
               m[0][1] * (m[1][0] * m[2][2] - m[1][2] * m[2][0]) +
               m[0][2] * (m[1][0] * m[2][1] - m[1][1] * m[2][0]);
  assert(det != 0.);

  double idet = 1. / det;

  r[0][0] = idet * (m[1][1] * m[2][2] - m[1][2] * m[2][1]);
  r[1][0] = idet * (m[1][2] * m[2][0] - m[1][0] * m[2][2]);
  r[2][0] = idet * (m[1][0] * m[2][1] - m[1][1] * m[2][0]);

  r[0][1] = idet * (m[0][2] * m[2][1] - m[0][1] * m[2][2]);
  r[1][1] = idet * (m[0][0] * m[2][2] - m[0][2] * m[2][0]);
  r[2][1] = idet * (m[0][1] * m[2][0] - m[0][0] * m[2][1]);

  r[0][2] = idet * (m[0][1] * m[1][2] - m[0][2] * m[1][1]);
  r[1][2] = idet * (m[0][2] * m[1][0] - m[0][0] * m[1][2]);
  r[2][2] = idet * (m[0][0] * m[1][1] - m[0][1] * m[1][0]);

#endif

  return r;
}

#endif  // SRC_INVERT_MATRIX_H_
