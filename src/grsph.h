// Copyright 2016 Matt Kinsey

// TODO we need more function level doc
// todo add a switch for omega? (its not always used anymore)

#ifndef SRC_GRSPH_H_
#define SRC_GRSPH_H_

#include <assert.h>
#include <mpi.h>
#include <array>
#include <chrono>
#include <functional>
#include <iostream>
#include <string>
#include <unordered_map>
#include <utility>  // for pair<>
#include <vector>

// TBB types
#include <tbb/cache_aligned_allocator.h>
#include <tbb/concurrent_unordered_map.h>
#include <tbb/concurrent_vector.h>

// grsph headers
#include "defines.h"        // some compile time stuff
#include "kerr.h"           // kerr-schild BH object
#include "minkowski.h"      // flat spacetime object
#include "particle.h"       // single part struct
#include "schwarzschild.h"  // BH in schild coords
#include "spacetime.h"      // generic spacetime object

#include "grsph_types.h"
#include "grsph_variables.h"

namespace grsph {

// ============================================
// GRSPH Functions
// ============================================

// General utils (util.cc)
double fast_dist2(const size_t i, const size_t ni);

// Config file handling (config.cc)
void load_cfg(const char *filename);
// void save_cfg(const char *filename); // todo needed for checkpointing

// General allocation and memory handling for particle arrays (alloc.cc)
void init_grsph();  // Do some init of mainly mpi
void alloc(const size_t npart);
template <typename T>
void alloc_particle_array(particle_vector<T> *a, const size_t size,
                          const T init_val);
size_t push_particle(const particle_t &p, const bool ghostp);
particle_t get_particle(const size_t i);
void replace_particle(const size_t i, const particle_t &p);
particle_t pushpop_particle(const size_t i, const particle_t &p);
// void rmpart(int i);
// todo define an array of array pointers for MPI arrays?

// Hashtable building and NN search (hashtable.cc)
void build_hashtable();
void hash_table_insert(const size_t i);
void build_neighbor_list();
// todo function to query for 1 particle and return its NN's
// void update_particle_neighbor_list(const size_t i);
unsigned int hash_ind(const std::array<unsigned int, DIM> ind,
                      const unsigned int l);
// todo need to query arbitrary positions
// std::vector<size_t> position_find_neighbors(const std::array<double, DIM>
// pos)

// Unstructured mesh manipulation and initialization (mesh.cc)
void init_mesh();  // wrapper
void init_rand_mesh(const size_t np);
void init_gaussian_mesh(const size_t np, const double stddev);
void init_radial_mesh_from_table(std::vector<double> rvec,
                                 std::vector<double> F);
void hex_lattice(const int nx);
void init_empty_mesh();
void glassify_mesh(const double tol);
void perturb_mesh(const double max_disp);
void scale_mesh(const std::array<double, DIM> scale);
void shift_mesh(const std::array<double, DIM> shift);
void calc_global_box();
void cubic_lattice(const size_t nx);

// hydro initialization (init_*.cc)
void init_riemann1d(double Dleft, double Dright, double Pleft, double Pright,
                    double psep = 0.005, double dsmooth = 20,
                    double xminmax = 1., double scale = 1.);
void init_KH(double vy0);
void init_sedov(double P0 = 0.00001, double D0 = 1., double E0 = 1.,
                double dr = 0.05);
void init_vortex(double scalefac = 0.001, double Pmag = 1., double Dmag = 1.);
void init_uniform(double P0 = 1., double D0 = 1.);
void init_STtest();
void init_2dRiemanntest();
// init utils (init_utils.cc)
void calc_init_D();
void init_sys_from_prim();
void rescale_hydro(double scale);

// Bondi initialization (init_bondi.cc)
void init_bondi(const double rc = 8., const double K = 1.,
                const double Rmin = 2., const double Rmax = 10.);
void init_bondi_prims(double rc = 8);
std::vector<double> bondi_dens(double rc, std::vector<double> rvec);
std::vector<double> bondi_cum_dens(double rc, std::vector<double> rvec);

// Calc relativstic density (calc_D[_gsl].cc)
void calc_D();
void particle_D_h_NR(const size_t i);
void particle_D_h_NR_gsl(const size_t i);

// Calc divv (calc_divv.cc)
void calc_divv();

// Calc alpha (calc_alpha.cc)
void calc_alpha();

// GR rhs (calc_rhs.cc)
void calc_rhs();
void particle_rhs_Ccalc(const size_t i);

// con2prim (con2prim_[type][_gsl].cc)
// Wrappers
void con2prim();
void prim2con();
// Gamma-law
void particle_grc2p_gamma(const size_t i);
void particle_grc2p_gamma_gsl(const size_t i);
void particle_grp2c_gamma(const size_t i);
void particle_grc2p_new(const size_t i);
// Polytropic
void particle_grc2p_poly(const size_t i);
// void particle_grc2p_poly_gsl(const size_t i);
void particle_grp2c_poly(const size_t i);

// MPI related stuff / Domain decomp (mpi.cc)
void calc_ORB_boxes();    // Calculate domain decomp using ORB
void update_ORB_boxes();  // Update positions of ORB cuts via timing stats
void sync_particles_to_procs();  // send particles to the proc that owns thier
                                 // position
void enforce_periodic_bounds();  // enforce periodic bounds using periodic_box
// todo function to erase ONE particle? (currently there is alot of
// ghost[i]=1; clearghosts() in the code)
void print_boxesf();

// Ghost handling (MPI/periodic) (ghosts.cc)
void find_ghosts();
void find_periodic_ghosts();
void send_ghosts();
void clear_ghosts();  // clear all ghost[i] = true particles from arrays
void ghost_sync_particlevec(particle_vector<double> &pvec);

// MPI aware reductions for particles (reductions.cc)
int global_np(bool include_ghosts = true);
double particle_min(particle_vector<double> *pvec);
double particle_max(particle_vector<double> *pvec);
std::pair<double, double> particle_minmax(particle_vector<double> *pvec);
double particle_mean(particle_vector<double> *pvec);

// IO (io_*.cc)
void output_h5part(double t);
// void checkpoint_h5part( filename );  // TODO
// void recover_h5part( filename );  // TODO
void print_conservation();
void print_part(int i);  // not pretty, for debugging

// Time integration (integrate.cc)
void take_step();  // wrapper to take step using ptr funcs
void leapfrog_step();
void tvd_rk2_step();
void tvd_rk3_step();
void check_dt(const size_t i);

// Timers (timers.cc)
void start_rhs_timing();
void stop_rhs_timing();
void sync_rhs_times();
void start_timer(const std::string tname);
void stop_timer(const std::string tname);
void clear_timers();
void print_timers();

// Memory sorting using space filling curve (hilbert.cc)
void sort_particles();

// Parallel particle loop wrapper (particle_loop.cc)
void particle_loop(const particle_func_t particle_func);
};  // namespace grsph

#endif  // SRC_GRSPH_H_
