// Copyright 2016 Matt Kinsey

// Utility functions used by the hashtable

#ifndef SRC_HASH_UTILS_H_
#define SRC_HASH_UTILS_H_

#include <vector>

// Return true is x is prime, else return false
inline bool is_prime(size_t x) {
  for (size_t i = 3; true; i += 2) {
    size_t q = x / i;
    if (q < i) return true;
    if (x == q * i) return false;
  }
  return true;
}

// Return the next prime int greater than x
inline size_t next_prime(size_t x) {
  if (x <= 2) return 2;
  if (!(x & 1)) ++x;
  for (; !is_prime(x); x += 2) {
    // do nothing
  }
  return x;
}

// This function takes an array of size 2*DIM containing ranges (xmin, xmax,
// ymin, ymax...) and returns an array of all possible array<DIM> that exist in
// that range.
template <typename T>
std::vector<std::array<T, DIM>> enum_irange(
    const std::array<T, 2 * DIM> irange) {
  std::array<T, DIM> ind = {};
  std::vector<std::array<T, DIM>> ret;

  for (size_t d = 0; d < DIM; ++d) {
    ind[d] = irange[2 * d];
  }

  bool done = false;
  while (!done) {
    ret.push_back(ind);

    for (size_t d = 0; d < DIM; ++d) {
      if (ind[d] < irange[2 * d + 1]) {
        ind[d]++;
        break;
      } else {
        if (d == DIM - 1) {
          done = true;
          break;
        }
        ind[d] = irange[2 * d];
      }
    }
  }

  return ret;
}

#endif  // SRC_HASH_UTILS_H_
