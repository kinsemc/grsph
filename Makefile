# This is the Makefile to build grsph.so and then the wrapper in ./grsph.cc

###############################################
# General options
###############################################

# Compiler stuff
CPP=OMPI_CXX=clang++-3.6 mpic++
##clang++ (requires clang >=3.8, there is a parser bug in 3.7)
OPTFLAGS = -O3 -fopenmp=libiomp5
#icc
#OPTFLAGS = -O3 -xHost #-fp-model precise -fopenmp
#g++
#OPTFLAGS = -O3 -fopenmp

DEBUGFLAGS=-g -Wall -Wextra -Wno-unused-parameter -Wno-missing-field-initializers -Wno-missing-braces

# Locations of the dependencies
# these are the defaults made by ./build_deps.sh
HDF5DIR=$(CURDIR)/external_libs/hdf5
H5PARTDIR=$(CURDIR)/external_libs/h5hut
GSLDIR=$(CURDIR)/external_libs/gsl
LIBCONFDIR=$(CURDIR)/external_libs/libconfig
TBBDIR = $(CURDIR)/external_libs/tbb
HILBERTDIR = $(CURDIR)/external_libs/hilbert

###############################################
# careful editing anything below here
###############################################

SHELL=/bin/bash
SRCDIR=./src/
LIBDIR=$(CURDIR)/lib/
INCDIR=./include/
BUILDDIR=./obj/

# General flags 
LIBDIRS = #-L/opt/apps/intel/15/composer_xe_2015.2.164/tbb/lib/intel64/gcc4.4
LIBINC = 
INCLUDES = -I$(INCDIR)

## External library flags
#TBB
LIBDIRS += -Wl,-rpath,$(TBBDIR)/lib/intel64/gcc4.4 -L$(TBBDIR)/lib/intel64/gcc4.4
LIBINC += -ltbb #-ltbbmalloc_proxy
INCLUDES += -I$(TBBDIR)/include

# H5Part (and hdf5...)
LIBDIRS += -Wl,-rpath,$(H5PARTDIR)/lib -L$(H5PARTDIR)/lib -Wl,-rpath,$(HDF5DIR)/lib -L$(HDF5DIR)/lib
LIBINC += -lH5hut -lz -lhdf5
INCLUDES += -I$(H5PARTDIR)/include -I$(HDF5DIR)/include

# GSL
LIBDIRS += -Wl,-rpath,$(GSLDIR)/lib -L$(GSLDIR)/lib
LIBINC += -lgsl -lgslcblas
INCLUDES += -I$(GSLDIR)/include

# libconfig
LIBDIRS += -Wl,-rpath,$(LIBCONFDIR)/lib -L$(LIBCONFDIR)/lib
LIBINC += -lconfig++
INCLUDES += -I$(LIBCONFDIR)/include

# libhilbert
LIBDIRS += -Wl,-rpath,$(HILBERTDIR)/lib -L$(HILBERTDIR)/lib
LIBINC += -lHilbert
INCLUDES += -I$(HILBERTDIR)/include

# Don't edit anything down here
LIBS = $(LIBDIRS) $(LIBINC)
CPPFLAGS= -std=c++11 -DDIM=$(dim) $(OPTFLAGS) $(DEBUGFLAGS) $(INCLUDES)

CC_FILES := $(wildcard $(SRCDIR)*.cc)
OBJ_FILES := $(addprefix $(BUILDDIR),$(notdir $(CC_FILES:.cc=.o)))

# ok now for the build targets
.PHONY: deps all clean
all: | deps check-vars lib/libgrsph.so grsph

$(BUILDDIR)%.o: $(SRCDIR)%.cc
	$(CPP) $(CPPFLAGS) -c -fPIC -o $@ $^

lib/libgrsph.so: $(OBJ_FILES)
	$(CPP) $(CPPFLAGS) -shared -fPIC $(BUILDDIR)*.o -o $(LIBDIR)libgrsph.so $(LIBS)
	cp $(SRCDIR)*.h $(INCDIR)

grsph: grsph.cc lib/libgrsph.so #MPIbench.cc NNbench.cc
	$(CPP) $(CPPFLAGS) grsph.cc -o grsph -Wl,-rpath,$(LIBDIR) -L$(LIBDIR) -lgrsph 
#	$(CPP) $(CPPFLAGS) NNbench.cc -o NNbench -Wl,-rpath,$(LIBDIR) -L$(LIBDIR) -lgrsph $(LIBS) 
#	$(CPP) $(CPPFLAGS) MPIbench.cc -o MPIbench -Wl,-rpath,$(LIBDIR) -L$(LIBDIR) -lgrsph $(LIBS)

#opengl:
#	$(CPP) $(CPPFLAGS) opengltest.cc -o opengltest -Wl,-rpath,$(LIBDIR) -L$(LIBDIR) -lgrsph $(LIBS) -lglut -lGL -lGLU -lGLEW -lm

clean:
	rm -f $(BUILDDIR)*.o $(INCDIR)*.h* $(LIBDIR)*.so grsph
	rm -rf $(BUILDDIR) $(INCDIR) $(LIBDIR)

deps:
	./build_dependencies.sh

check-vars: mkdirs
ifndef dim
	echo "dim undefined, setting to 1"
endif
dim?=1

mkdirs:
	mkdir -p $(BUILDDIR)
	mkdir -p $(INCDIR)
	mkdir -p $(LIBDIR)

format:
	clang-format-3.8 -i -style=google src/*.cc
	clang-format-3.8 -i -style=google src/*.h
	clang-format-3.8 -i -style=google ./*.cc

tidy:
	clang-tidy-3.8 src/* -checks=-*,performance-*,mpi-* -- $(CPPFLAGS) $(LIBS)

lint:
	cpplint --filter=-build/include_subdir,-readability/todo src/*
	cpplint --filter=-build/include_subdir,-readability/todo grsph.cc
