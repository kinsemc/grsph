#!/bin/bash

# Copyright 2016 Matt Kinsey

# This script installs libhilbert
# if the first arg exists rebuild (it can have any val)

set -e

REBUILD=$1

LIBHILBERT_VERSION=libhilbert-0.2-1
EXTLIB_DIR=$(dirname $0)

NPROCS=1
OS=$(uname -s)

if [ "${OS}" = "Linux" ]; then
  NPROCS=$(grep -c ^processor /proc/cpuinfo)
fi
if [ "${OS}" = "Darwin" ]; then # Assume Mac OS X
  NPROCS=$(system_profiler | awk '/Number Of CPUs/{print $4}{next;}')
fi

echo "Building libhilbert on ${OS} with ${NPROCS} threads"

cd ${EXTLIB_DIR}
mkdir -p srcs
LIBHILBERT_INSTALL_BASE=$(pwd)/hilbert
LIBHILBERT_BASE=$(pwd)/srcs/${LIBHILBERT_VERSION}

# Remove old build
if [ -d "${LIBHILBERT_INSTALL_BASE}" ] && [ -n "${REBUILD}" ]; then
   echo "Removing old libhilbert install version"
   rm -rf ${LIBHILBERT_INSTALL_BASE}
fi
if [ -d "${LIBHILBERT_BASE}" ] && [ -n "${REBUILD}" ]; then
   echo "Removing old libhilbert build version"
   rm -rf ${LIBHILBERT_BASE}
fi

cd srcs

#Download tarball                                                               
if [ ! -f ${LIBHILBERT_VERSION}.tar.gz ]; then
  echo "Downloading ${LIBHILBERT_VERSION}.tar.gz"
  #wget https://web.cs.dal.ca/~chamilto/hilbert/${LIBHILBERT_VERSION}.tar.gz
  wget https://mkinsey.com/${LIBHILBERT_VERSION}.tar.gz
fi

#Start build
echo "Untarring ${LIBHILBERT_VERSION}.tar.gz"
tar xzf ${LIBHILBERT_VERSION}.tar.gz

if test ! -d "${LIBHILBERT_INSTALL_BASE}"; then
   mkdir ${LIBHILBERT_INSTALL_BASE}
fi

cd ${LIBHILBERT_BASE}

# edit the config.mk file
./configure --prefix=${LIBHILBERT_INSTALL_BASE}

echo "Building libhilbert"
make -j ${NPROCS}
make install
