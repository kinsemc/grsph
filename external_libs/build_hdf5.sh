#!/bin/bash

# Copyright 2016 Matt Kinsey

# This script installs hdf5
# if the first arg exists rebuild (it can have any val)

set -e

REBUILD=$1

HDF5_VERSION=hdf5-1.8.12
EXTLIB_DIR=$(dirname $0)

NPROCS=1
OS=$(uname -s)

if [ "${OS}" = "Linux" ]; then
  NPROCS=$(grep -c ^processor /proc/cpuinfo)
fi
if [ "${OS}" = "Darwin" ]; then # Assume Mac OS X
  NPROCS=$(system_profiler | awk '/Number Of CPUs/{print $4}{next;}')
fi

echo "Building hdf5++ on ${OS} with ${NPROCS} threads"

cd ${EXTLIB_DIR}
mkdir -p srcs
HDF5_INSTALL_BASE=$(pwd)/hdf5
HDF5_BASE=$(pwd)/srcs/${HDF5_VERSION}

# Remove old build
if [ -d "${HDF5_INSTALL_BASE}" ] && [ -n "${REBUILD}" ]; then
   echo "Removing old hdf5 install dir"
   rm -rf ${HDF5_INSTALL_BASE}
fi
if [ -d "${HDF5_BASE}" ] && [ -n "${REBUILD}" ]; then
   echo "Removing old hdf5 build dir"
   rm -rf ${HDF5_BASE}
fi

cd srcs

#Download tarball                                                               
if [ ! -f ${HDF5_VERSION}.tar.gz ]; then
  echo "Downloading ${HDF5_VERSION}.tar.gz"
  wget https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.8/${HDF5_VERSION}/src/${HDF5_VERSION}.tar.gz
fi

#Start build
echo "Untarring ${HDF5_VERSION}.tar.gz"
tar xzf ${HDF5_VERSION}.tar.gz

if test ! -d "${HDF5_INSTALL_BASE}"; then
   mkdir ${HDF5_INSTALL_BASE}
fi

cd ${HDF5_BASE}

echo "Building hdf5" 
if [ -n $1 ]; then
  make clean
fi

./configure --prefix=${HDF5_INSTALL_BASE} --enable-shared --enable-parallel
make -j ${NPROCS}
make install
