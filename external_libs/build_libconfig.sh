#!/bin/bash

# Copyright 2016 Matt Kinsey

# This script installs libconfig
# if the first arg exists rebuild (it can have any val)

set -e

REBUILD=$1

LIBCONFIG_VERSION=libconfig-1.7
EXTLIB_DIR=$(dirname $0)

NPROCS=1
OS=$(uname -s)

if [ "${OS}" = "Linux" ]; then
  NPROCS=$(grep -c ^processor /proc/cpuinfo)
fi
if [ "${OS}" = "Darwin" ]; then # Assume Mac OS X
  NPROCS=$(system_profiler | awk '/Number Of CPUs/{print $4}{next;}')
fi

echo "Building libconfig on ${OS} with ${NPROCS} threads"

cd ${EXTLIB_DIR}
mkdir -p srcs
LIBCONFIG_INSTALL_BASE=$(pwd)/libconfig
LIBCONFIG_BASE=$(pwd)/srcs/${LIBCONFIG_VERSION}

# Remove old build
if [ -d "${LIBCONFIG_INSTALL_BASE}" ] && [ -n "${REBUILD}" ]; then
   echo "Removing old libconfig install version"
   rm -rf ${LIBCONFIG_INSTALL_BASE}
fi
if [ -d "${LIBCONFIG_BASE}" ] && [ -n "${REBUILD}" ]; then
   echo "Removing old libconfig build version"
   rm -rf ${LIBCONFIG_BASE}
fi

cd srcs

#Download tarball                                                               
if [ ! -f ${LIBCONFIG_VERSION}.tar.gz ]; then
  echo "Downloading ${LIBCONFIG_VERSION}.tar.gz"
  wget https://hyperrealm.github.io/libconfig/dist/${LIBCONFIG_VERSION}.tar.gz
fi

#Start build
echo "Untarring ${LIBCONFIG_VERSION}.tar.gz"
tar xzf ${LIBCONFIG_VERSION}.tar.gz

if test ! -d "${LIBCONFIG_INSTALL_BASE}"; then
   mkdir ${LIBCONFIG_INSTALL_BASE}
fi

cd ${LIBCONFIG_BASE}

# edit the config.mk file
./configure --prefix=${LIBCONFIG_INSTALL_BASE}

echo "Building libconfig"
make -j ${NPROCS}
make install
