#!/bin/bash

# Copyright 2016 Matt Kinsey

# This script installs h5hut
# if the first arg exists rebuild (it can have any val)

set -e

REBUILD=$1

H5HUT_VERSION=H5hut-1.99.13
EXTLIB_DIR=$(dirname $0)

NPROCS=1
OS=$(uname -s)

if [ "${OS}" = "Linux" ]; then
  NPROCS=$(grep -c ^processor /proc/cpuinfo)
fi
if [ "${OS}" = "Darwin" ]; then # Assume Mac OS X
  NPROCS=$(system_profiler | awk '/Number Of CPUs/{print $4}{next;}')
fi

echo "Building h5hut++ on ${OS} with ${NPROCS} threads"

cd ${EXTLIB_DIR}
mkdir -p srcs
H5HUT_INSTALL_BASE=$(pwd)/h5hut
H5HUT_BASE=$(pwd)/srcs/${H5HUT_VERSION}
HDF5_INSTALL_BASE=$(pwd)/hdf5

# Remove old build
if [ -d "${H5HUT_INSTALL_BASE}" ] && [ -n "${REBUILD}" ]; then
   echo "Removing old h5hut install dir"
   rm -rf ${H5HUT_INSTALL_BASE}
fi
if [ -d "${H5HUT_BASE}" ] && [ -n "${REBUILD}" ]; then
   echo "Removing old h5hut build dir"
   rm -rf ${H5HUT_BASE}
fi

cd srcs

#Download tarball                                                               
if [ ! -f ${H5HUT_VERSION}.tar.gz ]; then
    echo "Downloading ${H5HUT_VERSION}.tar.gz"
      wget --no-check-certificate https://amas.psi.ch/H5hut/raw-attachment/wiki/DownloadSources/${H5HUT_VERSION}.tar.gz
fi

#Start build
echo "Untarring ${H5HUT_VERSION}.tar.gz"
tar xzf ${H5HUT_VERSION}.tar.gz

if test ! -d "${H5HUT_INSTALL_BASE}"; then
   mkdir ${H5HUT_INSTALL_BASE}
fi

cd ${H5HUT_BASE}

#if [ -n $1 ]; then
#  make clean
#fi

echo "Building h5hut"
./autogen.sh
LDFLAGS="-L${HDF5_INSTALL_BASE}" ./configure --prefix=${H5HUT_INSTALL_BASE} --with-hdf5=${HDF5_INSTALL_BASE} --enable-shared --enable-c --enable-parallel --with-mpi=/home/matt/software/openmpi
make
make install
