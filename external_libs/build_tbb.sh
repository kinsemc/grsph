#!/bin/bash

# Copyright 2016 Matt Kinsey

# This script installs tbb
# if the first arg exists rebuild (it can have any val)

set -e

REBUILD=$1

TBB_VERSION=tbb44_20160526oss
EXTLIB_DIR=$(dirname $0)

NPROCS=1
OS=$(uname -s)

if [ "${OS}" = "Linux" ]; then
  NPROCS=$(grep -c ^processor /proc/cpuinfo)
fi
if [ "${OS}" = "Darwin" ]; then # Assume Mac OS X
  NPROCS=$(system_profiler | awk '/Number Of CPUs/{print $4}{next;}')
fi

echo "Building TBB on ${OS} with ${NPROCS} threads"

cd ${EXTLIB_DIR}
mkdir -p srcs
TBB_INSTALL_BASE=$(pwd)/tbb
TBB_BASE=$(pwd)/srcs/${TBB_VERSION}

# Remove old build
if [ -d "${TBB_INSTALL_BASE}" ] && [ -n "${REBUILD}" ]; then
   echo "Removing old GSL install version"
   rm -rf ${TBB_INSTALL_BASE}
fi
if [ -d "${TBB_BASE}" ] && [ -n "${REBUILD}" ]; then
   echo "Removing old GSL build version"
   rm -rf ${TBB_BASE}
fi

cd srcs

#Download tarball
if [ ! -f ${TBB_VERSION}_lin_1.tgz ]; then
  echo "Downloading ${TBB_VERSION}_lin_1.tgz"
  wget https://www.threadingbuildingblocks.org/sites/default/files/software_releases/linux/${TBB_VERSION}_lin_1.tgz
fi

#Start build
echo "Untarring ${TBB_VERSION}.tar.gz"
tar xzf ${TBB_VERSION}_lin_1.tgz

if test ! -d "${TBB_INSTALL_BASE}"; then
   mkdir ${TBB_INSTALL_BASE}
fi

#cd ${TBB_BASE}

echo "Building TBB"
cp -r ${TBB_VERSION}/* ${TBB_INSTALL_BASE}/
