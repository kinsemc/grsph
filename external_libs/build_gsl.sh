#!/bin/bash

# Copyright 2016 Matt Kinsey

# This script installs gsl
# if the first arg exists rebuild (it can have any val)

set -e

REBUILD=$1

GSL_VERSION=gsl-2.1
EXTLIB_DIR=$(dirname $0)

NPROCS=1
OS=$(uname -s)

if [ "${OS}" = "Linux" ]; then
  NPROCS=$(grep -c ^processor /proc/cpuinfo)
fi
if [ "${OS}" = "Darwin" ]; then # Assume Mac OS X
  NPROCS=$(system_profiler | awk '/Number Of CPUs/{print $4}{next;}')
fi

echo "Building GSL on ${OS} with ${NPROCS} threads"

cd ${EXTLIB_DIR}
mkdir -p srcs
GSL_INSTALL_BASE=$(pwd)/gsl
GSL_BASE=$(pwd)/srcs/${GSL_VERSION}

# Remove old build
if [ -d "${GSL_INSTALL_BASE}" ] && [ -n "${REBUILD}" ]; then
   echo "Removing old GSL install version"
   rm -rf ${GSL_INSTALL_BASE}
fi
if [ -d "${GSL_BASE}" ] && [ -n "${REBUILD}" ]; then
   echo "Removing old GSL build version"
   rm -rf ${GSL_BASE}
fi

cd srcs

#Download tarball
if [ ! -f ${GSL_VERSION}.tar.gz ]; then
  echo "Downloading ${GSL_VERSION}.tar.gz"
  wget ftp://ftp.gnu.org/gnu/gsl/${GSL_VERSION}.tar.gz
fi

#Start build
echo "Untarring ${GSL_VERSION}.tar.gz"
tar xzf ${GSL_VERSION}.tar.gz

if test ! -d "${GSL_INSTALL_BASE}"; then
   mkdir ${GSL_INSTALL_BASE}
fi

cd ${GSL_BASE}

echo "Building GSL"
./configure --prefix=${GSL_INSTALL_BASE}
make -j ${NPROCS}
make install
